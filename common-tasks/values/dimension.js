import React from "react";
import { Dimensions, PixelRatio } from "react-native";
let dimensions = Dimensions.get("window");

export default class Dimension extends React.Component {
    static width = dimensions.width
    static height = dimensions.height

    //button opacity
    static buttonOpacity = 0.95

    //toolbar height
    static toolbarHeight = PixelRatio.roundToNearestPixel(56);
    static toolbarIconSize = PixelRatio.roundToNearestPixel(20);
    static toolbarInsetStart = PixelRatio.roundToNearestPixel(0);
    static toolbarInsetEnd = PixelRatio.roundToNearestPixel(0);

    //font size
    static fontSize10 = PixelRatio.roundToNearestPixel(10);
    static fontSize11 = PixelRatio.roundToNearestPixel(11);
    static fontSize12 = PixelRatio.roundToNearestPixel(12);
    static fontSize13 = PixelRatio.roundToNearestPixel(13);
    static fontSize14 = PixelRatio.roundToNearestPixel(14);
    static fontSize15 = PixelRatio.roundToNearestPixel(15);
    static fontSize16 = PixelRatio.roundToNearestPixel(16);
    static fontSize17 = PixelRatio.roundToNearestPixel(17);
    static fontSize18 = PixelRatio.roundToNearestPixel(18);
    static fontSize19 = PixelRatio.roundToNearestPixel(19);
    static fontSize20 = PixelRatio.roundToNearestPixel(20);
    static fontSize22 = PixelRatio.roundToNearestPixel(22);
    static fontSize24 = PixelRatio.roundToNearestPixel(24);
    static fontSize26 = PixelRatio.roundToNearestPixel(26);
    static fontSize28 = PixelRatio.roundToNearestPixel(28);
    static fontSize30 = PixelRatio.roundToNearestPixel(30);
    static fontSize35 = PixelRatio.roundToNearestPixel(35);
    static fontSize40 = PixelRatio.roundToNearestPixel(40);

    //margins
    static margin1 = PixelRatio.roundToNearestPixel(1);
    static margin2 = PixelRatio.roundToNearestPixel(2);
    static margin3 = PixelRatio.roundToNearestPixel(3);
    static margin4 = PixelRatio.roundToNearestPixel(4);
    static margin5 = PixelRatio.roundToNearestPixel(5);
    static margin7 = PixelRatio.roundToNearestPixel(7);
    static margin6 = PixelRatio.roundToNearestPixel(6);
    static margin8 = PixelRatio.roundToNearestPixel(8);
    static margin10 = PixelRatio.roundToNearestPixel(10);
    static margin15 = PixelRatio.roundToNearestPixel(15);
    static margin20 = PixelRatio.roundToNearestPixel(20);
    static margin25 = PixelRatio.roundToNearestPixel(25);
    static margin30 = PixelRatio.roundToNearestPixel(30);
    static margin35 = PixelRatio.roundToNearestPixel(35);
    static margin40 = PixelRatio.roundToNearestPixel(40);
    static margin45 = PixelRatio.roundToNearestPixel(45);
    static margin50 = PixelRatio.roundToNearestPixel(50);
    static margin55 = PixelRatio.roundToNearestPixel(55);
    static margin60 = PixelRatio.roundToNearestPixel(60);
    static margin70 = PixelRatio.roundToNearestPixel(70);
    static margin80 = PixelRatio.roundToNearestPixel(80);
    static margin90 = PixelRatio.roundToNearestPixel(90);
    static margin100 = PixelRatio.roundToNearestPixel(100);
    static margin150 = PixelRatio.roundToNearestPixel(150);
    static margin250 = PixelRatio.roundToNearestPixel(250);

    //paddings
    static padding2 = PixelRatio.roundToNearestPixel(2);
    static padding3 = PixelRatio.roundToNearestPixel(3);
    static padding5 = PixelRatio.roundToNearestPixel(5);
    static padding7 = PixelRatio.roundToNearestPixel(7);
    static padding10 = PixelRatio.roundToNearestPixel(10);
    static padding13 = PixelRatio.roundToNearestPixel(13);
    static padding15 = PixelRatio.roundToNearestPixel(15);
    static padding20 = PixelRatio.roundToNearestPixel(20);
    static padding25 = PixelRatio.roundToNearestPixel(25);
    static padding30 = PixelRatio.roundToNearestPixel(30);
    static padding35 = PixelRatio.roundToNearestPixel(35);
    static padding40 = PixelRatio.roundToNearestPixel(40);
    static padding45 = PixelRatio.roundToNearestPixel(45);
    static padding50 = PixelRatio.roundToNearestPixel(50);

    //radius
    static radius3 = PixelRatio.roundToNearestPixel(3);
    static radius5 = PixelRatio.roundToNearestPixel(5);
    static radius10 = PixelRatio.roundToNearestPixel(10);
    static radius15 = PixelRatio.roundToNearestPixel(15);
    static radius17 = PixelRatio.roundToNearestPixel(17);
    static radius20 = PixelRatio.roundToNearestPixel(20);
    static radius25 = PixelRatio.roundToNearestPixel(25);
    static radius30 = PixelRatio.roundToNearestPixel(30);

    //button
    static zIndexNormal = 5;
    static elevationLight = 1;
    static elevationNormal = 3;
    static elevationHigh = 10;

    //static positive size
    static size0 = PixelRatio.roundToNearestPixel(0);
    static size1 = PixelRatio.roundToNearestPixel(1);
    static size5 = PixelRatio.roundToNearestPixel(5);
    static size10 = PixelRatio.roundToNearestPixel(10);
    static size13 = PixelRatio.roundToNearestPixel(13);
    static size15 = PixelRatio.roundToNearestPixel(15);
    static size20 = PixelRatio.roundToNearestPixel(20);
    static size25 = PixelRatio.roundToNearestPixel(25);
    static size30 = PixelRatio.roundToNearestPixel(30);
    static size34 = PixelRatio.roundToNearestPixel(34);
    static size35 = PixelRatio.roundToNearestPixel(35);
    static size40 = PixelRatio.roundToNearestPixel(40);
    static size45 = PixelRatio.roundToNearestPixel(45);
    static size50 = PixelRatio.roundToNearestPixel(50);
    static size55 = PixelRatio.roundToNearestPixel(55);
    static size60 = PixelRatio.roundToNearestPixel(60);
    static size70 = PixelRatio.roundToNearestPixel(70);
    static size80 = PixelRatio.roundToNearestPixel(80);
    static size85 = PixelRatio.roundToNearestPixel(85);
    static size90 = PixelRatio.roundToNearestPixel(90);
    static size100 = PixelRatio.roundToNearestPixel(100);
    static size105 = PixelRatio.roundToNearestPixel(105);
    static size120 = PixelRatio.roundToNearestPixel(120);
    static size125 = PixelRatio.roundToNearestPixel(100);
    static size140 = PixelRatio.roundToNearestPixel(140);
    static size150 = PixelRatio.roundToNearestPixel(150);
    static size200 = PixelRatio.roundToNearestPixel(200);
    static size250 = PixelRatio.roundToNearestPixel(250);
    static size300 = PixelRatio.roundToNearestPixel(300);
    static size400 = PixelRatio.roundToNearestPixel(400);
    static size450 = PixelRatio.roundToNearestPixel(450);
    static size500 = PixelRatio.roundToNearestPixel(500);

    //static negative size
    static sizeMinus3 = PixelRatio.roundToNearestPixel(-3);
    static sizeMinus5 = PixelRatio.roundToNearestPixel(-5);
    static sizeMinus7 = PixelRatio.roundToNearestPixel(-7);
    static sizeMinus10 = PixelRatio.roundToNearestPixel(-10);
    static sizeMinus15 = PixelRatio.roundToNearestPixel(-15);
    static sizeMinus20 = PixelRatio.roundToNearestPixel(-20);
    static sizeMinus25 = PixelRatio.roundToNearestPixel(-25);
}
