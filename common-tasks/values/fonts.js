export default class Fonts {
    static ralewayRegular="Raleway-Regular";
    static ralewayMedium = "Raleway-Medium";
    static ralewayBold="Raleway-Bold";
    static ralewayBlack="Raleway-Black";
    static ralewayLight="Raleway-Light";
    static ralewayThin="Raleway-Thin";

    // static montserratRegular="Montserrat-Regular";
    // static montserratMedium = "Montserrat-Medium";
    // static montserratBold="Montserrat-Bold";
    // static montserratBlack="Montserrat-Black";
    // static montserratLight="Montserrat-Light";
    // static montserratThin="Montserrat-Thin";
    // static montserratSemiBold="Montserrat-SemiBold";

    // static robotoRegular="Roboto-Regular";
    // static robotoMedium = "Roboto-Medium";
    // static robotoBold="Roboto-Bold.ttf";
    // static robotoBlack="Roboto-Black.ttf";
    // static robotoLight="Roboto-Light.ttf";

    // static clarikaRegular="CLARIKA-REGULAR";
    // static clarikaSemiBold = "CLARIKA-MEDIUM";
    // static clarikaBold = "CLARIKA-BOLD";
    static regularText = "SF-Pro-Display-Regular";
    static mediumText = "SF-Pro-Display-Medium";
    static boldText = "SF-Pro-Display-Bold";
}
