import CommonTasks from "../../common-tasks/common-tasks";

export default class Strings {
    //base urls starts with base_url
    //api endpoints starts with api
    //api params starts with pr
    //status strings starts with st
    //intent data starts with in
    //layout screen strings starts with screen name

    //base urls
    static BASE_URL_API = CommonTasks.ROOT_URL;

    //api endpoints
    static API_REVIEW_ADD = "review/add";
    static API_REVIEW_EDIT = 'review/edit';
    static API_REVIEW_DELETE = 'review/delete';
    static API_REVIEW_DELETE_FILE = 'review/delete-file';
    static API_VERIFY_REFERRAL_CODE = "verify-referral-code";
    static API_REPORT_US = "store/report-business";
    static API_REVIEW_COMMENT_ADD = "review/comment/add";
    static API_REVIEW_COMMENT_DELETE = "review/comment/delete";
    static API_REVIEW_COMMENT_LIST = "review/comment/list";
    static API_OFFER_COMMENT_ADD = "offer/comment/add";
    static API_OFFER_COMMENT_DELETE = "offer/comment/delete";
    static API_OFFER_COMMENT_LIST = "offer/comment/list";
    static API_POST_COMMENT_ADD = "post/comment/add";
    static API_POST_COMMENT_DELETE = "post/comment/delete";
    static API_POST_COMMENT_LIST = "post/comment/list";
    static API_POST_FILE_DELETE = "post/delete-file";
    static API_POST_EDIT = "post/edit";
    static API_POST_DELETE = "post/delete";
    static API_FOOD_MENU_COMMENT_ADD = "food-menu/comment/add";
    static API_FOOD_MENU_COMMENT_DELETE = "food-menu/comment/delete";
    static API_FOOD_MENU_COMMENT_LIST = "food-menu/comment/list";
    static API_STORE_PHOTO_COMMENT_ADD = "store/photo/comment/add";
    static API_STORE_PHOTO_COMMENT_DELETE = "store/photo/comment/delete";
    static API_STORE_PHOTO_COMMENT_LIST = "store/photo/comment/list";
    static API_TIPS_COMMENT_ADD = "tips/comment/add";
    static API_TIPS_COMMENT_DELETE = "tips/comment/delete";
    static API_TIPS_COMMENT_LIST = "tips/comment/list";
    static API_PRODUCT_COMMENT_ADD = "product/comment/add";
    static API_PRODUCT_COMMENT_DELETE = "product/comment/delete";
    static API_PRODUCT_COMMENT_LIST = "product/comment/list";
    static API_SOCIAL_SHARING = "store/social-sharing";
    static API_STORE_PHOTO_DELETE = "store/photo/delete";
    static API_CHANGE_PASSWORD = "user/change-password";
    static API_CLAIM_BUSINESS = "store/claim-business";
    static API_TERMS_COND = "terms/get-terms";
    static API_TIPS_DELETE="tips/delete"
    // static API_PHOTO_DELETE = "terms/get-terms";
   

    //api params starts with pr
    static PR_NEXT = "next";
    static PR_LIMIT = "limit";
    static PR_ID = "id";
    static PR_USER_ID = "user_id";
    static PR_COMMENT_ID = "comment_id";
    static PR_PARENT_ID = "parent_id";
    static PR_COMMENT = "comment";
    static PR_FILE = "file";
    static PR_REVIEW_ID = "review_id";
    static PR_OFFER_ID = "offer_id";
    static PR_POST_ID = "post_id";
    static PR_MENU_ID = "menu_id";
    static PR_IMAGE_ID = "image_id";
    static PR_TIPS_ID = "tips_id";
    static PR_PRODUCT_ID = "product_id";
    static PR_ENTITY_TYPE = 'entity_type';
    static PR_ENTITY_ID = 'entity_id';
    static PR_ITEM_ID = 'item_id';
    static PR_STORE_ID = 'store_id';
    static PR_FOLLOWING_ID = 'following_id';
    static PR_POST_ID = 'post_id';

    //static final variables
    static ST_REVIEW_MAX_FILE_COUNT = 5;
    static ST_MIME_TYPE_IMAGE = "image/jpeg";
    static ST_MIME_TYPE_VIDEO = "video/mp4";

    //screens

    //review screen
    static rvw_title = "Write a Review";
    static rvw_edit_title = "Edit Review";
    static rvw_review_label = "Write a review";
    static rvw_feedback_label = "How much did you enjoyed their service?";
    static rvw_horrible = "Horrible";
    static rvw_bad = "Bad";
    static rvw_average = "Average";
    static rvw_good = "Good";
    static rvw_excellent = "Excellent";
    static rvw_upload_file_label = "Add Photos/Videos";
    static rvw_upload_photo_label = "Upload Photo";
    static rvw_modal_title_label = "Choose File";
    static rvw_upload_video_label = "Upload Video";
    static rvw_choose_photo_title = "Choose Photo";
    static rvw_choose_photo_camera = "Camera";
    static rvw_choose_photo_cancel = "Cancel";
    static rvw_choose_video_title = "Choose Video";
    static rvw_choose_video_camera = "Camera";
    static rvw_choose_video_cancel = "Cancel";
    static rvw_err_max_file = "You can't add files more than " + Strings.ST_REVIEW_MAX_FILE_COUNT;
    static rvw_err_empty_rating = "Please rate the restaurant";
    static rvw_err_empty_review = "Please enter your review";
    static rvw_err_empty_feedback = "Please enter how you enjoyed their service";
    static rvw_scss_label1 = "Review submitted!";
    static rvw_scss_label2 = "Thanks for your valuable time.";
    static rvw_scss_btn = "Done";

    //referral code
    static rfrl_verify_title = "Referral Code";
    static rfrl_verify_referral_code_label = "Do You Have Any Referral Code?";
    static rfrl_verify_referral_code_skip_label = "If you don't have any referral code! then";
    static rfrl_verify_referral_code_skip = "Skip";
    static rfrl_verify_err_empty_referral_code = "Please enter your referral code";

    //comment screen
    static cmnt_write_comment = "Write a comment...";
    static cmnt_write_reply = "Write a reply...";
    static cmnt_delete_label = "Delete";
    static cmnt_reply_label = "Reply";
    static cmnt_comment_add_successfull = "Comment added successfully"
    static cmnt_reply_add_successfull = "Reply added successfully"

    //qr code scanner
    static qr_scnr_title = "QR Scanner";
    static qr_scnr_manual_qr_code_label = "Trouble Scanning QR Code? Enter Manually";

    //qr code viewer
    static qr_vwr_title = "Profile";
    static qr_vwr_since = "on Ratebowl since ";
    static qr_vwr_download = "Download QR Code";

    //report screen
    static rprtHeader = "Report Us";
    static rprtHeaderInfo1 = "Report us regarding the business.";
    static rprtItemHeaderInfo1 = "Report us regarding this ";
    static rprtHeaderInfo2 = "Feel Free to contact us.";
    static rprtFirstNameLabel = "First Name";
    static rprtLastNameLabel = "Last Name";
    static rprtEmailAddressLabel = "Email Address";
    static rprtAboutMeLabel = "About Me";
    static rprtBtnSubmit = "Submit Report";
    static rprtSuccess = "Report Submitted Successfully";
    static rprtDescriptionLabel = "Report Description";
    static rprtLabel
    //store detail
    static storeInfoTab = 'info';
    static storePostTab = 'storePage';
    static storeReviewTab = 'reviews';
    static storeOffersTab = 'offers';
    static storeTipsTab = 'more';
    static storeProductsTab = 'product';
    static storeMenusTab = 'menu';
    static storePhotosTab = 'photos';
    static storePostEntityType = 1;
    static storeReviewEntityType = 2;
    static storeOffersEntityType = 3;
    static storeTipsEntityType = 4;
    static storeProductsEntityType = 5;
    static storeMenusEntityType = 6;
    static storePhotosEntityType = 7;
    static storePostValue = 'post';
    static storeReviewValue = 'review';
    static storeOffersValue = 'offer';
    static storeTipsValue = 'tips';
    static storeProductsValue = 'product';
    static storeMenusValue = 'food-menu';
    static storePhotosValue = 'store-photo';


    //My account pages
    static acFollowersLabel = 'Followers'
    static acFollowingLabel = 'Following'
    static acfollowLabel = 'Follow'
    static acReviewsLabel = 'Reviews'
    static acPhotosLabel = 'Photos'
    static acStoreFollowLabel = 'Stores I Follow'
    static acLikesLabel = 'Likes'
    static acPhotoCatrgory = 'photos'
    static acFollowerCategory = 'followers'
    static acStoreFollowCategory = 'followingStore'
    static acLikesCategory = 'likes'
    static acRvwCategory = 'reviews'
    static acFirstSubCategory = 0
    static acSecondSubCategory = 1
    static unfollow_btn_txt = 'Unfollow'
    static follow_btn_txt = 'Follow'


    //settings page
    static setting_oldPass_label = 'Old Password'
    static setting_NewPass_label = 'New Password'
    static setting_ConfPass_label = 'Re-Type New Password'

    //error messages
    static errInternet = "No Internet Connection";
    static errServer = "Server Issues. Please try again later";
    static errEmptyFirstName = "Please enter first name";
    static errEmptyLastName = "Please enter last name";
    static errEmptyEmailAddress = "Please enter email address";
    static errEmptyAboutMe = "Please enter about me";
    static errInvalidEmailAddress = "Please enter valid email address";
    static errEmptyComment = "Please enter comment"
    static errEmptyCommentId = "Comment not found"
    static errEmptyUserId = "User not found"
    static errEmptyReply = "Please enter reply"
    static errEmptyDesc = "Please enter a description."

    static submit = "Submit";

}
