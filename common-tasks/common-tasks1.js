/*
*Common Tasks carried all over the project
*/

import React from 'react';
import Toast from 'react-native-root-toast';
import { Dimensions } from 'react-native';

export default class CommonTasks extends React.Component {

    static ROOT_URL = 'https://desarrollo.viamotutti.com/';
    //static GOOGLE_API_KEY = 'AIzaSyD8wisNt-7Npk-ZCnF8vupN987HCeVsllQ';

    //new key =AIzaSyD33IAoF6xVdx_edgK6URFFz0AWF6NaSPo

    //static GOOGLE_API_KEY = 'AIzaSyAbltHB5ecOCRsXFi2cWjZ7c43EiimF-pU';
    static GOOGLE_API_KEY = 'AIzaSyD33IAoF6xVdx_edgK6URFFz0AWF6NaSPo';
    //  static LANGUAGE='en';
    //  static REGION='IN';
    //  static LANGUAGE='es';
    //  static REGION='ES';

    static LANGUAGE = 'es';
    static REGION = 'IDD';
    static COUNTRY = 'AR';

    //static GOOGLE_API_KEY = 'AIzaSyD8wisNt-7Npk-ZCnF8vupN987HCeVsllQ';


    //FUNCTION - for displaying toast
    static _displayToast(message) {
        Toast.show(message, {
            duration: Toast.durations.SHORT,
            position: 10,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: "#f46468",
            textColor: "#ffff",
            containerStyle: {
                padding: 10,
                width: Dimensions.get('window').width,
                minHeight: 60,
                borderRadius: 0,
                justifyContent: 'center',
                alignItems: "center",
            },

            onShow: () => {

            },
            onShown: () => {

            },
            onHide: () => {

            },
            onHidden: () => {

            }
        });
    }

    static _displayToastSuccess(message) {
        Toast.show(message, {
            duration: Toast.durations.SHORT,
            position: 10,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            backgroundColor: "#009eff",
            textColor: "#ffff",
            containerStyle: {
                padding: 10,
                width: Dimensions.get('window').width,
                minHeight: 60,
                borderRadius: 0,
                justifyContent: 'center',
                alignItems: "center",
            },

            onShow: () => {

            },
            onShown: () => {

            },
            onHide: () => {

            },
            onHidden: () => {

            }
        });
    }


    //FUNCTION - verify whether entered email is valid or not
    static _verifyEmail(email) {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (email.match(mailformat)) {
            // alert("Valid Email");
            return true;
        }
        else {
            // alert("Invalid Email");
            return false;
        }
    }

    static serviceList = [
        {
            "name": "Landscaping"            
        },
        {
            "name": "Lawn care"            
        },
        {
            "name": "Tree maintenance"            
        },
        {
            "name": "Pavers"            
        },
        {
            "name": "Gravel"            
        },
        {
            "name": "Lighting"            
        },
        {
            "name": "Sod installation"            
        },
        {
            "name": "Synthetic grass"            
        },
        {
            "name": "Irrigation"            
        },
        {
            "name": "Walls"            
        },
        {
            "name": "Concrete"            
        }
    ];

    static stateList = [
        {
            "name": "Alabama",
            "abbreviation": "AL"
        },
        {
            "name": "Alaska",
            "abbreviation": "AK"
        },
        {
            "name": "American Samoa",
            "abbreviation": "AS"
        },
        {
            "name": "Arizona",
            "abbreviation": "AZ"
        },
        {
            "name": "Arkansas",
            "abbreviation": "AR"
        },
        {
            "name": "California",
            "abbreviation": "CA"
        },
        {
            "name": "Colorado",
            "abbreviation": "CO"
        },
        {
            "name": "Connecticut",
            "abbreviation": "CT"
        },
        {
            "name": "Delaware",
            "abbreviation": "DE"
        },
        {
            "name": "District Of Columbia",
            "abbreviation": "DC"
        },
        {
            "name": "Federated States Of Micronesia",
            "abbreviation": "FM"
        },
        {
            "name": "Florida",
            "abbreviation": "FL"
        },
        {
            "name": "Georgia",
            "abbreviation": "GA"
        },
        {
            "name": "Guam",
            "abbreviation": "GU"
        },
        {
            "name": "Hawaii",
            "abbreviation": "HI"
        },
        {
            "name": "Idaho",
            "abbreviation": "ID"
        },
        {
            "name": "Illinois",
            "abbreviation": "IL"
        },
        {
            "name": "Indiana",
            "abbreviation": "IN"
        },
        {
            "name": "Iowa",
            "abbreviation": "IA"
        },
        {
            "name": "Kansas",
            "abbreviation": "KS"
        },
        {
            "name": "Kentucky",
            "abbreviation": "KY"
        },
        {
            "name": "Louisiana",
            "abbreviation": "LA"
        },
        {
            "name": "Maine",
            "abbreviation": "ME"
        },
        {
            "name": "Marshall Islands",
            "abbreviation": "MH"
        },
        {
            "name": "Maryland",
            "abbreviation": "MD"
        },
        {
            "name": "Massachusetts",
            "abbreviation": "MA"
        },
        {
            "name": "Michigan",
            "abbreviation": "MI"
        },
        {
            "name": "Minnesota",
            "abbreviation": "MN"
        },
        {
            "name": "Mississippi",
            "abbreviation": "MS"
        },
        {
            "name": "Missouri",
            "abbreviation": "MO"
        },
        {
            "name": "Montana",
            "abbreviation": "MT"
        },
        {
            "name": "Nebraska",
            "abbreviation": "NE"
        },
        {
            "name": "Nevada",
            "abbreviation": "NV"
        },
        {
            "name": "New Hampshire",
            "abbreviation": "NH"
        },
        {
            "name": "New Jersey",
            "abbreviation": "NJ"
        },
        {
            "name": "New Mexico",
            "abbreviation": "NM"
        },
        {
            "name": "New York",
            "abbreviation": "NY"
        },
        {
            "name": "North Carolina",
            "abbreviation": "NC"
        },
        {
            "name": "North Dakota",
            "abbreviation": "ND"
        },
        {
            "name": "Northern Mariana Islands",
            "abbreviation": "MP"
        },
        {
            "name": "Ohio",
            "abbreviation": "OH"
        },
        {
            "name": "Oklahoma",
            "abbreviation": "OK"
        },
        {
            "name": "Oregon",
            "abbreviation": "OR"
        },
        {
            "name": "Palau",
            "abbreviation": "PW"
        },
        {
            "name": "Pennsylvania",
            "abbreviation": "PA"
        },
        {
            "name": "Puerto Rico",
            "abbreviation": "PR"
        },
        {
            "name": "Rhode Island",
            "abbreviation": "RI"
        },
        {
            "name": "South Carolina",
            "abbreviation": "SC"
        },
        {
            "name": "South Dakota",
            "abbreviation": "SD"
        },
        {
            "name": "Tennessee",
            "abbreviation": "TN"
        },
        {
            "name": "Texas",
            "abbreviation": "TX"
        },
        {
            "name": "Utah",
            "abbreviation": "UT"
        },
        {
            "name": "Vermont",
            "abbreviation": "VT"
        },
        {
            "name": "Virgin Islands",
            "abbreviation": "VI"
        },
        {
            "name": "Virginia",
            "abbreviation": "VA"
        },
        {
            "name": "Washington",
            "abbreviation": "WA"
        },
        {
            "name": "West Virginia",
            "abbreviation": "WV"
        },
        {
            "name": "Wisconsin",
            "abbreviation": "WI"
        },
        {
            "name": "Wyoming",
            "abbreviation": "WY"
        },
        {
            "name": "District of Columbia",
            "abbreviation": "DC"
        }
    ];
}

