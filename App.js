/*Pro In My Pocket*/


/*Color Codes
blue-#3A4958
black-#1e1e1e
or-#ababab
buttontext:#8b8b8b,
buttonbackgroud:#e0e0e0
 width: (screenSize.width) / 2 + 50, height: (screenSize.height) / 4
*/

/*Font name*/


import React, { Component } from 'react';
import { name as appName } from './app.json';
import {
  AppRegistry,
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,
  Image,
  Alert,
  SafeAreaView,
  Dimensions,
  ImageBackground,
} from 'react-native';

import { StackNavigator, NavigationActions, StackActions, createStackNavigator } from 'react-navigation';
import firebase from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';

import CommonTasks from "./common-tasks/common-tasks";

import SplashPage from './components/Home/splash-page';
import Home from './components/Home/home.js';
import SignUp from './components/Home/signup.js';
import SigninConfirm from './components/Home/signin-confirm';
import SigninWithEmail from './components/Home/sigin-with-email';
import ForgotPassword from './components/Home/forgot_password';




import CreateAccountStep1 from './components/CreateAccount/create-account-step1';
import CreateAccountStep2 from './components/CreateAccount/create-account-step2';
import CreateAccountStep2Fb from './components/CreateAccount/create-account-step2-facebookLogin.js';
import CreateAccountStep3 from './components/CreateAccount/create-account-step3';
import CreateAccountStep4 from './components/CreateAccount/create-account-step4';
import CreateAccountStep5 from './components/CreateAccount/create-account-step5';



import MyAccount from './components/MyAccount/my-account';
import BusinessInfoEdit from './components/MyAccount/business_info_edit.js';
import BusinessSummaryEdit from './components/MyAccount/business_summary_edit.js';
import ServiceEdit from './components/MyAccount/service_edit.js';
import ChangePassword from './components/MyAccount/change_password.js';
import AccountSettings from './components/MyAccount/account-setting.js';
import MyAccountPreview from './components/MyAccount/my-account-preview.js';
import EditAccount from './components/MyAccount/edit-account.js';
import EditPhoneVerify from './components/MyAccount/edit_phoneVerify.js';
import EditZipcodes from './components/MyAccount/zipcodes';

import NotificationSettings from './components/MyAccount/notification-setting.js';


import ProjectSearchPage from './components/ProjectSearch/project-search';
import ProjectPlaceBid from './components/ProjectSearch/project-place-bid';
import ProjectBidPlaced from './components/ProjectSearch/project-bid-placed';
import ImageView from './components/ProjectSearch/image-view';
import ProjectMessage from './components/ProjectSearch/project-message';
import ProjectReview from './components/ProjectSearch/project-review';
import ProjectEditBid from './components/ProjectSearch/edit-bid';

import FilterSearch from './components/Filter/filter-search';

import BidsPending from './components/Bid/bids-pending.js';

import NotificationsScreen from './components/Notifications/notification-screen.js';

import MessageListing from './components/Chat/messageListing.js';

import CustomerProfile from './components/profile/customerProfile.js';

import TestSearch from './components/Filter/testsearch.js';

import BuySubscriptionScreen from './components/subscription/buySubscription.js';
import AddCard from './components/subscription/addCard.js';
import CancelSubscription from './components/subscription/cancelSubscription';

import CompleteAccountStep3 from './components/CompleteInfo/complete-account-step3';
import CompleteAccountStep4 from './components/CompleteInfo/complete-account-step4';
import CompleteAccountStep5 from './components/CompleteInfo/complete-account-step5';


import BusinessServiceAreaAdd from './components/IndependentComponent/business-service-area-add';
import BusinessServiceAreaEdit from './components/IndependentComponent/business-service-area-edit';











//import MessageListing from './components/Chat/messageListing.js';


export default class App extends Component<Props> {

  constructor(props) {
    super(props);
    this.state = {
      user_status: "",
      user_id: "",
      screenName: 'ProjectSearchScreen',
      //screenName: 'DemoScreen',
      props: {},
      fcm_Token: ""
    }
  }

  componentWillUnmount() {
    this._messageListener();
    this._notificationOpenedListener();
    this._notificationForeGroundListner();
  }

  componentWillMount() {
    firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          AsyncStorage.setItem('fcm_token', fcmToken);
          this.setState({
            fcm_Token: fcmToken
          })
          console.log("fcm token" + fcmToken);
        }
      });

  }

  componentDidMount() {
    AsyncStorage.multiGet([
      'user_status',
      'user_id',]).then(values => {
        this.setState({
          user_status: values[0][1],
          user_id: values[1][1],
        }, this._checkUser.bind(this));
      });

  }
  _checkUser() {
    if (this.state.user_status == "1") {

      firebase.firestore().collection("users").doc(this.state.user_id)
        .get()
        .then(doc => {
          if (doc.exists) {
            doc.data().lastname
              ? AsyncStorage.setItem(
                "userName",
                doc.data().firstname + " " + doc.data().lastname
              )
              : AsyncStorage.setItem("userName", doc.data().firstname);

            doc.data().imageLink
              ? AsyncStorage.setItem("imageLink", doc.data().imageLink)
              : AsyncStorage.setItem("imageLink", " ");

            AsyncStorage.setItem(
              "userBusinessName",
              doc.data().BusinessInfo.CName
            );
            doc.data().SubcriptionType ?
              AsyncStorage.setItem(
                "SubcriptionType",
                doc.data().SubcriptionType
              )
              : AsyncStorage.setItem("SubcriptionType", "Free");
          }

          else {
            console.log("No such document!");
          }
        })
        .catch(eror => {
          console.log("Error getting document:" + error);
        });
      this._checkSubscription(this.state.user_id);
      this._saveTokenFirestore(this.state.user_id);
    }
    else {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({
          routeName: 'SplashPageScreen',
          // routeName: 'BusinessServiceAreaEdit',
        })],
      });
      this.props.navigation.dispatch(resetAction);
    }
  }

  async _checkSubscription(uid) {
    firebase.firestore().collection("proMonthlySubscriptionDetails").doc(uid)
      .get()
      .then(doc => {
        if (doc.exists) {
          AsyncStorage.setItem("SubcriptionStatus", doc.data().status);
        }
        else {
          console.log("No such document!");
        }
      })
      .catch(eror => {
        console.log("Error getting document:" + error);
      });
  }
  async _saveTokenFirestore(uid) {
    // alert(this.state.fcm_Token);
    console.log("fcm token for save" + this.state.fcm_Token);
    if (this.state.user_status == "1") {
      await firebase
        .firestore()
        .collection("sp_tokens")
        .doc(this.state.fcm_Token + "_" + uid)
        .set({
          uid: uid,
          fcm_token: this.state.fcm_Token,
          device_id: DeviceInfo.getUniqueID(),
          device_brand: DeviceInfo.getBrand(),
          deviceModel: DeviceInfo.getModel()
        })
        .then(user => {
          // alert("done");
          console.log("token  data written successfully");
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({
              routeName: this.state.screenName,
              params:
              {
                //uid: this.state.user_id
              }

            })],
          });
          this.props.navigation.dispatch(resetAction);

          //this.props.navigation.navigate('MyAccountScreen', { uid: this.state.user_id });  
        })
        .catch(error => {
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({
              routeName: this.state.screenName,
              params:
              {
                //uid: this.state.user_id
              }

            })],
          });
          this.props.navigation.dispatch(resetAction);
          console.log("Error writing token  data ");
        });
    }
    else {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({
          routeName: 'SplashPageScreen',
        })],
      });
      this.props.navigation.dispatch(resetAction);
    }

  }

  _notificationForeGroundListner() {
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      this.showAlert1(notification);
    });
  }



  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */

    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      console.log("listen");
      this.showAlert(title, body);
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      console.log("back ground");
      notification++;
      this.showAlert(title, body);
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      console.log("closed");
      notification++;
      this.showAlert(title, body);
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log(JSON.stringify(message));
    });
  }



  _notificationOpenedListener() {
    firebase.notifications().onNotificationOpened((notificationOpen) => {
      this.setState({
        screenName: notificationOpen.notification.data.navigate,
        props: { uid: this.state.user_id, selectedOption: notificationOpen.notification.data.option ? notificationOpen.notification.data.option : '' }
      })
      //const { title, body } = notificationOpen.notification;
      this.showAlert(notificationOpen.notification);
    }
    );
  }





  _messageListener() {
    firebase.messaging().onMessage((message) => {
      //process data message
      //alert(message);
      console.log('notification massage : ' + JSON.stringify(message));
    });
  }

  async showAlert1(notification1) {
    if (this.state.user_status == "1") {
      // Build a channel
      const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
        .setDescription('My apps test channel');

      if (notification1.data.navigate == 'BidsPendingScreen') {
        const notification = new firebase.notifications.Notification()

          .setNotificationId('notificationId')
          .setTitle(notification1.title)
          .setBody(notification1.body)
          .setSound('default')
          .setData({
            navigate: notification1.data.navigate,
            option: notification1.data.option,
          });
        await firebase.notifications().android.createChannel(channel);
        notification
          .android.setChannelId(channel.channelId);
        //firebase.notifications().android.createChannel(channel);
        firebase.notifications().displayNotification(notification)
      }
      else {
        const notification = new firebase.notifications.Notification()

          .setNotificationId('notificationId')
          .setTitle(notification1.title)
          .setBody(notification1.body)
          .setSound('default')
          .setData(notification1.data);
        await firebase.notifications().android.createChannel(channel);
        notification
          .android.setChannelId(channel.channelId);
        //firebase.notifications().android.createChannel(channel);
        firebase.notifications().displayNotification(notification)
      }


    }
  }

  showAlert(title) {

    if (this.state.user_status == "1") {
      console.log(title.data.option)
      if (title.data.navigate == 'BidsPendingScreen') {
        this.props.navigation.navigate(title.data.navigate, { uid: this.state.user_id, selectedOption: title.data.option ? title.data.option : '' })
      }
      else {
        this.props.navigation.navigate(title.data.navigate, title.data)
      }
    }
  }







  render() {
    return (
      <View>
      </View>
    );
  }
}


const screens = StackNavigator({
  AppScreen: { screen: App },
  HomeScreen: { screen: Home },

  SignUpScreen: { screen: SignUp },
  SigninConfirmScreen: { screen: SigninConfirm },
  SigninWithEmailScreen: { screen: SigninWithEmail },
  ForgotPasswordScreen: { screen: ForgotPassword },


  CreateAccountStep1Screen: { screen: CreateAccountStep1 },
  CreateAccountStep2Screen: { screen: CreateAccountStep2 },
  CreateAccountStep2FbScreen: { screen: CreateAccountStep2Fb },
  CreateAccountStep3Screen: { screen: CreateAccountStep3 },
  CreateAccountStep4Screen: { screen: CreateAccountStep4 },
  CreateAccountStep5Screen: { screen: CreateAccountStep5 },


  MyAccountScreen: { screen: MyAccount },
  BusinessInfoEditScreen: { screen: BusinessInfoEdit },
  BusinessSummaryEditScreen: { screen: BusinessSummaryEdit },
  ServiceEditScreen: { screen: ServiceEdit },
  ChangePasswordScreen: { screen: ChangePassword },
  AccountSettingsScreen: { screen: AccountSettings },
  MyAccountPreviewScreen: { screen: MyAccountPreview },
  EditAccountScreen: { screen: EditAccount },
  EditPhoneVerifyScreen: { screen: EditPhoneVerify },
  SplashPageScreen: { screen: SplashPage },
  NotificationSettingsScreen: { screen: NotificationSettings },
  EditZipcodesScreen: { screen: EditZipcodes },



  ProjectSearchScreen: { screen: ProjectSearchPage },
  ProjectPlaceBidScreen: { screen: ProjectPlaceBid },
  ProjectBidPlacedScreen: { screen: ProjectBidPlaced },
  ImageViewScreen: { screen: ImageView },
  ProjectMessageScreen: { screen: ProjectMessage },
  ProjectReviewScreen: { screen: ProjectReview },
  ProjectEditBidScreen: { screen: ProjectEditBid },


  FilterSearchScreen: { screen: FilterSearch },

  BidsPendingScreen: { screen: BidsPending },

  NotificationsScreen: { screen: NotificationsScreen },

  MessageListingScreen: { screen: MessageListing },

  CustomerProfileScreen: { screen: CustomerProfile },

  TestProfileScreen: { screen: TestSearch },


  BuySubscriptionScreen: { screen: BuySubscriptionScreen },
  AddCardScreen: { screen: AddCard },
  CancelSubscriptionScreen: { screen: CancelSubscription },

  CompleteAccountStep3Screen: { screen: CompleteAccountStep3 },
  CompleteAccountStep4Screen: { screen: CompleteAccountStep4 },
  CompleteAccountStep5Screen: { screen: CompleteAccountStep5 },
  BusinessServiceAreaAdd: { screen: BusinessServiceAreaAdd },
  BusinessServiceAreaEdit: { screen: BusinessServiceAreaEdit },


},
  // {
  //   initialRouteName: 'SplashPageScreen',
  //   headerMode: 'none'
  // },
  {
    navigationOptions: {
      header: null,
    },
  });


AppRegistry.registerComponent(appName, () => screens);
