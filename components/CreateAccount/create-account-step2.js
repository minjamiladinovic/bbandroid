// Create account page 2
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/verify-phone-number.png

import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  AsyncStorage,
  Button,
  ActivityIndicator,
  Platform,
  Dimensions,
  ImageBackground,
  ScrollView,
  Linking,
  StatusBar,
  Alert,
  BackHandler,
} from "react-native";

import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import firebase from "react-native-firebase";
import CommonTasks from "../../common-tasks/common-tasks";
import KeyboardSpacer from "react-native-keyboard-spacer";
import { TextInputMask } from "react-native-masked-text";

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get("window");
const db = firebase.firestore();

export default class CreateAccountStep2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
      phone: this.props.navigation.state.params.phone,
      uid: this.props.navigation.state.params.uid,
      loginStatus: this.props.navigation.state.params.loginStatus,
      phoneNumberFormat: this.props.navigation.state.params.phoneNumberFormat,
      isLoading: false,
      id: "",
      verfication_statement: false
    };
  }
  componentDidMount() {
    this._phoneVerify();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  handleBackButton() {
    return true;
  }

  gotoNextStep() {
    //this.props.navigation.navigate('CreateAccountStep3Screen');
    if (this.state.code === "" || this.state.code.trim() === "") {
      CommonTasks._displayToast("Please enter valid code");
      this.inputCode.focus();
      return false;
    }
    this._codeConfirm();
  }

  _checkPhone() {
    var phone = this.state.phone;
    if (this.state.phone.includes("+")) {
      //alert(true);
    } else {
      //alert(false);
      if (this.state.phone.length == 11) {
        this.setState({
          phone: '+' + this.state.phone

        }, this._phoneVerify.bind(this))
      }
      else if (this.state.phone.length == 10) {
        this.setState({
          phone: '+1' + this.state.phone

        }, this._phoneVerify.bind(this))
      }
    }
  }

  _phoneVerify() {
    console.log("phone no" + this.state.phone);
    this.setState({ isLoading: true });
    firebase
      .auth()
      .verifyPhoneNumber(this.state.phone)
      .then(credential => {
        //this.setState({ isLoading: false });
        this.setState({
          id: credential.verificationId,
          isLoading: false,
          verfication_statement: true
        });
        console.log(credential);
        alert(
          "A verification code has been sent to " + this.state.phoneNumberFormat
        );
      }) // onVerificationCompleted
      .catch(err => {
        this.setState({ isLoading: false, verfication_statement: false });
        console.log(err);
        var errorCode = err.code;
        var errorMessage = err.message;
        console.log(errorCode + "-" + errorMessage);
        //CommonTasks._displayToast(errorMessage);
        //alert(errorMessage);
        alert("Invalid phone number");
      });
  }

  _codeConfirm() {
    this.setState({ isLoading: true });
    const credential = firebase.auth.PhoneAuthProvider.credential(
      this.state.id,
      this.state.code
    );
    console.log("credential" + JSON.stringify(credential));
    firebase
      .auth()
      .signInWithCredential(credential)
      .then(data => {
        this.setState({ isLoading: false });
        console.log("confirm" + data);
        db.collection("users")
          .doc(this.state.uid)
          .update({
            phone: this.state.phone,
            phVerify_status: "Y"
          })
          .then(user => {
            this.setState({ isLoading: false });
            //console.log("update data"+user);
          })
          .catch(err => {
            this.setState({ isLoading: false });
            console.log("update err" + err);
          });
        CommonTasks._displayToast("Successfully verified Phone Number");
        this.props.navigation.replace("CreateAccountStep3Screen", {
          uid: this.state.uid,
          loginStatus: this.state.loginStatus
        });
      })
      .catch(err => {
        this.setState({ isLoading: false });
        console.log("err" + err);
        var errorMessage = err.message;
        CommonTasks._displayToast(err.message);
      });
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            style={styles.backContainer}
            onPress={() =>
              this.props.navigation.navigate("SigninWithEmailScreen")
            }
          >
            <Ionicons name="ios-arrow-back" style={styles.headerImage} />
            <Text style={styles.signinText}>Sign Out</Text>
          </TouchableOpacity>

          <Text style={styles.headerText}>Verify Phone</Text>
          <Text style={styles.nextText} onPress={() => this.gotoNextStep()}>
            Next
          </Text>
        </View>

        <View style={styles.verificationContainer}>
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            underlineColorAndroid="transparent"
            placeholder="Verification code"
            placeholderTextColor="#ababab"
            returnKeyType="done"
            autoCorrect={false}
            autoCapitalize="none"
            // onSubmitEditing={() => this.inputLname.focus()}
            onChangeText={input => this.setState({ code: input })}
            ref={input => (this.inputCode = input)}
          />
        </View>

        {this.state.verfication_statement == true ? (
          <Text style={styles.verificationText}>
            A verification code was sent &nbsp;
            <Text style={{ fontWeight: "bold", fontSize: 16 }}>
              {this.state.phoneNumberFormat}.
            </Text>
          </Text>
        ) : null}

        <View style={styles.verificationContainer}>
          {/* <TextInput
                        style={styles.textInput}
                        keyboardType='phone-pad'
                        underlineColorAndroid="transparent"
                        placeholder="Phone number"
                        placeholderTextColor="#ababab"
                        returnKeyType='done'
                        autoCorrect={false}
                        autoCapitalize="none"
                        // onSubmitEditing={() => this.inputLname.focus()}
                        onChangeText={(input) => this.setState({ phone: input })}
                        ref={((input) => this.inputPhone = input)}
                    /> */}

          <TextInputMask
            placeholder="Phone number"
            returnKeyType="done"
            value={this.state.phoneNumberFormat}
            refInput={ref => (this.inputPhone = ref)}
            onChangeText={phoneNumberFormat => {
              let phoneNumber = phoneNumberFormat
                .toString()
                .replace(/\D+/g, "");
              this.setState({
                phoneNumberFormat: phoneNumberFormat,
                phone: phoneNumber,
                verfication_statement: false
              });
            }}
            onFocus={() => this.setState({ selectedField: "phone" })}
            type={"cel-phone"}
            maxLength={
              this.state.phoneNumberFormat.toString().startsWith("1") ? 18 : 16
            }
            options={
              this.state.phone.startsWith("1")
                ? {
                  dddMask: "9 (999) 999 - "
                }
                : {
                  dddMask: "(999) 999 - "
                }
            }
            style={styles.textInput}
          />
          <TouchableOpacity onPress={() => this._checkPhone()}>
            <Text style={styles.sendCodeText}>Resend Code</Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.replace("CreateAccountStep3Screen", {
              uid: this.state.uid,
              loginStatus: this.state.loginStatus
            })
          }
          style={{ marginTop: 20 }}
        >
          <Text style={styles.sendCodeText}>Skip</Text>
        </TouchableOpacity>

        {this.state.isLoading == true ? (
          <View
            style={{
              position: "absolute",
              height: screenSize.height,
              width: screenSize.width,
              backgroundColor: "rgba(255, 255, 255, 0.2)"
            }}
          >
            <View style={styles.activity_sub}>
              <ActivityIndicator
                size="large"
                color="#D0D3D4"
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  height: 50
                }}
              />
            </View>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  headerContainer: {
    justifyContent: "space-between",
    flexDirection: "row",
    backgroundColor: "#3A4958",
    paddingVertical: 10,
    alignItems: "center"
  },

  backContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },

  signinText: {
    fontFamily: regularText,
    fontSize: 18,
    color: "#007aff",
    marginLeft: 10
  },

  headerImage: {
    fontSize: 30,
    color: "#007aff",
    fontWeight: "bold",
    marginHorizontal: 3
  },

  headerText: {
    fontFamily: boldText,
    color: "#1e1e1e",
    fontSize: 18
  },

  nextText: {
    color: "#007aff",
    fontFamily: boldText,
    fontSize: 18,
    marginRight: 10
  },

  textInput: {
    borderBottomColor: "#8e8e93",
    borderBottomWidth: 0.7,
    // paddingLeft : 15,
    marginLeft: 15,
    fontFamily: regularText,
    fontSize: 17
  },

  verificationContainer: {
    backgroundColor: "white",
    marginVertical: 20
  },

  verificationText: {
    fontFamily: regularText,
    fontSize: 15,
    marginLeft: 15
  },

  sendCodeText: {
    textAlign: "center",
    fontFamily: regularText,
    fontSize: 20,
    color: "#007aff",
    marginVertical: 10
  },
  activity_sub: {
    position: "absolute",
    top: screenSize.height / 2,
    backgroundColor: "black",
    width: 50,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 10,
    //elevation:5,
    ...Platform.select({
      android: { elevation: 5 },
      ios: {
        shadowColor: "#999",
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5
      }
    }),
    height: 50,
    borderRadius: 10
  }
});
