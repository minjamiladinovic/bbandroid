import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';
import CommonTasks from '../../common-tasks/common-tasks';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();


export default class CancelSubscription extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            uid: this.props.navigation.state.params.uid,
            cancel_reason: '',
            isLoading: false
        }
    }


    _cancelSubscription() {
        if (this.state.cancel_reason === "" || this.state.cancel_reason.trim() === " ") {
            CommonTasks._displayToast("Please enter reason for your subscription cancelation");
            return false;
        }
        this.setState({
            isLoading:true
        })
        return fetch('https://us-central1-pro-in-your-pocket.cloudfunctions.net/cancelMonthlySubscription?docId=' + this.state.uid + '&message=' + this.state.cancel_reason,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            })
            .then(response => response.json())
            .then(json => {
                console.log(json);
                //alert(JSON.stringify(json));
                this.setState({
                    isLoading:false
                })
                if (json.status == "success") {
                    alert("We are super sorry to see you go. We hope to see you again soon!");
                    AsyncStorage.setItem("SubcriptionType", "Pending");
                    AsyncStorage.setItem("SubcriptionStatus",'cancelled');
                    this.props.navigation.navigate("ProjectSearchScreen");
                }
            })


    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>

                    <TouchableOpacity style={styles.backContainer}
                        onPress={() => this.props.navigation.goBack()}>
                        <Text style={styles.signinText}>Close</Text>
                    </TouchableOpacity>


                    <Text style={styles.headerText}>Cancel Subscription</Text>

                    <TouchableOpacity style={{ width: 50 }}>

                    </TouchableOpacity>

                    {/* {(this.state.summary == "") ?
                        (<Text style={[styles.nextText,{color:'#3A4958'}]} onPress={() => this.gotoNextStep()}>Next</Text>)
                        :
                        (<Text style={[styles.nextText,{color:'#007aff'}]} onPress={() => this.gotoNextStep()}>Next</Text>)} */}
                </View>

                <View style={{ backgroundColor: "#ffff" }}>
                    <Text style={[styles.verificationText, { marginTop: 20 }]}>Write Note</Text>

                    <View style={styles.verificationContainer}>
                        <TextInput
                            style={styles.textInput}
                            keyboardType='default'
                            multiline={true}
                            numberOfLines={10}
                            underlineColorAndroid="transparent"
                            placeholder="Why do you want to cancel this subscription?"
                            placeholderTextColor="#ababab"
                            returnKeyType='done'
                            autoCorrect={false}
                            autoCapitalize="none"
                            onChangeText={(input) => this.setState({ cancel_reason: input })}
                            ref={((input) => this.cancel_reason = input)}
                        />
                    </View>

                    <TouchableOpacity
                        onPress={() => this._cancelSubscription()}
                        style={{ width: screenSize.width - 20, backgroundColor: "#3A4958", padding: 10, alignSelf: "center", justifyContent: "center", alignItems: "center", marginVertical: 10 }}>
                        <Text style={{ color: "#ffff", fontFamily: boldText, fontSize: 15 }}>CANCEL SUBSCRIPTION</Text>
                    </TouchableOpacity>

                </View>
                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>)
                    : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#3A4958',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    textInput: {
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 15,
        textAlignVertical: 'top',
        width: screenSize.width - 20,
        height: 200
    },

    verificationContainer: {
        backgroundColor: '#3A4958',
        width: screenSize.width - 20,
        alignSelf: "center",
        borderRadius: 5,
        marginVertical: 20,
        elevation: 1
    },

    verificationText: {
        fontFamily: regularText,
        fontSize: 17,
        marginHorizontal: 15,
        color: "#000"
    },

    sendCodeText: {
        textAlign: 'center',
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff',
        marginVertical: 10
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
});