import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    FlatList,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Alert,
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';
import Footer from '../footer/footer';
import {
    CachedImage,
} from 'react-native-cached-image';
import Feather from 'react-native-vector-icons/Feather';



const regularText = "SF-Pro-Display-Regular";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();


export default class BuySubscriptionScreen extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            //uid: this.props.navigation.state.params.uid,
            isLoading: false,
            Data: [],
            notificationData: [],
            notificationDataLength: 0,
            projectId: this.props.navigation.state.params.projectId,
            owner: this.props.navigation.state.params.ownerId,
            bidderId: this.props.navigation.state.params.bidderId,
            description: this.props.navigation.state.params.description,
            serviceId: this.props.navigation.state.params.serviceId,
            createdAt: this.props.navigation.state.params.createdAt,
            bidderIdList: this.props.navigation.state.params.bidderIdList ? this.props.navigation.state.params.bidderIdList : [],
            imageLink: this.props.navigation.state.params.imageLink,
            ownerName: this.props.navigation.state.params.ownerName,
            postalCode: this.props.navigation.state.params.postalCode,
            timeString: this.props.navigation.state.params.timeString,
            Pimage: this.props.navigation.state.params.Pimage,
            navigateTo:this.props.navigation.state.params.navigateTo,
            projectStatus:this.props.navigation.state.params.projectStatus,
            bidPrice:this.props.navigation.state.params.bidPrice,
            SubcriptionStatus:''
        }
    }

    componentWillMount()
    {
        AsyncStorage.getItem('SubcriptionStatus').then(value => this.setState({SubcriptionStatus: value }));
    }


    _goToAddCard() {
        this.props.navigation.replace('AddCardScreen',
            {
                projectId: this.state.projectId,
                ownerId: this.state.ownerId,
                description: this.state.description,
                serviceId: this.state.serviceId,
                createdAt: this.state.createdAt,
                bidderIdList: this.state.bidderIdList,
                imageLink: this.state.imageLink,
                ownerName: this.state.ownerName,
                postalCode: this.state.postalCode,
                timeString: this.state.timeString,
                Pimage: this.state.Pimage,
                bidderId: this.state.bidderId,
                navigateTo:this.state.navigateTo,
                projectStatus:this.state.projectStatus,
                bidPrice:this.state.bidPrice,
                SubcriptionStatus:this.state.SubcriptionStatus
            })
    }


    render() {

        return (
            <ImageBackground source={require('../../assets/images/bgSub.png')} style={{ width: '100%', resizeMode: 'cover', height: '100%', alignItems: 'center', justifyContent: 'center', }}>
                <TouchableOpacity onPress={() => this.props.navigation.pop()} style={{ position: 'absolute', top: 20, left: 10, padding: 10 }}>
                    <Ionicons name="ios-arrow-back" style={styles.backButton} />
                </TouchableOpacity>


                <View style={styles.container1}>
                    <View style={styles.container2}>
                        <View style={styles.container}>
                            <Text style={styles.SubscriptionPlan}>Subscription Plan</Text>
                            {(this.state.SubcriptionStatus!='cancelled')?
                            (<Text style={styles.offerText}>Start a
                                    <Text style={[styles.offerText,{color: '#3A4958'}]}> 30 day free trial </Text>
                                    with no restrictions
                            </Text>)
                            :
                            null}
                            <Text style={styles.priceText}>$ <Text style={styles.priceValueText}>24.00</Text><Text style={{ fontSize: 17 }}>/Month</Text></Text>
                            <View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Feather name='check' color='#fff' size={15} style={{ backgroundColor: '#3A4958', borderRadius: 15, padding: 2, marginBottom: 10 }} />
                                    <Text style={styles.featureText}>Unlimited project bids</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Feather name='check' color='#fff' size={15} style={{ backgroundColor: '#3A4958', borderRadius: 15, padding: 2, marginBottom: 10 }} />
                                    <Text style={styles.featureText}>Unlimited project wins</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Feather name='check' color='#fff' size={15} style={{ backgroundColor: '#3A4958', borderRadius: 15, padding: 2, marginBottom: 10 }} />
                                    <Text style={styles.featureText}>Unlimited email support</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Feather name='check' color='#fff' size={15} style={{ backgroundColor: '#3A4958', borderRadius: 15, padding: 2, marginBottom: 10 }} />
                                    <Text style={styles.featureText}>No hidden fees</Text>
                                </View>
                            </View>
                            <TouchableOpacity style={styles.buyNow}
                                onPress={() => this._goToAddCard()}
                            //onPress={() => this.props.navigation.replace('AddCardScreen')}
                            >
                                <Text style={styles.buyNowText}>Buy Now</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = {
    container: {
        height: 378,
        width: 256,
        position: 'absolute',
        top: -7,
        left: 7,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    container2: {
        height: 374,
        width: 270,
        position: 'absolute',
        top: -7,
        left: 7,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255,0.7)'
    },
    container1: {
        height: 350,
        width: 284,
        position: 'absolute',
        top: 100,
        // left : 53,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255,0.6)'
    },
    priceText: {
        fontFamily: 'regularText',
        fontSize: 40,
        fontWeight: 'bold',
        marginBottom: 20
    },
    priceValueText: {
        fontFamily: 'regularText',
        color: '#3A4958'
    },
    featureText: {
        fontFamily: 'regularText',
        color: '#4B4B4B',
        marginLeft: 15
    },
    buyNow: {
        backgroundColor: '#3A4958',
        padding: 10,
        paddingLeft: 50,
        paddingRight: 50,
        borderRadius: 30,
        marginTop: 20
    },
    buyNowText: {
        fontFamily: 'regularText',
        fontSize: 21,
        color: '#fff'
    },
    SubscriptionPlan: {
        fontFamily: 'regularText',
        fontSize: 17,
        fontWeight: 'bold',
        borderBottomWidth: 3,
        borderBottomColor: '#D9D9D9',
        paddingLeft: 25,
        paddingRight: 25,
        paddingBottom: 11,
        marginBottom: 15
    },
    headerContainer: {
        backgroundColor: '#3A4958',
        paddingTop: Platform.OS === 'ios' ? 50 : 30,
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: -5
    },
    headerText: {
        fontFamily: boldText,
        color: 'white',
        fontSize: 22,
        marginLeft: -40
    },
    timeText: {
        fontFamily: regularText,
        fontSize: 14,
        marginTop: 10
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
    backButton: {
        fontSize: 25,
        color: '#1e1e1e',
        paddingLeft: 10
    },
    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },
    offerText: {
        fontFamily: 'regularText',
        fontSize: 13,
        fontWeight: "600"
    }
};


