
/* Project search page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/feed-project-detail-deave-review-rating-selected.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    FlatList,
    StatusBar,
    BackHandler
} from 'react-native';

import StarRating from 'react-native-star-rating';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CommonTasks from '../../common-tasks/common-tasks';
import Footer from '../footer/footer';
import firebase from 'react-native-firebase';
import VerifiedLabel from '../IndependentComponent/verifiedLabel';



const regularText = "SF-Pro-Display-Regular";
const italicText = "SF-Pro-Display-LightItalic";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class CustomerProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            averageRating: 0,
            profileId : this.props.navigation.state.params.profileId,
            profileName : this.props.navigation.state.params.profileName,
            profileImage : this.props.navigation.state.params.profileImage,
            email_verify_status: this.props.navigation.state.params.email_verify_status,
            dataLength: 0,
            data1: [],
            data: [],
            serviceData: CommonTasks.serviceList,
            isLoading: false,
            //user_id: this.props.navigation.state.params.uid,
        }
    }

    componentDidMount() {
        //AsyncStorage.getItem('user_id').then(value => this.setState({ user_id: value }));
        this._getReviews(this.state.profileId)
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        this.props.navigation.pop();
         return true;
       }


    onStarRatingPress(rating) {
    //    console.log(rating);
        this.setState({
            starCount: rating
        });
    }



    _getReviews(ownerId)
    {
        this.setState({ isLoading: true })
        var totalRating=0;
        db.collection("review")
        .where("ownerId", "==", ownerId)
        .get().then((data) => {
            //console.log("data"+data.docs);
            var array = [];

            data.docs.forEach(doc => {
                //console.log('Pimage : '+doc.data().Pimage)
               
                
                totalRating = totalRating+ doc.data().rating;
                console.log(totalRating)
                var obj=doc.data();

                


                array.push(obj);}


            )
            console.log('array length : '+ array.length)
            if(array.length==0)
            {
                this._stopLoading()
            }
            else{
                this.setState({
                averageRating : totalRating/array.length,
                dataLength: array.length,
                data1: array,
            }, this._getReviewerName.bind(this))
            }
            

        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log(err)
        })
    }


    _getReviewerName() {
        var dbData = [];
        var pData = this.state.data1;
         
        pData.forEach(data => {
              var name
            db.collection("users").doc(data.userId).get()
              .then(doc => {
                if (doc.exists) {
                    var projectData=data// project data

                    projectData.firstname=doc.data().firstname;
                    projectData.lastname=doc.data().lastname;
                    projectData.imageLink=doc.data().imageLink;
                    
                    if(projectData.imageLink)
                    {
                        if(projectData.imageLink=='')
                        {
                            projectData.imageLink=null;
                        }
                    }
                    else{
                        projectData.imageLink=null;
                    }
                    if(projectData.firstname==''|| projectData.firstname == undefined)
                    {

                        projectData.userName='Pro';
                    }
                    else
                    {
                        if(projectData.lastname)
                        {
                            if(projectData.lastname=='')
                            {
                                projectData.lastname=null;
                            }
                        }
                        else{
                            projectData.lastname=null;
                        }

                        //nameformating
                        var nameflag=projectData.firstname.indexOf(' ');
                        var nameflag1=projectData.firstname.indexOf('  ');
                        if(nameflag!=-1)
                        {
                            var nameArray = projectData.firstname.split(' ')
                            var name=nameArray[0]+' '+nameArray[1];
                            projectData.userName=name;
                        }
                        else if(nameflag1!=-1) {
                            var nameArray = projectData.firstname.split('  ')
                            var name=nameArray[0]+' '+nameArray[1];
                            projectData.userName=name;
                        }
                        else{
                            
                            if(projectData.lastname!=null)
                            {
                                var name=projectData.firstname+' '+projectData.lastname;
                                projectData.userName=name;
                            }
                            else {
                                projectData.userName=projectData.firstname;
                            }
                        }
                    }
                  dbData.push(projectData);
                  if(dbData.length==this.state.dataLength)
                    {
                        dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));       
                        
                        this.setState({
                            data: dbData,
                            
                        },this._stopLoading.bind(this));
                    }
                }
                 else {
                    var projectData=data
                    name = 'Pro';
                    //console.log(name);
                    projectData.userName=name;
                    projectData.imageLink=null;
                    dbData.push(projectData);
                    
                    if(dbData.length==this.state.dataLength)
                    {
                        dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));
                        this.setState({
                            data: dbData,
                            
                        },this._stopLoading.bind(this));
                    }
                }
              })
              .catch(err => {
                console.log(err);
                var projectData=data
                name = 'Pro';
                //console.log(name);
                projectData.userName=name;
                projectData.imageLink=null;

                dbData.push(projectData);
                if(dbData.length==this.state.dataLength)
                {
                    dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));
                    this.setState({
                        data: dbData,
                        
                    },this._stopLoading.bind(this));
                }
              });
              
              
              
          })
          
          
          
        
      }


    _stopLoading() {
        this.setState({ isLoading: false, refreshing:false });
    }
   

    

    _showDate(date) {
        var dateValue =new Date(date)
        //console.log(dateValue)
        // var hours = dateValue.getHours();
        // var minutes = dateValue.getMinutes();
        // var ampm = hours >= 12 ? 'pm' : 'am';
        // hours = hours % 12;
        // hours = hours ? hours : 12; // the hour '0' should be '12'
        // minutes = minutes < 10 ? '0'+minutes : minutes;
        // hours = hours < 10 ? '0'+hours : hours;
        // var strTime = hours + ':' + minutes + ' ' + ampm;

        var dateReadable = dateValue.toDateString().trim();

        

        return (
               <Text >{dateReadable}</Text>
            
        )
    }

   

    render() {
        return (
            <View style={styles.container}>
            
            <View style={styles.ProfileView}>

                <TouchableOpacity onPress={() => this.props.navigation.pop ()} style ={{position : 'absolute',top : 20,left : 10,padding : 10}}>
                    <Ionicons name="ios-arrow-back" style={styles.backButton} />
                </TouchableOpacity>

                <Image
                    source={this.state.profileImage?{
                            uri:
                            this.state.profileImage
                        }:
                    require('../../assets/images/userOwner.png')}
                    style={styles.avatorImage}
                />
                <Text style={styles.profileName}>{this.state.profileName}</Text>
                <View style={{flexDirection : 'row',alignItems : 'center'}} >
                <StarRating
                    disabled={true}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={5}
                    rating={this.state.averageRating}
                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                    fullStarColor={'#3A4958'}
                    emptyStarColor={'#3A4958'}
                    starSize={35}
                    starStyle={{ paddingLeft :2 }}
                />
                <Text style={{fontSize : 20,color : '#8e8e93',fontFamily : regularText}}> {this.state.dataLength} Reviews</Text>    
                </View>
                
                <VerifiedLabel projectVerified={this.state.email_verify_status} type={'User'} />
            </View> 
            {(this.state.dataLength == 0)?
            null
            :
            <ScrollView  showsVerticalScrollIndicator ={false}>
            <FlatList
                data={this.state.data}
                
                renderItem={({ item }) => (
                <View style={{width:screenSize.width, paddingLeft : 10,paddingRight : 10, backgroundColor : 'white',marginBottom : 2,borderBottomWidth : 0.3, borderBottomColor: '#999', paddingVertical : 2}}>
                    <View style={styles.imageHeadingContainer}>
                        <View style={{ flexDirection: "row" }}>
                        <Image
                        source={
                            item.imageLink?{
                            uri:
                            item.imageLink
                        }:
                        require('../../assets/images/userOwner.png')}
                        style={styles.avatorImageSmall}
                        />
                        <View style={{ marginLeft: 10 ,flexDirection : 'row',alignItems : 'center',width:screenSize.width-90}} >
                            <View>
                                <Text style={styles.ReviewerName}>{item.userName}</Text>
                                <StarRating
                                    disabled={true}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={item.rating}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'#3A4958'}
                                    emptyStarColor={'#3A4958'}
                                    starSize={20}
                                    starStyle={{ paddingLeft :2 }}
                                    containerStyle={{ width : 100 ,paddingTop : 2}}
                                />
                            </View>
                            <Text style={styles.timeText}>{this._showDate(item.timeStamp)}</Text>
                            {/* //{this._showDate(item)}  */}
                        </View>
                        </View>
                        
                    </View>
                    <Text style={styles.reviewText}>
                    {item.review}
    
                    </Text>
                </View>
                )}
                keyExtractor={(item, index) => index.toString()}
            />
            </ScrollView> 

            }


               

                {(this.state.isLoading == true) ?
                    (
                        <View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                         <View style={styles.activity_sub}>
                        <ActivityIndicator
                            size="large"
                            color='#D0D3D4'
                            style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                        />
                    </View>   
                        </View>)
                    : null}
                    

            </View>
        );
    }
}

const styles = StyleSheet.create({

    ProfileView: {
        // position : 'absolute',
        // top : 0,
        // left : 0,
        width :  screenSize.width,
        height : 300,
        backgroundColor : 'white',
        marginBottom: 2,
        alignItems : "center",
        justifyContent : 'center',
        borderBottomWidth : 0.3,
        borderBottomColor: '#999'
    },
    container: {
        flex: 1,
        backgroundColor: '#eee',
    },

    topContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center'
    },

    backButton: {
        fontSize: 30,
        color: '#1e1e1e',
        paddingLeft: 10
    },
    ReviewerName : {
        fontSize : 18,
        fontWeight : 'bold',
        fontFamily : regularText,
        marginBottom : 3
    },  

    avatorImage: {
        width: 180,
        height: 180,
        borderRadius: 90,
        borderWidth: 2,
        borderColor: "#D0D3D4",
        marginTop : 10
    },
    avatorImageSmall: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 2,
        borderColor: "#D0D3D4",
        marginTop : 10
    },

    titleText: {
        fontFamily: regularText,
        fontWeight: 'bold',
        color: '#1e1e1e',
        fontSize: 16,
        maxWidth: screenSize.width - 100
    },

    avatorContainer: {
        flexDirection: 'row',
        marginTop: 10,
        padding: 10
    },

    
    profileName: {
        fontFamily: regularText,
        fontWeight : 'bold',
        color: '#1e1e1e',
        fontSize: 25,
        marginTop: 10
    },

    starContainer: {
        paddingLeft: 10,
        marginTop: 10,
        borderTopColor: '#8e8e93',
        borderTopWidth: 0.5,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
        paddingVertical: 10
    },

    rateText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#8e8e93',
        marginBottom: 5
    },
    timeText: {
        fontFamily: regularText,
        fontSize: 16,
        color: '#8e8e93',
        position : 'absolute',
        top : 35,
        right : 15
    },

    textInput: {
        marginLeft: 10,
        fontFamily: italicText,
        fontSize: 16,
        flex: 1,
    },

    reviewContainer: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
    },

    placeReviewContainer: {
        backgroundColor: '#3A4958',
        marginHorizontal: 10,
        marginVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        borderRadius: 25,
    },

    reviewText: {
        color: '#555',
        fontFamily: regularText,
        fontSize: 17
        
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
})
{/* <ScrollView>
<View style={styles.topContainer}>
    {/* <TouchableOpacity onPress={() => this.props.navigation.replace('BidsPendingScreen',{uid: this.state.user_id, selectedOption : 'Completed'})}>
        <Ionicons name="ios-arrow-back" style={styles.backButton} />
    </TouchableOpacity> */}
    {/* <View style={styles.avatorContainer}>
        <Image
            source={
            require('../../assets/images/userOwner.png')}
            style={styles.avatorImage}
        />
        <View style={{ marginLeft: 10 }}>
            <Text style={styles.titleText}>abc Xyz&nbsp; </Text>
           
           {/* {this._showDate(this.state.createdAt)} */}
            {/* <Text style={styles.timeText}>Tuesday 10:54 am</Text> */}
        {/* </View>
    </View> */} 
//</View>

{/* <View style={styles.starContainer}>
    <Text style={styles.rateText}>Rate your customer</Text>
    <StarRating
        disabled={false}
        emptyStar={'ios-star-outline'}
        fullStar={'ios-star'}
        halfStar={'ios-star-half'}
        iconSet={'Ionicons'}
        maxStars={5}
        rating={this.state.starCount}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
        fullStarColor={'#007aff'}
        emptyStarColor={'#007aff'}
        containerStyle={{ marginRight: 140 }}
    // starSize={30}
    />
</View> */}

{/* <View style={styles.reviewContainer}>
    <TextInput
        style={styles.textInput}
        multiline={true}
        keyboardType='default'
        underlineColorAndroid="transparent"
        placeholder="Enter a message"
        placeholderTextColor="#ababab"
        returnKeyType='send'
        autoCorrect={false}
        autoCapitalize="none"
        // onSubmitEditing={() => this.inputPassword.focus()}
        onChangeText={(input) => this.setState({ message: input })}
        ref={((input) => this.inputMessage = input)}
    />
</View>

<TouchableOpacity activeOpacity={0.7}
style={styles.placeReviewContainer}
    onPress={() => this._sendReview()}
>
    <Text style={styles.reviewText}>Leave Review</Text>
</TouchableOpacity> */}
//</ScrollView> */}