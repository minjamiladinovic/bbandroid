// Create account page 2
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/verify-phone-number.png

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    BackHandler,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class CompleteAccountStep4 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            summary: '',
            summary: "",
            uid: this.props.navigation.state.params.uid,
            // loginStatus : this.props.navigation.state.params.loginStatus,
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        //ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }

    gotoNextStep() {
        this.setState({ isLoading: true });
        db.collection("users").doc(this.state.uid).set({
            BusinessSummary: this.state.summary
        }, { merge: true }).then((user) => {
            this.setState({ isLoading: false });
            console.log("user data" + user);
            this.props.navigation.replace('CompleteAccountStep5Screen', { uid: this.state.uid });
        }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error);
            var errorCode = err.code;
            var errorMessage = err.message;
            console.log(errorCode + "-" + errorMessage);
            CommonTasks._displayToast(errorMessage);
        })

        // this.props.navigation.navigate('CreateAccountStep5Screen');
        
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.backContainer}
                        onPress={() => this.props.navigation.navigate('SigninWithEmailScreen')}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Sign Out</Text>
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Business Summary</Text>
                    {(this.state.summary == "") ?
                        (<Text style={[styles.nextText, { color: '#c7c7cc' }]} onPress={() => this.gotoNextStep()}>Next</Text>)
                        :
                        (<Text style={[styles.nextText, { color: '#007aff' }]} onPress={() => this.gotoNextStep()}>Next</Text>)}
                </View>

                <View style={styles.verificationContainer}>
                    <TextInput
                        style={styles.textInput}
                        keyboardType='default'
                        multiline={true}
                        numberOfLines={5}
                        underlineColorAndroid="transparent"
                        placeholder="Business summary"
                        placeholderTextColor="#ababab"
                        returnKeyType='done'
                        autoCorrect={false}
                        autoCapitalize="none"
                        // onSubmitEditing={() => this.inputLname.focus()}
                        onChangeText={(input) => this.setState({ summary: input })}
                        ref={((input) => this.inputCode = input)}
                    />
                </View>

                <Text style={styles.verificationText}>Enter a summary about your business. This will display on your public profile to help customers get to know you.</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        //color: '#c7c7cc',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    textInput: {
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17,
        textAlignVertical: 'top',
        height: 100
    },

    verificationContainer: {
        backgroundColor: 'white',
        marginVertical: 20,
        elevation: 1
    },

    verificationText: {
        fontFamily: regularText,
        fontSize: 15,
        marginHorizontal: 15
    },

    sendCodeText: {
        textAlign: 'center',
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff',
        marginVertical: 10
    },
});