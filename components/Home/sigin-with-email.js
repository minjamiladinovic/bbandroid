/* Sign with email page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/sign-in-2.png
 */

import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  AsyncStorage,
  Button,
  ActivityIndicator,
  Platform,
  Dimensions,
  ImageBackground,
  ScrollView,
  Linking,
  StatusBar,
  Alert,
  Keyboard
} from "react-native";

import firebase from "react-native-firebase";
import CommonTasks from "../../common-tasks/common-tasks";
import KeyboardSpacer from "react-native-keyboard-spacer";
import DeviceInfo from "react-native-device-info";

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get("window");
const db = firebase.firestore();

export default class SigninWithEmail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      isLoading: false,
      fcm_Token: ""
    };
  }

  async componentDidMount() {
    await AsyncStorage.getItem("fcm_token").then(value => {
      console.log("login fcm" + value);
      this.setState({ fcm_Token: value })
    }

    );
  }

  gotoMyProfile() {
    //this.props.navigation.navigate('MyAccountScreen');
    this._formValidation();
  }

  _formValidation() {
    if (this.state.email === "" || this.state.email.trim() === "") {
      CommonTasks._displayToast("Please Enter Your email id");
      this.inputEmail.focus();
      return false;
    }
    if (this.state.password === "" || this.state.password.trim() === "") {
      CommonTasks._displayToast("Please Enter Your Password");
      this.inputPassword.focus();
      return false;
    }
    this._onLogin();
  }

  async _saveTokenFirestore(uid) {
    await firebase
      .firestore()
      .collection("sp_tokens")
      .doc(this.state.fcm_Token + "_" + uid)
      .set({
        uid: uid,
        fcm_token: this.state.fcm_Token,
        device_id: DeviceInfo.getUniqueID(),
        device_brand: DeviceInfo.getBrand(),
        deviceModel: DeviceInfo.getModel()
      })
      .then(user => {
        console.log("token  data written successfully");
      })
      .catch(error => {
        console.log("Error writing token  data ");
      });
  }

  _complete_business_info(userId) {
    setTimeout(() => {
      Alert.alert(
        'Business Info is not added',
        'Add it now ?',
        [
          { text: 'Later', onPress: () => { } },
          { text: 'OK', onPress: () => this.props.navigation.navigate("CompleteAccountStep3Screen", { uid: userId }) },//
        ],
        { cancelable: false }
      );
    }, 1000)
  }

  _complete_business_summary(userId) {
    setTimeout(() => {
      Alert.alert(
        'Business summary is not added',
        'Add it now ?',
        [
          { text: 'Later', onPress: () => { } },
          { text: 'OK', onPress: () => this.props.navigation.navigate("CompleteAccountStep4Screen", { uid: userId }) },//
        ],
        { cancelable: false }
      );
    }, 1000)
  }
  _complete_service_type(userId) {
    setTimeout(() => {
      Alert.alert(
        'Services is not added',
        'Add it now ?',
        [
          { text: 'Later', onPress: () => { } },
          { text: 'OK', onPress: () => this.props.navigation.navigate("CompleteAccountStep5Screen", { uid: userId }) },//
        ],
        { cancelable: false }
      );
    }, 1000)
  }


  _onLogin = () => {
    this.setState({ isLoading: true });
    Keyboard.dismiss();
    firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(user => {
        this._saveTokenFirestore(user.user.uid);
        console.log("success" + JSON.stringify(user));
        console.log(user.user.uid);
        AsyncStorage.setItem("user_id", user.user.uid);
        AsyncStorage.setItem("user_status", "1");
        AsyncStorage.setItem("loginStatus", "n");
        firebase
          .firestore()
          .collection("users")
          .doc(user.user.uid)
          .get()
          .then(doc => {
            this.setState({ isLoading: false })
            if (doc.exists) {

              if (doc.data().BusinessInfo == undefined) {
                // alert(doc.data().BusinessInfo)
                // console.log('doc.data().BusinessInfo==undefined)')
                this._complete_business_info(user.user.uid);
              }

              else if (doc.data().BusinessSummary == undefined) {
                // alert(doc.data().BusinessInfo)
                // console.log('doc.data().BusinessInfo==undefined)')
                this._complete_business_summary(user.user.uid);
              }
              else if (doc.data().ServiceType.length == 0 || doc.data().ServiceType == undefined) {
                // alert(doc.data().BusinessInfo)
                // console.log('doc.data().BusinessInfo==undefined)')
                this._complete_service_type(user.user.uid);
              }

              doc.data().lastname
                ? AsyncStorage.setItem(
                  "userName",
                  doc.data().firstname + " " + doc.data().lastname
                )
                : AsyncStorage.setItem("userName", doc.data().firstname);

              doc.data().imageLink
                ? AsyncStorage.setItem("imageLink", doc.data().imageLink)
                : AsyncStorage.setItem("imageLink", " ");

              AsyncStorage.setItem(
                "userBusinessName",
                doc.data().BusinessInfo.CName
              );
              doc.data().SubcriptionType ?
                AsyncStorage.setItem(
                  "SubcriptionType",
                  doc.data().SubcriptionType
                )
                : AsyncStorage.setItem("SubcriptionType", "Free");
              this.props.navigation.replace("ProjectSearchScreen", {
                uid: user.user.uid
              });

            } else {
              console.log("No such document!");
            }
          })
          .catch(eror => {
            //alert(eror)
            this.setState({ isLoading: false });
            CommonTasks._displayToast("Invalid Login Id or Password");
            console.log("Error getting document:" + error);
          });


        //this.props.navigation.navigate('MyAccountScreen',{uid: user.user.uid });
      })
      .catch(error => {
        this.setState({ isLoading: false });
        const { code, message } = error;
        console.log("error" + error);
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorCode + "-" + errorMessage);
        CommonTasks._displayToast(errorMessage);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Sign In</Text>
        </View>

        <ScrollView keyboardShouldPersistTaps="always">
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.textInput}
              keyboardType="email-address"
              underlineColorAndroid="transparent"
              placeholder="Email"
              placeholderTextColor="#ababab"
              returnKeyType="next"
              autoCorrect={false}
              autoCapitalize="none"
              onSubmitEditing={() => this.inputPassword.focus()}
              onChangeText={input => this.setState({ email: input })}
              ref={input => (this.inputEmail = input)}
            />
            <TextInput
              style={{ marginLeft: 15 }}
              keyboardType="default"
              secureTextEntry={true}
              underlineColorAndroid="transparent"
              placeholder="Password"
              placeholderTextColor="#ababab"
              returnKeyType="done"
              autoCorrect={false}
              autoCapitalize="none"
              //onSubmitEditing={() => this.uEmail.focus()}
              onChangeText={input => this.setState({ password: input })}
              ref={input => (this.inputPassword = input)}
            />
          </View>

          <TouchableOpacity
            style={styles.signinButton}
            onPress={() => this.gotoMyProfile()}
          >
            <Text style={styles.signinBtnText}>Sign In</Text>
          </TouchableOpacity>

          <View style={styles.bottomContainer}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("ForgotPasswordScreen")
              }
            >
              <Text style={styles.forgotText}>Forgot Password</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("CreateAccountStep1Screen")
              }
            >
              <Text style={styles.forgotText}>Create Account</Text>
            </TouchableOpacity>
          </View>
          <KeyboardSpacer />
        </ScrollView>
        {this.state.isLoading == true ? (
          <View
            style={{
              position: "absolute",
              height: screenSize.height,
              width: screenSize.width,
              backgroundColor: "rgba(255, 255, 255, 0.2)"
            }}
          >
            <View style={styles.activity_sub}>
              <ActivityIndicator
                size="large"
                color="#D0D3D4"
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  height: 50
                }}
              />
            </View>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  headerContainer: {
    alignItems: "center",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderBottomColor: "#8e8e93",
    borderBottomWidth: 0.7
  },
  headerText: {
    color: "#1e1e1e",
    fontFamily: boldText,
    fontSize: 20
  },

  inputContainer: {
    backgroundColor: "white",
    marginVertical: 25,
    borderBottomColor: "#8e8e93",
    borderBottomWidth: 0.7
  },

  textInput: {
    borderBottomColor: "#8e8e93",
    borderBottomWidth: 0.7,
    // paddingLeft : 15,
    marginLeft: 15
  },

  signinButton: {
    backgroundColor: "#3A4958",
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 20,
    // marginVertical : 15,
    height: 50,
    borderRadius: 4
  },

  signinBtnText: {
    color: "white",
    fontFamily: regularText,
    fontSize: 20
  },

  bottomContainer: {
    marginHorizontal: 30,
    marginTop: 30,
    flexDirection: "row",
    justifyContent: "space-between"
  },

  forgotText: {
    fontFamily: regularText,
    color: "#3A4958",
    fontSize: 18
  },
  activity_sub: {
    position: "absolute",
    top: screenSize.height / 2,
    backgroundColor: "black",
    width: 50,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 10,
    //elevation:5,
    ...Platform.select({
      android: { elevation: 5 },
      ios: {
        shadowColor: "#999",
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5
      }
    }),
    height: 50,
    borderRadius: 10
  }
});
