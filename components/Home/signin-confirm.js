
/* Sign page Screen page
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/sign-in.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
} from 'react-native';

import firebase from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');


const FBSDK = require('react-native-fbsdk');
const {
    LoginManager,
    AccessToken,
    GraphRequest,
    GraphRequestManager,
    LoginButton,
} = FBSDK;


export default class SigninConfirm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            fcm_Token : '',
        }
    }


    componentDidMount() { 
         AsyncStorage.getItem('fcmToken').then(value => this.setState({ fcm_Token: value }));
    }


    


    gotoSigninPage() {
        this.props.navigation.replace('SigninWithEmailScreen');
    }

   async fbLogin() {
        LoginManager.logOut();
        await LoginManager.logInWithReadPermissions(['public_profile','email']).then(
            (result)=>{
              if (result.isCancelled) {
                alert('Login was cancelled');
              } else {
                // alert('Login was successful with permissions: '
                //          + result.grantedPermissions.toString());
                //         alert();
                AccessToken.getCurrentAccessToken().then((data) => {
                    //alert(data.accessToken);
                    console.log(data.accessToken);
                    const accessToken = data.accessToken;
                    this._facebookLoginFirebase(accessToken);

                    // fetch('https://graph.facebook.com/me?fields=id,email,name,gender,picture&access_token=' + accessToken)
                    //     .then((response) => response.json())
                    //     .then((json) => {
                    //         // Some user object has been set up somewhere, build that user here
                    //         alert("json data" + JSON.stringify(json));
                    //         console.log("json data" + JSON.stringify(json));
                          
                    //         // console.log("id" +this.state.user_id);
                    //         // alert(this.state.user_id);
                            
  
                    //     })
                    //     .catch(() => {
                    //         reject('ERROR GETTING DATA FROM FACEBOOK')
                    //     })
  
  
                }).catch((err) => {
                    console.log(err);
                    alert(err);
                })
            }
            },
            
          ).catch((error)=> {
              alert('Login failed with error: ' + error);console.log('Login failed with error: ' + error);
            });
      }

async _facebookLoginFirebase(accessToken) {
    console.log(accessToken);
        const credential = firebase.auth.FacebookAuthProvider.credential(accessToken);
        await firebase.auth().signInWithCredential(credential)
        .then((data)=> {console.log(JSON.stringify(data))
        this._firebaseSaveUserData(data)})

        
        .catch((error)=> {
            alert('Login failed with error: ' + error);console.log('Login failed with error: ' + error);
          });

      }




async _firebaseSaveUserData(data)  {
    console.log('_firebaseSaveUserData')
        console.log('fname : '+data.additionalUserInfo.profile.first_name)
        console.log('lname : '+data.additionalUserInfo.profile.last_name)
        console.log('email : '+data.additionalUserInfo.profile.email)
        console.log('email : '+data.user.uid)

        if (data.additionalUserInfo.profile.picture.data.url == undefined || data.additionalUserInfo.profile.picture.data.url == '') {
            AsyncStorage.setItem('imageLink', '');
        }
        else {
            AsyncStorage.setItem('imageLink', data.additionalUserInfo.profile.picture.data.url);
        }
        data.additionalUserInfo.profile.last_name?
        AsyncStorage.setItem('userName', data.additionalUserInfo.profile.first_name + ' '+data.additionalUserInfo.profile.last_name )
        :
        AsyncStorage.setItem('userName', data.additionalUserInfo.profile.first_name )
  await firebase.firestore().collection("users").doc(data.user.uid).update({
            firstname : data.additionalUserInfo.profile.first_name,
            lastname: data.additionalUserInfo.profile.last_name,
            email: data.additionalUserInfo.profile.email,
            uid:data.user.uid,
            login_status : 'f',
            imageLink: data.additionalUserInfo.profile.picture.data.url
    }).then((user)=>{
        this.setState({isLoading:false});
        console.log("user data"+user);
        this._saveTokenFirestore(data.user.uid)
        this._navigateProjectSearchScreen(data.user.uid)
        //this.props.navigation.navigate('CreateAccountStep2FbScreen',{uid: data.user.uid,loginStatus : 'f'})

    }).catch((error)=>{console.log("user error"+error)
    this._firebaseSetUserData(data);
    })
    // Alert.alert(
    //     'Pro In Your Pockets For Pros',
    //      'You have successfully Registered With Pro In Your Pockets For Pros',
    //     [
    //         { text: 'OK', onPress: () => this.props.navigation.replace('LoginScreen') },
    //     ],
    //     { cancelable: false }
    // );
}

async _firebaseSetUserData(data) {

    await firebase.firestore().collection("users").doc(data.user.uid).set({
        firstname : data.additionalUserInfo.profile.first_name,
        lastname: data.additionalUserInfo.profile.last_name,
        email: data.additionalUserInfo.profile.email,
        uid:data.user.uid,
        createdAt: new Date(),
        login_status : 'f',
        imageLink: data.additionalUserInfo.profile.picture.data.url
    }).then((user)=>{
        this.setState({isLoading:false});
        console.log("Document successfully written!");
        console.log(data.user.uid);
        
        this._saveTokenFirestore(data.user.uid)
        this.props.navigation.replace('CreateAccountStep2FbScreen',{uid: data.user.uid,loginStatus : 'f'})

    })
    .catch((error)=> console.log("user writting data err : " + error))
}

async  _saveTokenFirestore(uid) {
        await firebase.firestore().collection("sp_tokens").doc(this.state.fcm_Token).set({
            uid : uid,
            fcm_token : this.state.fcm_Token,
            device_id : DeviceInfo.getUniqueID(),
            device_brand: DeviceInfo.getBrand(),
            deviceModel : DeviceInfo.getModel()
        }).then((user)=>{
            console.log("token  data written successfully");

        }).catch((error)=>{
            console.log("Error writing token  data ")
        })
    }

    _navigateProjectSearchScreen(userId)
        {
            console.log(userId);
            AsyncStorage.setItem('user_id', userId);
            AsyncStorage.setItem('user_status', "1");
            AsyncStorage.setItem('loginStatus', 'f' );
            this.props.navigation.replace('ProjectSearchScreen',{uid: userId })
        }



    render() {
        return (
            <View>
                <StatusBar
                    backgroundColor="#1e1e1e"
                    barStyle="light-content"
                />
                <View style={styles.headerContainer}>
                    <Text style={styles.welcomeText}>Welcome</Text>
                </View>

                <View style={styles.container}>
                    <View style={{ alignSelf: 'center', }}>
                        <Image
                            style={{ width: (screenSize.width) / 2 + 50, height: (screenSize.height) / 4, resizeMode: "contain" }}
                            source={require('../../assets/images/banner-blue.png')}
                        />
                    </View>

                    <View>
                        <Text style={styles.welcomeProText}>Welcome to the Pro In Your Pocket Pro App!</Text>
                        <Text style={styles.welcomeProText}>Sign in or create a Pro account to get started</Text>
                    </View>

                    {/* <TouchableOpacity style={styles.facebookBtnContainer} onPress={() => this.fbLogin()}>
                        <Text style={styles.facebookSigninText}>Continue with Facebook</Text>
                    </TouchableOpacity> */}
                    
                    <TouchableOpacity style={styles.emailSigninContainer} onPress={() => this.gotoSigninPage()}>
                        <Text style={styles.emailSigninText}>Sign in with email</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateAccountStep1Screen')}>
                        <Text style={styles.createAccountText}>Create an account</Text>
                    </TouchableOpacity>

                    

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        alignItems: 'center',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
    },
    welcomeText: {
        color: '#1e1e1e',
        fontFamily: boldText,
        fontSize: 20
    },

    container: {
        flexDirection: 'column',
        height: screenSize.height - 40,
        justifyContent: 'center',
        alignItems: 'center',
    },

    welcomeProText: {
        fontFamily: regularText,
        fontSize: 17,
        color: '#8e8e93'
    },

    facebookBtnContainer: {
        backgroundColor: '#3A4958',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: 240,
        marginTop: 30,
        borderRadius: 3
    },

    facebookSigninText: {
        color: 'white',
        fontFamily: regularText,
        fontSize: 18
    },

    emailSigninContainer: {
        backgroundColor: '#3A4958',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: 200,
        marginTop: 15,
        borderRadius: 3
    },

    emailSigninText: {
        color: '#1e1e1e',
        fontFamily: regularText,
        fontSize: 18
    },

    createAccountText: {
        fontFamily: regularText,
        color: '#3A4958',
        fontSize: 17,
        marginTop: 25
    }
});