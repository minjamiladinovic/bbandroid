import React, { Component } from "react";
import {
    SafeAreaView,
    ScrollView,
    Linking,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    StatusBar,
    RefreshControl,
    FlatList,
    BackHandler
} from "react-native";

import firebase from "react-native-firebase";

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { CachedImage } from 'react-native-cached-image';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';

import { SegmentedControls } from 'react-native-radio-buttons';
import Footer from '../footer/footer';

//import Moment from "react-moment";

//import { DotsLoader, RotationHoleLoader, CirclesLoader, BubblesLoader } from 'react-native-indicator';


const screenSize = Dimensions.get('window');
const options = [
    "Inbox",
    "Archive",
];

const db = firebase.firestore();

export default class MessageListing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: 'Inbox',
            chatUser: [],
            uid: this.props.navigation.state.params.uid,
            chatUser_inbox: [],
            chatUser_archive: [],
            chatUsr_details: [],
            isLoading: false,
            inbox_show: false,
            refreshing: false,
            show_search: false,
            search_chat: [],
            global_array: [],
            projects: [],
            data_id: [],
        }
    }

    componentDidMount() {
        this._getProjects();
        // firebase.analytics().setCurrentScreen("Messages", "Messages");
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    
    handleBackButton = () => {
        this.props.navigation.pop();
         return true;
       }

    setSelectedOption(selectedOption) {
        if (selectedOption.trim() == "Inbox") {
            this.setState({
                inbox_show: true,
                selectedOption: selectedOption
            });
        }
        else {
            this.setState({
                inbox_show: false,
                selectedOption: selectedOption
            });
        }
    }

    _getMessages() {
        var inbox = db.collection("messages").doc(this.state.uid).collection("inbox");
        var archive = db.collection("messages").doc(this.state.uid).collection("archive");

        inbox.get().then((data) => {
            if (data.size > 0) {
                this.setState({
                    isLoading: true
                });
                data.docs.map(doc => {
                    //console.log(doc.data());
                    var obj_id = doc.id;
                    var dateValue = new Date(doc.data().chat[0].createdAt);
                    var timeNumber = dateValue.valueOf();
                    var message = doc.data().chat[0].description;
                    var chatId = doc.id.split("_")[0];
                    var project_id = doc.id.split("_")[1];
                    var createdAt = doc.data().chat[0].createdAt;

                    var project = this._getProjectDetails(project_id);
                    var hire_id = " ";

                    if (project.hiredId != undefined) {
                        hire_id = project.hiredId;
                    }
                    // console.log("project" + project);
                    // console.log(project.description);
                    var obj = {
                        "id": chatId,// new change
                        "project_id": project_id,
                        "project_description": project.description,
                        "hire_id": hire_id,
                        "date": timeNumber,
                        "message": message,
                        "createdAt": createdAt
                    };
                    // console.log("obj" + obj);
                    //this.state.chatUser_inbox.push(obj_id);
                    this.state.chatUser_inbox.push(obj);
                    this.state.global_array.push(obj);

                    // console.log("inbox user" + this.state.chatUser_inbox);
                })
                var dbData = this.state.chatUser_inbox;
                dbData = dbData.sort((a, b) => (b.date > a.date) ? 1 : ((a.date > b.date) ? -1 : 0));
                this.setState({
                    chatUser_inbox: dbData
                }, this._getInboxDetails.bind(this));
            }
            else {
                this.setState({
                    isLoading: false
                })
            }

        }).catch((err) => {
            this.setState({ isLoading: false });
            // console.log("err");
            // console.log(err)
        })

        archive.get().then((data) => {
            if (data.size > 0) {
                this.setState({ isLoading: true });
                data.docs.map(doc => {
                    // console.log(doc.data());
                    var obj_id = doc.id;
                    var dateValue = new Date(doc.data().chat[0].createdAt);
                    var timeNumber = dateValue.valueOf();
                    var message = doc.data().chat[0].description;
                    var chatId = doc.id.split("_")[0];
                    var project_id = doc.id.split("_")[1];
                    var project = this._getProjectDetails(project_id);
                    var createdAt = doc.data().chat[0].createdAt;
                    var hire_id = " ";

                    if (project.hiredId != undefined) {
                        hire_id = project.hiredId;
                    }
                    // console.log("project" + project);

                    var obj = {
                        "id": chatId,// new change
                        "project_id": project_id,
                        "project": project,
                        "project_description": project.description,
                        "hire_id": hire_id,
                        "date": timeNumber,
                        "message": message,
                        "createdAt": createdAt
                    };
                    // console.log("obj" + obj);
                    //this.state.chatUser_archive.push(obj_id);
                    this.state.chatUser_archive.push(obj);
                    this.state.global_array.push(obj);

                    // console.log("archive user" + JSON.stringify(this.state.chatUser_archive));
                })

                var dbData_archive = this.state.chatUser_archive;
                dbData_archive = dbData_archive.sort((a, b) => (b.date > a.date) ? 1 : ((a.date > b.date) ? -1 : 0));
                this.setState({
                    chatUser_archive: dbData_archive
                }, this._getArchiveDetails.bind(this));
            }
            else {
                this.setState({
                    isLoading: false
                })
            }

        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log("err");
            console.log(err)
        })

    }



    _getProjects() {
        this.setState({ isLoading: true })
        db.collection("projects").get().then((data) => {
            var array = [];
            var array_id = [];
            var mod_array = [];
            var i = 0;

            data.forEach(doc => {
                var obj_id = doc.id;
                this.state.data_id.push(obj_id);
            })

            data.docs.forEach(doc => {
                var obj = doc.data();
                array.push(obj);
            })
            for (data of array) {
                data.projectId = this.state.data_id[i];
                mod_array.push(data);
                i++;
            }
            // console.log("modify array" + JSON.stringify(mod_array));
            this.setState({
                projects: mod_array,
            }, this._getMessages.bind(this))

        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log(err)
        })

    }


    _getProjectDetails(id) {
        var project = {};

        for (var i = 0; i < this.state.projects.length; i++) {
            if (this.state.projects[i].projectId == id) {

                project = this.state.projects[i];
            }
        }
        return project;
    }


    _getInboxDetails() {
        for (var i = 0; i < this.state.chatUser_inbox.length; i++) {
            var docRef2 = db.collection("users").doc(this.state.chatUser_inbox[i].id);
            this._getDetails(docRef2, "yes");
        }
    }
    _getArchiveDetails() {
        for (var i = 0; i < this.state.chatUser_archive.length; i++) {
            var docRef2 = db.collection("users").doc(this.state.chatUser_archive[i].id);
            this._getDetails(docRef2, "yes");
        }
    }

    _getDetails(docRef2, status) {
        if (status.trim() == "yes") {
            this.setState({
                isLoading: true
            });
        }


        if (status == "yes") {
            docRef2.get().then((doc) => {
                if (doc.exists) {
                    // console.log("Document data:", doc.data());
                    var obj = doc.data();
                    this.state.chatUsr_details.push(obj);
                    // console.log("chat user details" + this.state.chatUsr_details);

                    var inbox_len = this.state.chatUser_inbox.length;
                    var archive_len = this.state.chatUser_archive.length;
                    var len = inbox_len + archive_len;

                    // console.log("total len" + len);
                    // console.log("chat len" + this.state.chatUsr_details.length);
                    if (this.state.chatUsr_details.length == len) {
                        this.setState({
                            isLoading: false,
                            refreshing: false,
                            inbox_show: true
                        });
                    }
                }
            }).catch((error) => {
                this.setState({
                    isLoading: false
                });
                console.log("Error getting document:", error);
            });
        }

        else {
            //var chat=[];
            docRef2.get().then((doc) => {
                if (doc.exists) {
                    // console.log("Document data:", doc.data());
                    var obj = doc.data();
                    this.state.chatUsr_details.push(obj);
                    //console.log("chat user details" + this.state.chatUsr_details);

                    var inbox_len = this.state.chatUser_inbox.length;
                    var archive_len = this.state.chatUser_archive.length;
                    var len = inbox_len + archive_len;

                    //console.log("total len" + len);
                    //console.log("chat len" + this.state.chatUsr_details.length);
                    if (this.state.chatUsr_details.length > len) {
                        this.setState({
                            isLoading: false,
                            refreshing: false,
                            inbox_show: true
                        });
                    }
                }
            }).catch((error) => {
                this.setState({
                    isLoading: false
                });
                //console.log("Error getting document:", error);
            });
        }

    }


    _stopLoading() {
        //console.log("chat user details" + this.state.chatUsr_details);

        var inbox_len = this.state.chatUser_inbox.length;
        var archive_len = this.state.chatUser_archive.length;
        var len = inbox_len + archive_len;
        //console.log("total len" + len);
        //console.log("chat len" + this.state.chatUsr_details.length);
        if (this.state.chatUsr_details.length == len) {
            this.setState({
                isLoading: false,
                refreshing: false,
                inbox_show: true
            });
        }
    }


    _getName(item) {
        //console.log("name item" + item.hire_id);
        var name = this._bidName(item.id);
        //console.log("get name" + name);
        var firstname ;
        var lastname;
        var lastnameInitial;

        if(name != undefined)
        {
             firstname = name.split(" ")[0];
             lastname = name.split(" ")[1];

             if(lastname != " ")
             {
                  lastnameInitial = lastname.charAt(0);
             }

        }
        else
        {
            firstname='Customer';
        }
       
       
       
       

        //console.log("lastname"+lastnameInitial);
        //console.log("uid"+this.state.uid);
        //console.log("hire id"+item.hire_id);

        if (this.state.uid == item.hire_id) {
            return (
                <View style={{ marginLeft: 10, marginBottom: 20 }}>
                    <View style={{ flexDirection: "row", width: screenSize.width - 70 - 10 }}>
                        <Text style={{ fontSize: 16, color: "black", fontWeight: "600" }}>{firstname} {lastname}</Text>
                        {/* <View style={{flexDirection:"row",marginLeft:50}}>
          
                      <Moment element={Text} style={{ fontSize: 12, color: "#827e7e" }} fromNow>
                        {item.createdAt}
                      </Moment>
                    </View> */}
                    </View>

                    <Text numberOfLines={1} ellipsizeMode="tail" style={{ fontSize: 14, color: "#827e7e", marginTop: 5, fontWeight: "800",width: screenSize.width - 70 - 10-30 }}>Project : " {item.project_description} " </Text>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={{ fontSize: 12, color: "#827e7e", marginTop: 5, fontWeight: "300",width: screenSize.width - 70 - 10-30 }}>{item.message}</Text>
                </View>
            )
        }

        else {
            return (
                <View style={{ marginLeft: 10, marginBottom: 20 }}>
                    <View style={{ flexDirection: "row", width: screenSize.width - 70 - 10 }}>
                        <Text style={{ fontSize: 16, color: "black", fontWeight: "600" }}>{firstname} {lastnameInitial}</Text>
                        {/* <View style={{flexDirection:"row",marginLeft:50}}>
          
                      <Moment element={Text} style={{ fontSize: 12, color: "#827e7e" }} fromNow>
                        {item.createdAt}
                      </Moment>
                    </View> */}
                    </View>

                    <Text numberOfLines={1} ellipsizeMode="tail" style={{ fontSize: 14, color: "#827e7e", marginTop: 5, fontWeight: "800",width: screenSize.width - 70 - 10-30 }}>Project : " {item.project_description} " </Text>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={{ fontSize: 12, color: "#827e7e", marginTop: 5, fontWeight: "300",width: screenSize.width - 70 - 10-30 }}>{item.message}</Text>
                </View>
            )
        }

    }


    _bidName(id) {
        //console.log("id" + id);
        var name = "";
        for (var i = 0; i < this.state.chatUsr_details.length; i++) {
            //console.log("uid" + this.state.chatUsr_details[i].uid + "id" + id);
            if (this.state.chatUsr_details[i].uid.trim() == id) {
                name = this.state.chatUsr_details[i].firstname + " " + this.state.chatUsr_details[i].lastname;
            }
        }
        //console.log("get name" + name);
        return name;
    }

    _getUserImage(item) {
        url = this._bidderImage(item.id);
        if (url == "" || url == undefined) {
            return (
                <TouchableOpacity>
                    <Image
                        source={require('../../assets/images/userOwner.png')}
                        style={styles.avatorImage}
                    />
                </TouchableOpacity>
            )
        }
        else {
            return (
                <TouchableOpacity>
                    <CachedImage
                        source={{ uri: url }}
                        style={styles.avatorImage}
                    />
                </TouchableOpacity>
            )
        }

    }

    _bidderImage(id) {
        var url = "";
        for (var i = 0; i < this.state.chatUsr_details.length; i++) {
            //console.log("uid" + this.state.chatUsr_details[i].uid);
            if (this.state.chatUsr_details[i].uid == id) {
                url = this.state.chatUsr_details[i].imageLink;
            }
        }
        return url;
    }

    _bidderBusiness(id) {

        // console.log("id"+id);
        //console.log("details" + JSON.stringify(this.state.bidderDetails));
        var businessName = "";
        for (var i = 0; i < this.state.chatUsr_details.length; i++) {
           // console.log("uid" + this.state.chatUsr_details[i].uid + "id" + id);
            if (this.state.chatUsr_details[i].uid == id) {
                if (this.state.chatUsr_details[i].BusinessInfo.CName != undefined) {
                   // console.log("get name" + this.state.chatUsr_details[i].BusinessInfo.CName);
                    businessName = this.state.chatUsr_details[i].BusinessInfo.CName;
                }

            }
        }
       /// console.log("get name" + businessName);
        return businessName;
    }


    _goToChat(item, status) {
        var name = this._bidName(item.id);
        var firstname = name.split(" ")[0];
        var lastname = name.split(" ")[1];
        var lastnameInitial = lastname.charAt(0);
        var displayName="";

        if(this.state.uid == item.hire_id)
        {
            displayName=firstname+" "+lastname;
        }
        else
        {
            displayName=firstname+" "+lastnameInitial;
        }
       
        // var business_name = this._bidderBusiness(item);
        var url = this._bidderImage(item.id);

        this.props.navigation.replace("ProjectMessageScreen",
            {
                toChatId: item.id,
                fromChatId: this.state.uid,
                ProjectOwnerName:displayName,
                project_id: item.project_id,
                imageLink: url,
                navigateFrom: "message_listings",
                message_status: status,
                Uid: this.state.uid,
            });
    }


    _onRefresh = () => {
        this.setState({ refreshing: true });
        this._getMessages_refresh();
    }

    _getMessages_refresh() {
        //alert();
        var array_inbox = [];

        var inbox = db.collection("messages").doc(this.state.uid).collection("inbox");
        var archive = db.collection("messages").doc(this.state.uid).collection("archive");

        inbox.get().then((data) => {
            data.docs.map(doc => {
                //console.log(doc.data());
                var obj_id = doc.id;
               // console.log("obj_id" + doc.id);
                array_inbox.push(obj_id);
                //this.state.chatUser_inbox.push(obj_id);
                //console.log("inbox user" + this.state.chatUser_inbox);
            })
            this.setState({
                chatUser_inbox: array_inbox
            }, this._getInboxDetails.bind(this))


            //this.setState({ isLoading: false });
            //console.log("succss");
        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log("err");
            console.log(err)
        })

        var array_archive = [];

        archive.get().then((data) => {
            //this.setState({ isLoading: true });
            data.docs.map(doc => {
                //console.log(doc.data());
                var obj_id = doc.id;
              //  console.log("obj_id" + doc.id);
                //this.state.chatUser_archive.push(obj_id);
                array_archive.push(obj_id);
                //console.log("archive user" + this.state.chatUser_archive);
            })

            this.setState({
                chatUser_archive: array_archive,
                //chatUsr_details:[]
            }, this._getArchiveDetails.bind(this));


            // this.setState({ isLoading: false });
            //console.log("succss");
        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log("err");
            console.log(err)
        })

    }

    _getSearch(input) {
        var matchedService = [];
        this.setState({
            search_chat: []
        });

        if (input.trim() == '') {
            this.setState({ show_search: false });
            return false;
        }
        else {
            var counter = 0;
            matchedService = [];

            for (var i = 0; i < this.state.chatUsr_details.length; i++) {
                var name = this.state.chatUsr_details[i].firstname.toLowerCase();
                // if ((name.indexOf(input.trim().toLowerCase()) != -1) && counter < 4) 
                if ((name.trim().toLowerCase().includes(input.trim().toLowerCase())) && counter < 4) {
                    var obj = this._getObject(this.state.chatUsr_details[i].uid);
                    matchedService.push(obj);
                    //this.state.search_chat.push(obj);
                    counter++;
                }
                else if (counter == 4) {
                    break;
                }
                else {

                }
            }
            if (matchedService.length != 0) {
                this.setState({
                    search_chat: matchedService,
                    show_search: true,
                    // inbox_show:false
                });
                //alert(this.state.search_chat.length);
               // console.log("search chat" + this.state.search_chat);
            }
            else {
                this.setState({
                    show_search: false,
                });
            }

        }
    }

    _getObject(id) {
        var obj;
        for (var i = 0; i < this.state.global_array.length; i++) {
            if (this.state.global_array[i].id == id) {
                obj = this.state.global_array[i];
            }
        }
        return obj;
    }


    _removeSearch() {
        this.setState({
            show_search: false,
            search_chat: []
        })
        this.inputMessage.setNativeProps({ text: "" });
    }


    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#3A4958"
                    barStyle="light-content"
                />

                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" style={styles.backButton} />
                    </TouchableOpacity>

                    <Text style={{ color: "#ffff", fontWeight: "600", fontSize: 20 }}>Messages</Text>

                    <View style={{ width: 30, height: 40 }} />
                </View>
                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>)
                    :
                    (
                        <ScrollView style={{ marginBottom: 60 }}>
                            <View style={{ backgroundColor: "#ffff" }}>
                                <View style={{
                                    borderBottomColor: "#bbc1c8",
                                    borderBottomWidth: 0.8,
                                    width: screenSize.width,
                                    alignSelf: "center",
                                    padding: 10, flexDirection: "row",
                                    borderRadius: 3,
                                    marginTop: 10,
                                    //marginBottom:5,
                                    height: 50
                                }}>
                                    <Feather name='search' color='#969da8' size={30} />
                                    <TextInput
                                        style={styles.textStyle}
                                        keyboardType='default'
                                        underlineColorAndroid="transparent"
                                        placeholder="Search"
                                        placeholderTextColor="#ababab"
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        onChangeText={(input) => this._getSearch(input)}
                                        ref={((input) => this.inputMessage = input)}
                                    >
                                    </TextInput>

                                    {(this.state.show_search == true) ?
                                        (
                                            <TouchableOpacity onPress={() => this._removeSearch()}>
                                                <Entypo name='cross' color='#969da8' size={30} />
                                            </TouchableOpacity>
                                        )
                                        :
                                        null}
                                </View>
                            </View>

                            {(this.state.show_search == false) ?
                                (<View style={[styles.descriptionView,]}>
                                    <SegmentedControls
                                        tint={'#3A4958'}
                                        selectedTint={'white'}
                                        backTint={'#fff'}
                                        options={options}
                                        allowFontScaling={false} // default: true
                                        onSelection={this.setSelectedOption.bind(this)}
                                        selectedOption={this.state.selectedOption}
                                        optionStyle={{ fontSize: 16 }}
                                        optionContainerStyle={{ flex: 1 }}
                                    />
                                </View>)
                                :
                                null}

                            {/* <View style={styles.flatListView}>
                                    <View style={styles.messageView}>
                                        <Image
                                            source={require('../../assets/images/user.png')}
                                            style={styles.avatorImage}
                                        />
                                        <View style={{ marginLeft: 10 }}>
                                            <Text style={{ fontSize: 16, color: "black", fontWeight: "600" }}>Justin W </Text>
                                            <Text style={{ fontSize: 12, color: "#827e7e", marginTop: 5, fontWeight: "600" }}>LANDSCALLING-SERVICES</Text>
                                        </View>

                                        <Text style={{ fontSize: 12, color: '#a09a9a', marginTop: -20, marginLeft: 15 }}>12th jan 2018</Text>
                                    </View>
                                </View> */}

                            {(this.state.show_search == false) ?
                                (<View>
                                    {(this.state.inbox_show == true) ?
                                        (<FlatList
                                            data={this.state.chatUser_inbox}
                                            renderItem={({ item, index }) =>
                                                (
                                                    <TouchableOpacity style={styles.flatListView}
                                                        onPress={() => this._goToChat(item, "inbox")}
                                                    >
                                                        <View style={styles.messageView}>

                                                            {this._getUserImage(item)}
                                                            {this._getName(item)}

                                                            {/* <Text style={{ fontSize: 12, color: '#a09a9a', marginTop: -20, marginLeft: 15 }}>12th jan 2018</Text> */}
                                                        </View>
                                                    </TouchableOpacity>
                                                )}
                                            keyExtractor={item => item.id}
                                        />)
                                        :
                                        null}
                                    {(this.state.selectedOption == "Archive") ?
                                        (<FlatList
                                            data={this.state.chatUser_archive}
                                            renderItem={({ item, index }) =>
                                                (
                                                    <TouchableOpacity style={styles.flatListView}
                                                        onPress={() => this._goToChat(item, "archive")}>
                                                        <View style={styles.messageView}>

                                                            {this._getUserImage(item)}
                                                            {this._getName(item)}

                                                            {/* <Text style={{ fontSize: 12, color: '#a09a9a', marginTop: -20, marginLeft: 15 }}>12th jan 2018</Text> */}
                                                        </View>
                                                    </TouchableOpacity>
                                                )}
                                            keyExtractor={item => item.id}
                                        />)
                                        :
                                        null}

                                </View>)
                                :
                                null}

                            {(this.state.show_search == true) ?
                                (
                                    <View>
                                        {
                                            this.state.search_chat.map((item, index) => (
                                                <TouchableOpacity style={styles.flatListView}
                                                    onPress={() => this._goToChat(item, "archive")}>
                                                    <View style={styles.messageView}>

                                                        {this._getUserImage(item)}
                                                        {this._getName(item)}

                                                        {/* <Text style={{ fontSize: 12, color: '#a09a9a', marginTop: -20, marginLeft: 15 }}>12th jan 2018</Text> */}
                                                    </View>
                                                </TouchableOpacity>
                                            ))
                                        }
                                    </View>
                                )
                                :
                                null}


                        </ScrollView>
                    )}
                <Footer itemColor='message'></Footer>
            </View>

        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },

    textStyle: {
        width: screenSize.width - 60 - 10 - 20 - 30,
        marginLeft: 10,
        height: 30,
        padding: 0,
        margin: 0,
        flex: 1
    },
    descriptionView: {
        backgroundColor: "#ffff",
        //elevation:2,
        padding: 15,
        width: screenSize.width,
    },

    backButton: {
        fontSize: 30,
        color: '#ffff',
        paddingLeft: 10
    },

    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },

    messageView: {
        flexDirection: 'row',
        backgroundColor: "#ffff",
        width: screenSize.width,
        height: 80,
        paddingHorizontal: 10,
        alignItems: "center",
        marginBottom: 10,
       // marginTop: 10,
        borderBottomColor: "#bbc1c8",
        borderBottomWidth: 0.4,
    },
    flatListView:
    {
        marginTop: 10,
        backgroundColor: "#ffff"
    },
    headerContainer: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#3A4958',
        paddingTop: Platform.OS === 'ios' ? 20 : 5,
        paddingHorizontal: 5,
        height: 60
    },
    activity_main: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 5,
        //elevation: 5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
    },

    // activity_sub: {
    //     position: 'absolute',
    //     top: screenSize.height / 2,
    //     //backgroundColor: '#ffff',
    //     //width: 50,
    //     alignSelf: 'center',
    //     justifyContent: "center",
    //     alignItems: 'center',
    //     zIndex: 10,
    //     //elevation:5,
    //     ...Platform.select({
    //         android: { elevation: 5, },
    //         ios: {
    //             shadowColor: '#999',
    //             shadowOffset: {
    //                 width: 0,
    //                 height: 3
    //             },
    //             shadowRadius: 5,
    //             shadowOpacity: 0.5,
    //         },
    //     }),
    //     //height: 50,
    //     //borderRadius: 10
    // },

};

