
/* My Account Setting page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/notification-events.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Switch
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');

export default class NotificationSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newProject: true,
            bidAccept: true,
        }
    }

    render() {
        return(
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.backContainer} onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Settings</Text>
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Notification Events</Text>
                    <Text style={styles.nextText}></Text>
                </View>

                <View style={styles.headingContainer}>
                    <Text style={styles.headingText}>EVENTS</Text>
                </View>

                <View style={styles.itemContainer}>
                    <Text style={styles.itemText}>New project posted</Text>
                    <Switch
                        onValueChange={(value) => this.setState({ newProject: value })}                        
                        value={this.state.newProject} />
                </View>

                <View style={styles.itemContainer}>
                    <Text style={styles.itemText}>Bid accepted</Text>
                    <Switch
                        onValueChange={(value) => this.setState({ bidAccept: value })}                        
                        value={this.state.bidAccept} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff'
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18,

    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    headingContainer: {
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 10,
    },

    headingText: {
        fontFamily: regularText,
        fontSize: 16
    },

    itemContainer: {
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 10,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
    },

    itemText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#1e1e1e'
    },
})