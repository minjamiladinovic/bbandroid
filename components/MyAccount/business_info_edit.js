// Create account page 3
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/business-info.png

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
} from 'react-native';

import { Picker, Item } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { TextInputMask } from 'react-native-masked-text';
import MyAccount from './my-account';
import states from "../../assets/files/states";


const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class BusinessInfoEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            businessType: false,
            individualType: false,
            type_business: "",
            selectedState: '',
            selectedCity: "",
            businessText: "",
            uid: this.props.navigation.state.params.uid,
            businessInfo: this.props.navigation.state.params.businessInfo,
            companyName: "",
            myPhone: "",
            myEmail: "",
            companyAddress1: "",
            companyAddress2: "",
            city: "",
            postcode: "",
            isLoading: false,
            phoneNumberFormat: "",
            cities: [],
            zipcodeCity: [],
            zipcodeState: [],
            showCity: true
        }
    }

    componentDidMount() {

        var business = this.state.businessInfo;
        this.inputComPhone.setNativeProps({ text: this._formatPhoneNumber(business.CPhone) });
        console.log("business_type" + business.BusinessType);
        if (business.BusinessType == "Business") {
            this.setState({
                businessType: true,
                businessText: business.BusinessType
            })
        }
        else if (business.BusinessType == "Individual") {
            this.setState({
                individualType: true,
                businessText: business.BusinessType
            })
        }
        this.setState({
            companyName: business.CName,
            myPhone: business.CPhone,
            //phoneNumberFormat:this._formatPhoneNumber(business.CPhone),
            phoneNumberFormat: business.CPhone,
            myEmail: business.CEmail,
            companyAddress1: business.CAddress1,
            companyAddress2: business.CAddress2,
            //city: business.CCity,
            selectedState: business.CState || '',
            selectedCity: business.CCity || '',
            postcode: business.CPin || '',
            cities: business.cityList,
        })
    }


    _businessCheck() {
        if (this.state.businessType == false) {
            this.setState({
                businessText: "Business",
                businessType: true,
                individualType: false
            })
        }
    }

    _formatPhoneNumber(phoneNumberString) {
        var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
        if (match) {
            return '(' + match[1] + ') ' + match[2] + '-' + match[3]
        }
        return null
    }

    _indCheck() {
        if (this.state.individualType == false) {
            this.setState({
                businessText: "Individual",
                businessType: false,
                individualType: true
            })
        }
    }

    // onValueChange(value) {
    //     this.setState({
    //         selectedState: value
    //     });
    // }


    onValueChange(value) {
        this.setState({
            selectedState: value,
            showCity: true,
            //selectedCity:this.state.cities[0].city
        });
        for (var i = 0; i < states.length; i++) {
            if (states[i].name.trim() == value.trim()) {
                var ctites_zip = [];
                for (var key in states[i].cities) {
                    if (states[i].cities.hasOwnProperty(key)) {
                        var obj = {};
                        obj.city = key;
                        obj.zipcode = states[i].cities[key];
                        ctites_zip.push(obj);

                        //keys.push(key);
                        //data.push(obj[key]); // Not necessary, but cleaner, in my opinion. See the example below.
                    }
                }
                this.setState({
                    cities: ctites_zip,
                    // zipcodes:zipcodes
                })

                // for(var i=0;i<ctites_zip.length;i++)
                // {
                //     this.state.zipcodeState.push(ctites_zip[i].zipcode[0])
                // }
                var zipCode = [];
                for (var i = 0; i < ctites_zip.length; i++) {
                    var obj = {};
                    obj.zipCode = ctites_zip[i].zipcode[0];
                    obj.show = false;
                    zipCode.push(obj)
                }

                this.setState({ zipcodeState: zipCode })


                console.log(this.state.zipcodeState);


            }
        }

    }

    onValueChangeCity(value) {
        console.log("zip" + this.state.zipcodeState);

        this.setState({
            selectedCity: value
        });
        for (var i = 0; i < this.state.cities.length; i++) {
            if (this.state.cities[i].city.trim() == value.trim()) {
                this.setState({ zipcodeCity: this.state.cities[i].zipcode })
            }
        }

        console.log(this.state.zipcodeCity)

    }

    gotoNextStep() {
        this._formValidation();
    }

    _formValidation() {

        if (this.state.businessText === "" || this.state.businessText.trim() === " ") {
            CommonTasks._displayToast("Please Choose your Business Type");
            return false;
        }
        if (this.state.companyName === '' || this.state.companyName.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Company Name");
            return false;
        }
        if (this.state.myPhone === '' || this.state.myPhone.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Company's Contact Number");
            return false;
        }
        if (this.state.myEmail === '' || this.state.myEmail.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Company's Email");
            return false;
        }
        if (this.state.companyAddress1 === '' || this.state.companyAddress1.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Company's Address");
            return false;
        }
        if (this.state.selectedCity === '' || this.state.selectedCity.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Company's City");
            return false;
        }
        if (this.state.selectedState === '' || this.state.selectedState.trim() === 'Select State') {
            CommonTasks._displayToast("Please Enter Your Company's State");
            return false;
        }
        if (this.state.postcode === '' || this.state.postcode === '') {
            CommonTasks._displayToast("Please Enter Your Company's Postal Code");
            return false;
        }
        this._addBusiness();

    }

    _addBusiness() {
        this.setState({ isLoading: true });
        db.collection("users").doc(this.state.uid).update({

            "BusinessInfo.BusinessType": this.state.businessText,
            "BusinessInfo.CName": this.state.companyName,
            "BusinessInfo.CPhone": this.state.myPhone,
            "BusinessInfo.CEmail": this.state.myEmail,
            "BusinessInfo.CAddress1": this.state.companyAddress1,
            "BusinessInfo.CAddress2": this.state.companyAddress2,
            "BusinessInfo.CCity": this.state.selectedCity,
            "BusinessInfo.CState": this.state.selectedState,
            "BusinessInfo.CPin": this.state.postcode,
            "BusinessInfo.cityList": this.state.cities,
            // "CPin": this.state.zipcodeCity,
            "BusinessInfo.StateZip": this.state.zipcodeState,
            "BusinessInfo.SelectedZip": this.state.businessInfo.SelectedZip

        }).then((user) => {
            var objInfo = {
                // "CPhone": this.state.myPhone,
                // "CCity": this.state.city,
                // "CName": this.state.companyName,
                // "CAddress1": this.state.companyAddress1,
                // "CEmail": this.state.myEmail,
                // "CAddress2": this.state.companyAddress2,
                // "CPin": this.state.postcode,
                // "BusinessType": this.state.businessText,
                // "CState": this.state.selectedState
                "BusinessType": this.state.businessText,
                "CName": this.state.companyName,
                "CPhone": this.state.myPhone,
                "CEmail": this.state.myEmail,
                "CAddress1": this.state.companyAddress1,
                "CAddress2": this.state.companyAddress2,
                "CCity": this.state.selectedCity,
                "CState": this.state.selectedState,
                "cityList": this.state.cities,
                "CPin": this.state.zipcodeCity,
                "StateZip": this.state.zipcodeState,
                "SelectedZip": this.state.businessInfo.SelectedZip
            };
            MyAccount._changeBusinessInfo(objInfo);
            this.setState({ isLoading: false });
            console.log("user data" + user);
            this.props.navigation.pop();
            CommonTasks._displayToast("Successfully updated Business Info");
        }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error);
        })
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>

                    <TouchableOpacity style={styles.backContainer}
                        onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Back</Text>
                    </TouchableOpacity>

                    <Text style={styles.headerText}>Business Info</Text>

                    <TouchableOpacity onPress={() => this.gotoNextStep()}>
                        <Text style={styles.nextText}>Save</Text>
                    </TouchableOpacity>

                </View>
                <ScrollView>
                    <View style={{ marginTop: 20, marginLeft: 10 }}>
                        <Text style={styles.businessText}>BUSINESS TYPE</Text>
                    </View>

                    <View style={styles.businessTypeContainer}>
                        <TouchableOpacity style={styles.infoText} onPress={() => this._businessCheck()}>
                            <Text style={styles.optionText}>Business</Text>
                            {(this.state.businessType == true) ?
                                (<Ionicons name="ios-checkmark" style={styles.optionImage} />)
                                :
                                null}
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.infoText} onPress={() => this._indCheck()}>
                            <Text style={styles.optionText}>Individual</Text>
                            {(this.state.individualType == true) ?
                                (<Ionicons name="ios-checkmark" style={styles.optionImage} />)
                                :
                                null}
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginTop: 20, marginLeft: 10 }}>
                        <Text style={styles.businessText}>BUSINESS CONTACT</Text>
                    </View>

                    <View style={styles.verificationContainer}>
                        <TextInput
                            style={styles.textInput}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="My Company"
                            placeholderTextColor="#ababab"
                            returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            value={this.state.companyName}
                            onSubmitEditing={() => this.inputComPhone.focus()}
                            onChangeText={(input) => this.setState({ companyName: input })}
                            ref={((input) => this.inputComName = input)}
                        />

                        <TextInputMask
                            placeholder="My phone"
                            returnKeyType='next'
                            placeholderTextColor="#ababab"
                            onSubmitEditing={() => this.inputComEmail.focus()}
                            value={this.state.phoneNumberFormat}
                            //ref={((input) => this.inputComPhone = input)}
                            refInput={ref => (this.inputComPhone = ref)}
                            onChangeText={(phoneNumberFormat) => {
                                let phoneNumber = phoneNumberFormat.toString().replace(/\D+/g, '');
                                console.log(phoneNumber)
                                this.setState({ phoneNumberFormat: phoneNumberFormat, myPhone: phoneNumber })
                            }}
                            onFocus={() => this.setState({ selectedField: 'phone' })}
                            type={'cel-phone'}
                            maxLength={this.state.phoneNumberFormat.startsWith("1") ? 18 : 16}
                            options={
                                this.state.myPhone.startsWith("1") ?
                                    {
                                        dddMask: '9 (999) 999 - '
                                    } : {
                                        dddMask: '(999) 999 - '
                                    }
                            }
                            style={styles.textInput}
                        // keyboardType='phone-pad'
                        // underlineColorAndroid="transparent"
                        // placeholder="My phone"
                        // placeholderTextColor="#ababab"
                        // returnKeyType='next'
                        // autoCorrect={false}
                        // autoCapitalize="none"
                        // value={this.state.phoneNumberFormat}
                        // //value={this.state.myPhone}
                        // onSubmitEditing={() => this.inputComEmail.focus()}
                        // onChangeText={(input) => this.setState({ myPhone: input })}
                        // ref={((input) => this.inputComPhone = input)}
                        />

                        <TextInput
                            style={styles.textInput}
                            keyboardType='email-address'
                            underlineColorAndroid="transparent"
                            placeholder="My Email"
                            placeholderTextColor="#ababab"
                            returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            value={this.state.myEmail}
                            onSubmitEditing={() => this.inputComAddr1.focus()}
                            onChangeText={(input) => this.setState({ myEmail: input })}
                            ref={((input) => this.inputComEmail = input)}
                        />
                    </View>

                    <View style={{ marginTop: 20, marginLeft: 10 }}>
                        <Text style={styles.businessText}>BUSINESS ADDRESS</Text>
                    </View>

                    <View style={styles.verificationContainer}>
                        {/* 
                        <View style={styles.pickerStyle}>
                            <Picker
                                mode="dropdown"
                                iosHeader="States"
                                style={{ width: screenSize.width - 15, }}
                                selectedValue={this.state.selectedState}
                                onValueChange={this.onValueChange.bind(this)}
                            >
                                {
                                    // CommonTasks.stateList.map((s, key) => <Picker.Item key={s.abbreviation} value={s.name} label={s.name} />)
                                    states.map((s, key) => <Picker.Item key={s.name} value={s.name} label={s.name} />)
                                }
                            </Picker>
                        </View>

                        {(this.state.showCity == true) ?
                            (<View style={styles.pickerStyle}>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="States"
                                    style={{ width: screenSize.width - 15, }}
                                    selectedValue={this.state.selectedCity}
                                    onValueChange={this.onValueChangeCity.bind(this)}
                                >
                                    {
                                        //CommonTasks.stateList.map((s, key) => <Picker.Item key={s.abbreviation} value={s.name} label={s.name} />)

                                        this.state.cities.map((s, key) => <Picker.Item key={s.city} value={s.city} label={s.city} />)
                                    }
                                </Picker>
                            </View>)
                            :
                            null} */}

                        <TextInput
                            style={styles.textInput}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="State"
                            placeholderTextColor="#ababab"
                            returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            value={this.state.selectedState}
                            onSubmitEditing={() => this.selectedCity.focus()}
                            onChangeText={(input) => this.setState({ selectedState: input })}
                            ref={((input) => this.selectedState = input)}
                        />




                        <TextInput
                            style={styles.textInput}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="City"
                            placeholderTextColor="#ababab"
                            returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            value={this.state.selectedCity}
                            onSubmitEditing={() => this.inputPostcode.focus()}
                            onChangeText={(input) => this.setState({ selectedCity: input })}
                            ref={((input) => this.selectedCity = input)}
                        />


                        <TextInput
                            style={styles.textInput}
                            keyboardType='numeric'
                            underlineColorAndroid="transparent"
                            placeholder="Postal code"
                            placeholderTextColor="#ababab"
                            returnKeyType='done'
                            autoCorrect={false}
                            autoCapitalize="none"
                            value={this.state.postcode}
                            // onSubmitEditing={() => this.inputComPhone.focus()}
                            onChangeText={(input) => this.setState({ postcode: input })}
                            ref={((input) => this.inputPostcode = input)}
                        />
                    </View>

                    <TextInput
                        style={styles.textInput}
                        keyboardType='default'
                        underlineColorAndroid="transparent"
                        placeholder="line 1"
                        placeholderTextColor="#ababab"
                        returnKeyType='next'
                        autoCorrect={false}
                        autoCapitalize="none"
                        value={this.state.companyAddress1}
                        onSubmitEditing={() => this.inputComAddr2.focus()}
                        onChangeText={(input) => this.setState({ companyAddress1: input })}
                        ref={((input) => this.inputComAddr1 = input)}
                    />

                    <TextInput
                        style={styles.textInput}
                        keyboardType='default'
                        underlineColorAndroid="transparent"
                        placeholder="line 2"
                        placeholderTextColor="#ababab"
                        autoCorrect={false}
                        autoCapitalize="none"
                        value={this.state.companyAddress2}
                        onChangeText={(input) => this.setState({ companyAddress2: input })}
                        ref={((input) => this.inputComAddr2 = input)}
                    />
                    <KeyboardSpacer />
                </ScrollView>

                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>)
                    : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 15
    },

    businessText: {
        fontFamily: regularText,
        fontSize: 16,
        marginBottom: 6
    },

    businessTypeContainer: {
        backgroundColor: 'white'
    },

    infoText: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        // paddingLeft : 15,
        marginLeft: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 45
    },

    optionText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#1e1e1e'
    },

    optionImage: {
        fontSize: 34,
        color: '#007aff',
        marginRight: 14
    },

    verificationContainer: {
        backgroundColor: 'white',
        // marginVertical: 20
    },

    textInput: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17
    },

    pickerStyle: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        marginLeft: 15,
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
})