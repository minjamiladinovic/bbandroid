import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    FlatList,
    Keyboard
} from 'react-native';

import { Picker, Item } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { TextInputMask } from 'react-native-masked-text';
import MyAccount from './my-account';
import states from "../../assets/files/states";
import { StackNavigator, NavigationActions, StackActions, createStackNavigator } from 'react-navigation';
import Communications from 'react-native-communications';

var feedbackBodyStr = '';
const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class BusinessInfoEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            uid: this.props.navigation.state.params.uid,
            businessInfo: this.props.navigation.state.params.businessInfo,
            cities: [],
            zipcodeCity: [],
            zipcodeState: [],
            zipcodeState1: [],
            zipcodeSelected: [],
            selectedState: '',
            selectedCity: "",
            showState: false,
            showCity: false,
            searchArray: [],
            searchText: "",
            searchArrayCity: [],
            searchTextCity: "",
            showZip: false,
            searchArrayZip: [],
            searchTextZip: "",
        }
    }

    componentDidMount() {
        // this._selectCity(this.state.selectedCity);

        var business = this.state.businessInfo;
        var zipCode = [];
        this.setState({
            selectedState: business.CState,
            selectedCity: business.CCity,
            postcode: business.CPin,
            cities: business.cityList,
            zipcodeSelected: business.SelectedZip,
            zipcodeState: business.StateZip,
        }, this.getCityFromZip.bind(this, '35006'))

    }

    _searchStates(text) {
        //console.log(text==''?this.setState({cancel : false}): this.setState({cancel : true}))
        this.setState({ searchText: text })
        var s = states.slice();
        var searchResult = [];
        var i = 0;
        if (text != '') {
            s.forEach((data) => {
                if (data.name.toLowerCase().indexOf(text.toLowerCase()) != -1 && i < 5) {
                    searchResult.push(data);
                    i++;
                }
                if (i == 4) {
                    this.setState({ searchArray: searchResult })
                }
            })
        }
        this.setState({ searchArray: searchResult })
    }

    _selectState(value) {
        this.setState({
            selectedState: value,
        });
        for (var i = 0; i < states.length; i++) {
            if (states[i].name.trim() == value.trim()) {
                var ctites_zip = [];
                for (var key in states[i].cities) {
                    if (states[i].cities.hasOwnProperty(key)) {
                        var obj = {};
                        obj.city = key;
                        obj.zipcode = states[i].cities[key];
                        ctites_zip.push(obj);

                        //keys.push(key);
                        //data.push(obj[key]); // Not necessary, but cleaner, in my opinion. See the example below.
                    }
                }
                this.setState({
                    cities: ctites_zip,
                    // zipcodes:zipcodes
                })
                var zipCode = [];
                for (var i = 0; i < ctites_zip.length; i++) {
                    var obj = {};
                    obj.zipCode = ctites_zip[i].zipcode[0];
                    obj.show = false;
                    zipCode.push(obj)
                }

                this.setState({ showState: false })
                // this.setState({ showState: false, zipcodeState: zipCode })
                console.log(this.state.zipcodeState);
            }
        }

    }

    clearText() {
        //this.inputMessage.setNativeProps({text: ''});
        this.setState({ searchText: '', searchArray: [] })
        Keyboard.dismiss();
    }
    getCityFromZip(value) {
        let city = '';
        let state = '';
        states.forEach(el => {//contains obj state
            if (el.name == this.state.selectedState) {
                state=el.name;
                Object.keys(el.cities).forEach(key => {//array of cities name in this state
                    if (key == this.state.selectedCity) {
                        // el.cities[key].forEach(el => {
                        //     if (el == value) {
                        //         city=key;
                        //     }
                        // })
                    }
                })
            }
        })
        return value.toString()+'-'+city+','+state;
    }

    _searchCities(text) {
        this.setState({ searchTextCity: text })
        var s = this.state.cities.slice();
        var searchResult = [];
        var i = 0;
        if (text != '') {
            s.forEach((data) => {
                if (data.city.toLowerCase().indexOf(text.toLowerCase()) != -1 && i < 5) {
                    searchResult.push(data);
                    i++;
                }
                if (i == 4) {
                    this.setState({ searchArrayCity: searchResult })
                }
            })
        }
        this.setState({ searchArrayCity: searchResult })
    }

    _selectCity(value) {
        let tempArr = [];
        this.setState({
            selectedCity: value
        });
        for (var i = 0; i < this.state.cities.length; i++) {
            if (this.state.cities[i].city.trim() == value.trim()) {
                this.setState({ zipcodeCity: this.state.cities[i].zipcode })
                this.state.cities[i].zipcode.forEach(element => {
                    let tempObj = {};
                    tempObj.zipCode = element;
                    tempObj.show = false;
                    tempArr.push(tempObj);
                });
            }

        }
        this.setState({ showCity: false, zipcodeState: tempArr })
        console.log(this.state.zipcodeCity)

    }

    clearTextCity() {
        //this.inputMessage.setNativeProps({text: ''});
        this.setState({ searchTextCity: '', searchArrayCity: [] })
        Keyboard.dismiss();
    }

    _selectZip(index, zipCode) {
        var newArray = this.state.zipcodeState.slice();
        newArray[index] = {
            zipCode: newArray[index].zipCode,
            show: newArray[index].show == false ? true : false,
        };
        //console.log("new data array" + newArray);
        this.setState({
            zipcodeState: newArray,
        })


        if (this.state.zipcodeSelected.indexOf(zipCode) === -1) {
            //this.state.serviceValue.push(name);
            this.state.zipcodeSelected.push(zipCode);
        }
        else {
            var array = this.state.zipcodeSelected;
            array.forEach(function (element, i, object) {
                console.log("service element" + element);
                if (element == zipCode) {
                    //alert("del");
                    object.splice(i, 1);
                }
            });
            this.setState({
                zipcodeSelected: array
            })
        }
        //console.log("service" + this.state.serviceValue);
    }

    _searchZip(text) {
        //console.log(text==''?this.setState({cancel : false}): this.setState({cancel : true}))
        this.setState({ searchTextZip: text })
        var s = this.state.zipcodeState.slice();
        var searchResult = [];
        var i = 0;
        if (text != '') {
            s.forEach((data) => {
                if (data.zipCode.toString().toLowerCase().indexOf(text.toString().toLowerCase()) != -1 && i < 5) {
                    searchResult.push(data);
                    i++;
                }
                if (i == 4) {
                    this.setState({ searchArrayZip: searchResult })
                }
            })
        }
        this.setState({ searchArrayZip: searchResult })
    }

    _selectZip2(index, zipCode) {
        var tempZipsShowValArr = this.state.zipcodeState;
        var tempZipsArr = this.state.zipcodeSelected;
        tempZipsShowValArr.forEach((element) => {//removing that zip and changin show status of city zips list.
            if (element.zipCode == zipCode) {
                element.show = true ? false : true;
            }
        })
        if (tempZipsArr.indexOf(zipCode) === -1) {
            tempZipsArr.push(zipCode);
        } else {
            tempZipsArr.splice(tempZipsArr.indexOf(zipCode), 1)
        }
        this.setState({
            zipcodeState: tempZipsShowValArr,
            zipcodeSelected: tempZipsArr
        })
    }


    _removeZip(zipCode) {
        var tempZipsSelected = this.state.zipcodeSelected;
        tempZipsSelected.splice(tempZipsSelected.indexOf(zipCode), 1);//removing that zipcode from simple arr
        var tempZipsShowValuesArr = this.state.zipcodeState;
        tempZipsShowValuesArr.forEach((element) => {//according that zip changing show status of city zips list.
            if (element.zipCode == zipCode) {
                element.show = !element.show;
            }
        })
        this.setState({
            zipcodeSelected: tempZipsSelected,
            zipcodeState: tempZipsShowValuesArr
        })
    }


    removeTextZip() {
        this.setState({ searchTextZip: '', searchArrayZip: [] })
        Keyboard.dismiss();
    }
    _addBusiness() {
        this.setState({ isLoading: true });
        db.collection("users").doc(this.state.uid).update({
            BusinessInfo:
            {
                "BusinessType": this.state.businessInfo.BusinessType,
                "CName": this.state.businessInfo.CName,
                "CPhone": this.state.businessInfo.CPhone,
                "CEmail": this.state.businessInfo.CEmail,
                "CAddress1": this.state.businessInfo.CAddress1,
                "CAddress2": this.state.businessInfo.CAddress2,
                "CCity": this.state.selectedCity,
                "CState": this.state.selectedState,
                "cityList": this.state.cities,
                "CPin": this.state.zipcodeCity,
                "StateZip": this.state.zipcodeState,
                "SelectedZip": this.state.zipcodeSelected,
            }
        }).then((user) => {
            var objInfo = {
                // "CPhone": this.state.myPhone,
                // "CCity": this.state.city,
                // "CName": this.state.companyName,
                // "CAddress1": this.state.companyAddress1,
                // "CEmail": this.state.myEmail,
                // "CAddress2": this.state.companyAddress2,
                // "CPin": this.state.postcode,
                // "BusinessType": this.state.businessText,
                // "CState": this.state.selectedState
                "BusinessType": this.state.businessInfo.BusinessType,
                "CName": this.state.businessInfo.CName,
                "CPhone": this.state.businessInfo.CPhone,
                "CEmail": this.state.businessInfo.CEmail,
                "CAddress1": this.state.businessInfo.CAddress1,
                "CAddress2": this.state.businessInfo.CAddress2,
                "CCity": this.state.selectedCity,
                "CState": this.state.selectedState,
                "cityList": this.state.cities,
                "CPin": this.state.zipcodeCity,
                "StateZip": this.state.zipcodeState,
                "SelectedZip": this.state.zipcodeSelected,
            };
            MyAccount._changeBusinessInfo(objInfo);
            this.setState({ isLoading: false });
            console.log("user data" + user);
            //this.props.navigation.pop();
            // CommonTasks._displayToast("Successfully updated Business Info");
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({
                    routeName: 'ProjectSearchScreen',
                    params:
                    {
                        uid: this.state.user_id
                    }

                })],
            });
            this.props.navigation.dispatch(resetAction);
        }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error);
        })
    }

    render() {
        return (
            <View style={styles.container}>

                {(this.state.showState == false && this.state.showCity == false && this.state.showZip == false) ?
                    (<View>
                        <View style={styles.headerContainer}>
                            <TouchableOpacity style={styles.backContainer}
                                onPress={() => this.props.navigation.goBack()}>
                                <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                                <Text style={styles.signinText}>Back</Text>
                            </TouchableOpacity>

                            <Text style={styles.headerText}>Business service area</Text>

                            <TouchableOpacity onPress={() => this._addBusiness()}>
                                <Text style={styles.nextText}>Save</Text>
                            </TouchableOpacity>

                        </View>
                        <View>
                            <View style={{ marginTop: 20, marginLeft: 10 }}>
                                <Text style={styles.businessText}>ADDRESS</Text>
                            </View>
                            <View style={styles.businessTypeContainer}>
                                <TouchableOpacity style={styles.infoText} onPress={() => this.setState({ showState: true })}>
                                    <Text style={styles.optionText}>State</Text>
                                    <View style={{ flexDirection: "row" }}>
                                        <Text style={[styles.optionText, { marginRight: 14 }]}>{this.state.selectedState}</Text>
                                        <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                                    </View>


                                </TouchableOpacity>
                                <TouchableOpacity style={styles.infoText} onPress={() => this.setState({ showCity: true })}>
                                    <Text style={styles.optionText}>City</Text>
                                    <View style={{ flexDirection: "row" }}>
                                        <Text style={[styles.optionText, { marginRight: 14 }]}>{this.state.selectedCity}</Text>
                                        <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                                    </View>

                                </TouchableOpacity>
                            </View>

                            <View style={{ marginTop: 20, marginLeft: 10 }}>
                                <Text style={styles.businessText}>ZIP CODE</Text>
                            </View>
                            <View style={styles.businessTypeContainer}>

                                <TouchableOpacity style={styles.zipCodeAdd}
                                    onPress={() => this.setState({ showZip: true })}
                                >
                                    <Text style={styles.businessText}>Add new zip code</Text>
                                    <Entypo name="plus" style={{ fontSize: 20, color: "black" }} />
                                </TouchableOpacity>



                                <View style={{}}>
                                    <FlatList
                                        contentContainerStyle={{ width: screenSize.width - 20 }}
                                        pagingEnabled={true}
                                        numColumns={2}
                                        data={this.state.zipcodeSelected}
                                        key={this.state.zipcodeSelected.length}
                                        extraData={this.state}
                                        refreshing={true}
                                        renderItem={({ item, index }) =>
                                            <View style={styles.tagView}
                                            >
                                                {/* <Text style={{ fontFamily: regularText, fontSize: 10, color: "#ffff", marginLeft: 5, fontWeight: "800" }}>{item.toString() + '-' + this.state.selectedCity + ', ' + this.state.selectedState}</Text> */}
                                                <Text style={{ fontFamily: regularText, fontSize: 10, color: "#ffff", marginLeft: 5, fontWeight: "800" }}>{this.getCityFromZip(item)}</Text>
                                                <TouchableOpacity onPress={() => this._removeZip(item)}>
                                                    <MaterialCommunityIcons name="close" style={{ fontSize: 15, color: "#ffff" }} />
                                                </TouchableOpacity>
                                            </View>
                                        }
                                        keyExtractor={item => item}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>)
                    :
                    null}

                {(this.state.showState == true) ?
                    (<View>
                        <View style={styles.headerContainer}>
                            <TouchableOpacity style={styles.backContainer}
                                onPress={() => this.setState({ showState: false })}>
                                <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                                <Text style={styles.signinText}>Back</Text>
                            </TouchableOpacity>
                            <Text style={styles.headerText}>States</Text>
                            <Text style={styles.nextText}></Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 3 }}>

                            <TextInput style={{ borderBottomWidth: 0.5, width: screenSize.width - 50, padding: -10, paddingLeft: 15 }}
                                underlineColorAndroid='rgba(200, 200, 200, 0)'
                                onChangeText={(text) => this._searchStates(text)}
                                keyboardType='default'
                                value={this.state.searchText}
                                placeholder="Search"
                                placeholderTextColor="#ababab"
                                autoCorrect={false}
                                autoCapitalize="none"
                                autoComplete='off'
                                ref={((input) => this.inputMessage = input)}>
                            </TextInput>
                            {(this.state.searchText == '') ?
                                <TouchableOpacity>
                                    <Feather name='search' size={23} style={{ padding: 4, paddingLeft: 10, }} ></Feather>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.clearText()}>
                                    <MaterialIcons name='cancel' size={23} style={{ padding: 4, paddingLeft: 10, }} ></MaterialIcons>
                                </TouchableOpacity>
                            }

                        </View>

                        {(this.state.searchArray.length) ?
                            (<View style={{ width: screenSize.width, backgroundColor: '#fff', maxHeight: screenSize.height - 120, backgroundColor: '#fff', paddingHorizontal: 10, elevation: 3, paddingBottom: 40 }}>
                                <FlatList
                                    keyboardShouldPersistTaps={true}
                                    data={this.state.searchArray}
                                    renderItem={({ item, index }) =>
                                        <TouchableOpacity style={styles.itemContainer}
                                            onPress={() => this._selectState(item.name)}
                                        >
                                            <Text style={styles.itemText}>{item.name}</Text>
                                            {(item.name == this.state.selectedState) ?
                                                (<Ionicons name="ios-checkmark" style={styles.optionImage1} />)
                                                :
                                                null}
                                        </TouchableOpacity>
                                    }
                                    keyExtractor={item => item.name}
                                />
                            </View>)
                            :
                            null}

                        {(this.state.searchArray.length == 0) ?
                            (<View style={styles.verificationContainer}>
                                <FlatList
                                    data={states}
                                    renderItem={({ item, index }) =>
                                        <TouchableOpacity style={styles.itemContainer}
                                            onPress={() => this._selectState(item.name)}
                                        >
                                            <Text style={styles.itemText}>{item.name}</Text>
                                            {(item.name == this.state.selectedState) ?
                                                (<Ionicons name="ios-checkmark" style={styles.optionImage1} />)
                                                :
                                                null}
                                        </TouchableOpacity>
                                    }
                                    keyExtractor={item => item.name}
                                />
                            </View>)
                            :
                            null}
                    </View>)
                    :
                    null}


                {(this.state.showCity == true) ?
                    (<View>
                        <View style={styles.headerContainer}>
                            <TouchableOpacity style={styles.backContainer}
                                onPress={() => this.setState({ showCity: false })}>
                                <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                                <Text style={styles.signinText}>Back</Text>
                            </TouchableOpacity>
                            <Text style={styles.headerText}>Cities</Text>
                            <Text style={styles.nextText}></Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 3 }}>

                            <TextInput style={{ borderBottomWidth: 0.5, width: screenSize.width - 50, padding: -10, paddingLeft: 15 }}
                                underlineColorAndroid='rgba(200, 200, 200, 0)'
                                onChangeText={(text) => this._searchCities(text)}
                                keyboardType='default'
                                value={this.state.searchTextCity}
                                placeholder="Search"
                                placeholderTextColor="#ababab"
                                autoCorrect={false}
                                autoCapitalize="none"
                                autoComplete='off'
                                ref={((input) => this.inputMessage = input)}>
                            </TextInput>
                            {(this.state.searchTextCity == '') ?
                                <TouchableOpacity>
                                    <Feather name='search' size={23} style={{ padding: 4, paddingLeft: 10, }} ></Feather>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.clearTextCity()}>
                                    <MaterialIcons name='cancel' size={23} style={{ padding: 4, paddingLeft: 10, }} ></MaterialIcons>
                                </TouchableOpacity>
                            }
                        </View>
                        {(this.state.searchArrayCity.length) ?
                            (<View style={{ width: screenSize.width, backgroundColor: '#fff', maxHeight: screenSize.height - 120, backgroundColor: '#fff', paddingHorizontal: 10, elevation: 3, paddingBottom: 40 }}>
                                <FlatList
                                    keyboardShouldPersistTaps={true}
                                    data={this.state.searchArrayCity}
                                    renderItem={({ item, index }) =>
                                        <TouchableOpacity style={styles.itemContainer}
                                            onPress={() => this._selectCity(item.city)}
                                        >
                                            <Text style={styles.itemText}>{item.city}</Text>
                                            {(item.city == this.state.selectedCity) ?
                                                (<Ionicons name="ios-checkmark" style={styles.optionImage1} />)
                                                :
                                                null}
                                        </TouchableOpacity>
                                    }
                                    keyExtractor={item => item.city}
                                />
                            </View>)
                            :
                            null}
                        {(this.state.searchArrayCity.length == 0) ?
                            (<View style={styles.verificationContainer}>
                                <FlatList
                                    data={this.state.cities}
                                    renderItem={({ item, index }) =>
                                        <TouchableOpacity style={styles.itemContainer}
                                            onPress={() => this._selectCity(item.city)}
                                        >
                                            <Text style={styles.itemText}>{item.city}</Text>
                                            {(item.city == this.state.selectedCity) ?
                                                (<Ionicons name="ios-checkmark" style={styles.optionImage1} />)
                                                :
                                                null}
                                        </TouchableOpacity>
                                    }
                                    keyExtractor={item => item.city}
                                />
                            </View>)
                            :
                            null}
                    </View>)
                    :
                    null}


                {(this.state.showZip == true) ?
                    (<View>
                        <View style={styles.headerContainer}>
                            <TouchableOpacity style={styles.backContainer}
                                onPress={() => this.setState({ showZip: false })}>
                                <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                                <Text style={styles.signinText}>Back</Text>

                            </TouchableOpacity>
                            <Text style={styles.headerText}>Zipcodes</Text>
                            <Text style={styles.nextText} onPress={() => this.setState({ showZip: false })}>Done</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 3 }}>

                            <TextInput style={{ borderBottomWidth: 0.5, width: screenSize.width - 50, padding: -10, paddingLeft: 15 }}
                                underlineColorAndroid='rgba(200, 200, 200, 0)'
                                onChangeText={(text) => this._searchZip(text)}
                                keyboardType='default'
                                value={this.state.searchTextZip}
                                placeholder="Search"
                                placeholderTextColor="#ababab"
                                autoCorrect={false}
                                autoCapitalize="none"
                                autoComplete='off'
                                ref={((input) => this.inputMessage = input)}>
                            </TextInput>
                            {(this.state.searchTextZip == '') ?
                                <TouchableOpacity>
                                    <Feather name='search' size={23} style={{ padding: 4, paddingLeft: 10, }} ></Feather>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.removeTextZip()}>
                                    <MaterialIcons name='cancel' size={23} style={{ padding: 4, paddingLeft: 10, }} ></MaterialIcons>
                                </TouchableOpacity>
                            }
                        </View>
                        {(this.state.searchArrayZip.length) ?
                            (<View style={{ width: screenSize.width, backgroundColor: '#fff', maxHeight: screenSize.height - 120, backgroundColor: '#fff', paddingHorizontal: 10, elevation: 3, paddingBottom: 40 }}>
                                <FlatList
                                    keyboardShouldPersistTaps={true}
                                    data={this.state.searchArrayZip}
                                    renderItem={({ item, index }) =>
                                        <TouchableOpacity style={styles.itemContainer}
                                            onPress={() => this._selectZip2(index, item.zipCode)}
                                        >
                                            <Text style={styles.itemText}>{item.zipCode.toString() + '-' + this.state.selectedCity + ', ' + this.state.selectedState}</Text>
                                            {(item.show == true) ?
                                                (<Ionicons name="ios-checkmark" style={styles.optionImage1} />)
                                                :
                                                null}
                                        </TouchableOpacity>
                                    }
                                    keyExtractor={item => item.zipCode}
                                />
                            </View>)
                            :
                            null}
                        {(this.state.searchArrayZip.length == 0) ?
                            (<View style={styles.verificationContainer}>
                                <FlatList
                                    data={this.state.zipcodeState}
                                    renderItem={({ item, index }) =>
                                        <TouchableOpacity style={styles.itemContainer}
                                            onPress={() => this._selectZip(index, item.zipCode)}
                                        >
                                            <Text style={styles.itemText}>{item.zipCode.toString() + '-' + this.state.selectedCity + ', ' + this.state.selectedState}</Text>
                                            {(item.show == true) ?
                                                (<Ionicons name="ios-checkmark" style={styles.optionImage1} />)
                                                :
                                                null}
                                        </TouchableOpacity>
                                    }
                                    keyExtractor={item => item.zipCode}
                                />
                            </View>)
                            :
                            null}
                    </View>)
                    :
                    null}
                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>)
                    : null}
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 15
    },

    businessText: {
        fontFamily: regularText,
        fontSize: 16,
        marginBottom: 6
    },

    businessTypeContainer: {
        backgroundColor: 'white'
    },

    infoText: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        // paddingLeft : 15,
        marginLeft: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 45
    },

    optionText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#1e1e1e'
    },

    optionImage: {
        fontSize: 34,
        color: '#8e8e93',
        marginRight: 14
    },

    verificationContainer: {
        backgroundColor: 'white',
        // marginVertical: 20
    },

    textInput: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17
    },

    pickerStyle: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        marginLeft: 15,
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
    zipCodeAdd: {
        marginTop: 20,
        marginLeft: 10,
        width: screenSize.width - 30,
        paddingHorizontal: 15,
        backgroundColor: "#EFEFF3",
        height: 40,
        borderRadius: 20,
        alignSelf: 'center',
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 20
    },
    verificationContainer: {
        backgroundColor: 'white',
        marginVertical: 0,
        elevation: 1,
        maxHeight: screenSize.height - 102
    },

    verificationText: {
        fontFamily: regularText,
        fontSize: 15,
        marginHorizontal: 15
    },
    itemContainer: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
        // paddingLeft : 15,
        marginLeft: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    itemText: {
        fontFamily: regularText,
        fontSize: 18,
        paddingVertical: 7
    },
    optionImage1: {
        fontSize: 34,
        color: '#007aff',
        marginRight: 14
    },
    tagView: {
        backgroundColor: "#3A4958",
        paddingHorizontal: 5,
        height: 30,
        borderRadius: 15,
        // width: 100,
        justifyContent: "space-between",
        alignItems: "center",
        margin: 3,
        flexDirection: "row"
    }
})