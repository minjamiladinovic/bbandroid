// Create account page 2
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/verify-phone-number.png

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    FlatList,
    Keyboard,
    Alert,
    BackHandler
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';
import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions, StackActions } from 'react-navigation';


const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();


export default class ServiceEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            summary: '',
            service: [],
            isLoading: true,
            serviceValue:this.props.navigation.state.params.serviceShow,
            serviceShow: this.props.navigation.state.params.serviceShow,
            uid: this.props.navigation.state.params.uid,
            searchText : '',
            searchArray : [],
            cancel : false,
        }
    }

    componentDidMount() {
        console.log('services :  '+this.props.navigation.state.params.serviceShow)
        var serviceData = CommonTasks.serviceList;
        var service = [];

        if (this.state.serviceShow.length == 0) {
            // var serviceData = CommonTasks.serviceList;
            // var service = [];


            for (var data of serviceData) {
                data.show = false;
                service.push(data);
            }
            this.setState({
                service: service
            }, this._stopLoading.bind(this))
        }
        else {
            var service_name = [];
            var global_service = [];
            for (var i = 0; i < serviceData.length; i++) {
                var check = this._checkid(serviceData[i].name,serviceData[i].id);

                //console.log("check" + check);
                if (check == true) {
                    var obj = {
                        name: serviceData[i].name,
                        id:serviceData[i].id,
                        show: true
                    }
                    global_service.push(obj);
                    service_name.push(serviceData[i].name);
                    //console.log("done" + i);
                }
                else if (check == false) {
                    var obj = {
                        name: serviceData[i].name,
                        id:serviceData[i].id,
                        show: false
                    }
                    global_service.push(obj);
                    //console.log("hi" + i);
                }
            }
           // console.log("array interest" + JSON.stringify(global_service));

            this.setState({
                service: global_service,
                //serviceValue: service_name
            }, this._stopLoading.bind(this))

        }


        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        this.props.navigation.replace('MyAccountScreen', { uid: this.state.uid })
         return true;
       }

    _checkid(name,id) 
    {
        var serviceData = this.state.serviceShow;
        var check = false;
        //console.log(this.state.interest_edit);
        for (var j = 0; j < serviceData.length; j++) {
            console.log("select"+serviceData[j].name);
            if (name == serviceData[j].name) {
                check = true;
            }
        }
        return (check);
    }
    _stopLoading() {
        this.setState({
            isLoading: false
        })
        //console.log(this.state.service);
    }

    _checkService(index, name,id) {
        var newArray = this.state.service.slice();
        newArray[index] = {
            name: newArray[index].name,
            id: newArray[index].id,
            show: newArray[index].show == false ? true : false,
        };
        //console.log("new data array" + newArray);
        this.setState({
            service: newArray,
        })

        var obj={}
        obj.name=name;
        obj.id=id;
        var serviceCheck=false;

        this.state.serviceValue.forEach(function(item){

            if(item.id==id)
            {
                serviceCheck=true;
            }

         })

         if(serviceCheck ==true)
         {
            var array = this.state.serviceValue;
            array.forEach(function (element, i, object) {
                console.log("service element"+element);
                if (element.id==id) {
                    //alert("del");
                    object.splice(i, 1);
                }
            });
            this.setState({
                serviceValue: array
            })
         }
         else if(serviceCheck == false)
         {
            this.state.serviceValue.push(obj);
         }
        // if (this.state.serviceValue.indexOf(obj) === -1) {
        //     //this.state.serviceValue.push(name);
        //     this.state.serviceValue.push(obj);
        // }
        // else {
        //     var array = this.state.serviceValue;
        //     array.forEach(function (element, i, object) {
        //         console.log("service element"+element);
        //         if (element.id==id) {
        //             //alert("del");
        //             object.splice(i, 1);
        //         }
        //     });
        //     this.setState({
        //         serviceValue: array
        //     })
        // }
        //console.log("service" + this.state.serviceValue);
    }

    gotoNextStep() {
        if(this.state.serviceValue.length==0)
        {
            CommonTasks._displayToast("Please Select your Services");
            return false;
        }
        this.setState({ isLoading: true });
        db.collection("users").doc(this.state.uid).update({
            ServiceType: this.state.serviceValue
        }).then((user) => {
            this.setState({ isLoading: false });
           
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({
                    routeName: 'ProjectSearchScreen',
                    params:
                    {
                        uid: this.state.uid
                    }

                })],
            });
            this.props.navigation.dispatch(resetAction);
        }).catch((error) => {
            this.setState({ isLoading: false });
           // console.log("user error" + error);
            var errorCode = err.code;
            var errorMessage = err.message;
           // console.log(errorCode + "-" + errorMessage);
            CommonTasks._displayToast(errorMessage);
        })

    }



    _searchServices(text) {
        //console.log(text==''?this.setState({cancel : false}): this.setState({cancel : true}))
        this.setState({searchText : text})
        var s=this.state.service.slice();
        var searchResult=[];
        var i=0;
        if(text!='')
        {
          s.forEach((data) =>
        {
            if(data.name.toLowerCase().indexOf(text.toLowerCase())!=-1 && i<5)
            {
                searchResult.push(data);
                i++;
            }
            if(i==4)
            {
                this.setState({searchArray: searchResult})
            }
        })  
        }   
        this.setState({searchArray: searchResult})
    }


    _doneSearch()
    {
        this.setState({searchText : '',searchArray :   []})
        Keyboard.dismiss();
    }



    clearText()  {
        //this.inputMessage.setNativeProps({text: ''});
        this.setState({searchText : '',searchArray :   []})
        Keyboard.dismiss();
      }


      _checkService1(name,id) {
          var index;
        var newArray = this.state.service.slice();
        var newArray1=[]
        newArray.forEach((data) => { 
            if(data.name==name)
            {
                var obj=data
                obj.show=data.show?false:true;
                newArray1.push(obj)
            }
            else{
                var obj=data
    
                newArray1.push(obj)
            }
        })
       
        this.setState({
            service: newArray1,
        })
        var obj={}
        obj.name=name;
        obj.id=id;


        var serviceCheck=false;

        this.state.serviceValue.forEach(function(item){

            if(item.id==id)
            {
                serviceCheck=true;
            }

         })

         if(serviceCheck ==true)
         {
            var array = this.state.serviceValue;
            array.forEach(function (element, i, object) {
                console.log("service element"+element);
                if (element.id==id) {
                    //alert("del");
                    object.splice(i, 1);
                }
            });
            this.setState({
                serviceValue: array
            })
         }
         else if(serviceCheck == false)
         {
            this.state.serviceValue.push(obj);
         }


        this._doneSearch();
    }




    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.backContainer}
                        onPress={() =>this.props.navigation.replace('MyAccountScreen', { uid: this.state.uid })}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Back</Text>
                    </TouchableOpacity>

                    <Text style={styles.headerText}>Services</Text>
                    <Text style={styles.nextText} onPress={() => this.gotoNextStep()}>Save</Text>
                </View>
                <View style={{flexDirection :  'row',alignItems : 'center',marginVertical : 3}}>

                    <TextInput style = {{borderBottomWidth : 0.5,width : screenSize.width-50 ,padding : -10,paddingLeft : 15}}
                        underlineColorAndroid='rgba(200, 200, 200, 0)'
                        onChangeText={(text) => this._searchServices(text)}
                        keyboardType='default'
                        value={this.state.searchText}
                        placeholder="Search"
                        placeholderTextColor="#ababab"
                        autoCorrect={false}
                        autoCapitalize="none"
                        autoComplete='off'
                        ref={((input) => this.inputMessage = input)}>
                    </TextInput>
                    {(this.state.searchText=='')?
                     <TouchableOpacity>
                        <Feather name='search' size={23 } style={{padding : 4,paddingLeft : 10,}} ></Feather>
                     </TouchableOpacity>
                     :
                     <TouchableOpacity onPress = {() => this.clearText()}>
                        <MaterialIcons name='cancel' size={23 } style={{padding : 4,paddingLeft : 10,}} ></MaterialIcons>
                     </TouchableOpacity>
                    }
                    
                </View>
                {(this.state.searchArray.length)?
                (<View style={{width : screenSize.width, backgroundColor : '#fff',maxHeight : screenSize.height-120,backgroundColor : '#fff', paddingHorizontal : 10,elevation : 3,paddingBottom : 40 }}>
                    <FlatList
                        keyboardShouldPersistTaps={true}
                        data={this.state.searchArray}
                        renderItem={({ item, index }) =>
                            <TouchableOpacity style={styles.itemContainer}
                                onPress={() => this._checkService1(item.name,item.id)}>
                                <Text style={styles.itemText}>{item.name}</Text>
                                {(item.show == true) ?
                                    (<Ionicons name="ios-checkmark" style={styles.optionImage} />)
                                    :
                                    null}
                            </TouchableOpacity>
                        }
                        keyExtractor={item => item.name}
                    />
                    
                    
                </View>)
                :
                null}
                {(this.state.searchArray.length == 0)?
                (<View style={styles.verificationContainer}>
                    <FlatList
                        data={this.state.service}
                        renderItem={({ item, index }) =>
                            <TouchableOpacity style={styles.itemContainer}
                                onPress={() => this._checkService(index,item.name,item.id)}>
                                <Text style={styles.itemText}>{item.name}</Text>
                                {(item.show == true) ?
                                    (<Ionicons name="ios-checkmark" style={styles.optionImage} />)
                                    :
                                    null}
                            </TouchableOpacity>
                        }
                        keyExtractor={item => item.name}
                    />
                </View>)
                :
                null}
                {(this.state.isLoading == true) ?
                    (<View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                            <View style={styles.activity_sub}>
                        <ActivityIndicator
                            size="large"
                            color='#D0D3D4'
                            style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                        />
                    </View>
                            </View>)
                    : null}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    textInput: {
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17,
        textAlignVertical: 'top',
        height: 100
    },

    verificationContainer: {
        backgroundColor: 'white',
        marginVertical: 0,
        elevation: 1,
        maxHeight : screenSize.height-102
    },

    verificationText: {
        fontFamily: regularText,
        fontSize: 15,
        marginHorizontal: 15
    },

    sendCodeText: {
        textAlign: 'center',
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff',
        marginVertical: 10
    },

    itemContainer: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
        // paddingLeft : 15,
        marginLeft: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    itemText: {
        fontFamily: regularText,
        fontSize: 18,
        paddingVertical: 7
    },

    optionImage: {
        fontSize: 34,
        color: '#007aff',
        marginRight: 14
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
});