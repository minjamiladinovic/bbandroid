
/* My Account page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/account.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Alert,
    BackHandler
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';
import Footer from '../footer/footer';
import CommonTasks from '../../common-tasks/common-tasks';
import moment from 'moment';

const regularText = "SF-Pro-Display-Regular";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();
var bInfo = {};
var bSummary = ''
export default class MyAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            uid: this.props.navigation.state.params.uid,
            businessInfo: {},
            businessSummary: "",
            serviceShow: [],
            name: "",
            email: "",
            phone: "",
            firstname: "",
            lastname: "",
            imageLink: '',
            defaultImage: false,
            subscription_type: "",
            lifeTimeSubscription: false,
            subscriptionDate: ''
        }
    }

    componentDidMount() {
        //alert();
        this._getUserDetails();
        AsyncStorage.getItem('userName').then(value => this.setState({ name: value }));
        //AsyncStorage.getItem('imageLink').then(value => (value==null || value== '')?this.setState({ imageLink: '' }):this.setState({ imageLink: { uri: value } },console.log('async uri : '+this.state.imageLink)));//value!=''?this.setState({ imageLink: { uri: value } }):null

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        this.props.navigation.pop();
        return true;
    }

    _gotoBusinessInfo() {
        if (JSON.stringify(bInfo) == JSON.stringify(this.state.businessInfo)) {
            this.props.navigation.navigate('BusinessInfoEditScreen', { uid: this.state.uid, businessInfo: this.state.businessInfo })
        }
        else {
            this.props.navigation.navigate('BusinessInfoEditScreen', { uid: this.state.uid, businessInfo: bInfo })
        }
    }

    _gotoZipCodesEdit() {
        if (JSON.stringify(bInfo) == JSON.stringify(this.state.businessInfo)) {
            this.props.navigation.navigate('BusinessServiceAreaEdit', { uid: this.state.uid, businessInfo: this.state.businessInfo })
        }
        else {
            this.props.navigation.navigate('BusinessServiceAreaEdit', { uid: this.state.uid, businessInfo: bInfo })
        }
    }

    _gotoBusinessSummary() {
        if (bSummary == this.state.businessSummary)
            this.props.navigation.navigate('BusinessSummaryEditScreen', { uid: this.state.uid, businessSummary: this.state.businessSummary })
        else
            this.props.navigation.navigate('BusinessSummaryEditScreen', { uid: this.state.uid, businessSummary: bSummary })
    }

    _gotoServiceEdit() {
        this.props.navigation.replace('ServiceEditScreen', { uid: this.state.uid, serviceShow: this.state.serviceShow });

    }

    static _changeBusinessInfo(NewBusinessInfo) {
        bInfo = NewBusinessInfo;
    }

    static _changeBusinessSummary(NewBusinessSummary) {
        bSummary = NewBusinessSummary;

    }

    _set_B_Info() {
        bInfo = this.state.businessInfo;
        bSummary = this.state.businessSummary;
    }


    _getUserDetails() {
        debugger;
        this.setState({ isLoading: true });
        var docRef = db.collection("users").doc(this.state.uid);
        docRef.get().then((doc) => {
            if (doc.exists) {
                console.log("Document data:", doc.data());
                console.log(doc.data().BusinessInfo);
                console.log("image link : " + doc.data().imageLink);
                if (doc.data().imageLink == undefined || doc.data().imageLink == "" || doc.data().imageLink == null) {
                    this.setState({
                        imageLink: ''
                    })
                }
                else {
                    this.setState({
                        imageLink: { uri: doc.data().imageLink },
                        defaultImage: false
                    })

                }

                if (doc.data().subscription_start_time != undefined) {
                    let currentTimestamp = Date.now() / 1000;
                    let diff = (currentTimestamp - doc.data().subscription_start_time) / 2592000;
                    // time diff in no like 1 month 2 month
                    let diffInNumber = parseInt(diff.toString());
                    // add that much month with subscription start time
                    let newDueDate = doc.data().subscription_start_time + (2592000 * (diffInNumber + 1));
                    // convert in moment
                    var Subscription_date = moment(newDueDate * 1000).format('MMM DD, YYYY');
                    this.setState({
                        subscriptionDate: Subscription_date
                    })
                }


                AsyncStorage.setItem("SubcriptionType",doc.data().SubcriptionType);

                this.setState({
                    businessInfo: doc.data().BusinessInfo,
                    firstname: doc.data().firstname,
                    lastname: doc.data().lastname,
                    name: doc.data().firstname + " " + doc.data().lastname,
                    businessSummary: doc.data().BusinessSummary,
                    serviceShow: doc.data().ServiceType,
                    email: doc.data().email,
                    phone: doc.data().phone ? doc.data().phone : '',
                    subscription_type: doc.data().SubcriptionType,
                    lifeTimeSubscription: doc.data().lifetimeSubscription,
                    //subscriptionDate: doc.data().subscription_start_time

                }, this._set_B_Info.bind(this))
            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
            }
            this.setState({ isLoading: false });
        }).catch((eror) => {
            this.setState({ isLoading: false });
            console.log("Error getting document:", error);
        });
    }

    _signout() {
        Alert.alert(
            'Pro In Your Pocket For Pros',
            'Are you sure to logout?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Ok', onPress: () => this._confirmSignout() },
            ],
            { cancelable: false }
        )
    }
    _confirmSignout() {
        AsyncStorage.setItem('user_status', "");
        AsyncStorage.setItem('user_status', "");
        AsyncStorage.setItem('userName', "");
        AsyncStorage.setItem('imageLink', "");
        AsyncStorage.setItem('userBusinessName', "");

        this.props.navigation.replace('SigninConfirmScreen');
    }

    _goToMyAccount() {
        this.props.navigation.replace('MyAccountPreviewScreen', {
            uid: this.state.uid,
            email: this.state.email,
            name: this.state.name,
            phone: this.state.phone,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            //defaultImage:this.state.defaultImage,
            imageLink: this.state.imageLink,
            subscription_type: this.state.subscription_type,
            lifeTimeSubscription: this.state.lifeTimeSubscription,
            subscriptionDate: this.state.subscriptionDate

        })
    }

    gotoProjectSearch() {
        this.props.navigation.navigate("ProjectSearchScreen", { uid: this.state.uid });

    }




    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AccountSettingsScreen')}>
                        <MaterialCommunityIcons style={{ paddingLeft: 6 }} name="settings" color="white" size={28} />
                    </TouchableOpacity>

                    <Text style={styles.headerText}>Account</Text>
                    <View></View>
                </View>
                <ScrollView>
                    <View style={styles.avatorContainer}>
                        <View style={styles.imageContainer}>
                            {/* <Image
                                source={require('../../assets/images/profileImage.png')}
                                style={styles.imageStyle}
                            /> */}

                            {(this.state.imageLink == '' || this.state.imageLink == undefined || this.state.imageLink == null) ?
                                (
                                    <View>
                                        <Image
                                            source={require('../../assets/images/profileImage.png')}
                                            style={[styles.imageStyle,{zIndex:0}]}
                                        />
                                         {(this.state.lifeTimeSubscription)?
                                        <Image
                                            source={require('../../assets/images/medal.png')}
                                            style={{ position: "absolute", height:35, width:35, right:0,zIndex:10 }}
                                        />
                                        :
                                        null}
                                    </View>
                                )
                                :

                                (
                                    <View>
                                        <Image
                                            style={[styles.imageStyle,{zIndex:0}]}
                                            source={this.state.imageLink}

                                        />
                                        {(this.state.lifeTimeSubscription)?
                                        <Image
                                            source={require('../../assets/images/medal.png')}
                                            style={{ position: "absolute", height:35, width:35, right:0,zIndex:10 }}
                                        />
                                        :
                                        null}
                                    </View>
                                )}
                        </View>
                        <View style={{ alignItems: 'center', width: screenSize.width - 80, justifyContent: 'center' }}>
                            <Text style={styles.avatorName}>{this.state.name}</Text>
                            <Text style={{ fontSize: 20, fontFamily: regularText, }}> Pro in your Pocket</Text>
                        </View>

                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ backgroundColor: 'white', }}
                        onPress={() => this._gotoBusinessInfo()}
                    >
                        <View style={styles.optionContainer}>
                            <Text style={styles.optionText}>Business Info</Text>
                            <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ backgroundColor: 'white', }}
                        onPress={() => this._gotoBusinessSummary()}
                    >
                        <View style={styles.optionContainer}>
                            <Text style={styles.optionText}>Business Summary</Text>
                            <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ backgroundColor: 'white', }}
                        onPress={() => this._gotoServiceEdit()}
                    >
                        <View style={styles.optionContainer}>
                            <Text style={styles.optionText}>Business Services</Text>
                            <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ backgroundColor: 'white', }}
                        onPress={() => this._gotoZipCodesEdit()}
                    >
                        <View style={styles.optionContainer}>
                            <Text style={styles.optionText}>Business Service Area</Text>
                            <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ backgroundColor: 'white', }}
                        onPress={() => this._goToMyAccount()}
                    >
                        <View style={styles.optionContainer}>
                            <Text style={styles.optionText}>My Account</Text>
                            <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ backgroundColor: 'white', }}
                        onPress={() => this.props.navigation.navigate('ChangePasswordScreen', { uid: this.state.uid, email: this.state.email })}
                    >
                        <View style={styles.optionContainer}>
                            <Text style={styles.optionText}>Password</Text>
                            <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                        </View>
                    </TouchableOpacity>
                </ScrollView>


                <Footer itemColor='account'></Footer>


                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>
                    )
                    : null}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    headerContainer: {
        backgroundColor: '#3A4958',
        paddingTop: Platform.OS === 'ios' ? 50 : 30,
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    headerText: {
        fontFamily: boldText,
        color: 'white',
        fontSize: 22
    },

    avatorContainer: {
        backgroundColor: 'white',
        marginVertical: 10,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },

    imageContainer: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        height: 80,
        backgroundColor: '#fff',
        borderRadius: 40,
    },

    imageStyle: {
        width: 80,
        height: 80,
        borderRadius: 40
    },

    avatorName: {
        fontFamily: boldText,
        fontSize: 20,
        color: '#1e1e1e',
        marginLeft: 10
    },

    optionContainer: {
        flexDirection: 'row',
        borderBottomColor: '#8e8e93',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 0.7,
        marginLeft: 10,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10
    },

    optionText: {
        fontFamily: regularText,
        fontSize: 19
    },

    optionImage: {
        fontSize: 20,
        color: '#8e8e93'
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
})