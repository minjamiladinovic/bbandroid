// Create account page 2
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/verify-phone-number.png

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Alert
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';
import CommonTasks from '../../common-tasks/common-tasks';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { TextInputMask } from 'react-native-masked-text';


const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class EditPhoneVerify extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
            phone: this.props.navigation.state.params.phone,
            uid: this.props.navigation.state.params.uid,
            isLoading: false,
            verificationId:this.props.navigation.state.params.verificationId,
            verfication_statement:this.props.navigation.state.params.verfication_statement,
            firstname:this.props.navigation.state.params.firstname,
            lastname:this.props.navigation.state.params.lastname,
            email:this.props.navigation.state.params.email,
            phoneNumberFormat : this.props.navigation.state.params.phoneNumberFormat,
            phoneNumberFormat1 : this.props.navigation.state.params.phoneNumberFormat,

        }
    }
    componentDidMount() {
        alert("A verification code has been sent to " + this.state.phoneNumberFormat1);
        //this._phoneVerify();
    }

    gotoNextStep() {
        if(this.state.code=='')
        {
            CommonTasks._displayToast("Please enter the Code to Verify");
        }
        else if(this.state.code.length<=3)
        {
            CommonTasks._displayToast("Please enter a valid Code to Verify");
        }
        else{
            this._codeConfirm();
        }
        //this.props.navigation.navigate('CreateAccountStep3Screen');
        
    }


    _phoneVerify() {
        var phno=this.state.phone;
        if(phno.length<10)
        {
            CommonTasks._displayToast("Please Enter a valid Phone number");  
            return false;
        }
        console.log(this.state.phone)
        if(phno.length==11){phno='+'+phno}
        else {phno='+1'+phno}
        this.setState({ isLoading:true , phoneNumberFormat1: this.state.phoneNumberFormat })
        firebase.auth().verifyPhoneNumber(phno)
        .then((credential) => {
                //this.setState({ isLoading: false });
                this.setState({verificationId: credential.verificationId, isLoading: false, verfication_statement: true })
                console.log(credential);
                alert("A verification code has been sent to " + this.state.phoneNumberFormat1);
            }) // onVerificationCompleted
            .catch((err) => {
                this.setState({ isLoading: false, verfication_statement: false });
                console.log(err);
                var errorCode = err.code;
                var errorMessage = err.message;
                console.log(errorCode + "-" + errorMessage);
                //CommonTasks._displayToast(errorMessage);
                alert(errorMessage);
            });

            // firebase.auth().verifyPhoneNumber(this.state.phone,
            //     60,                 // Timeout duration
            //     TimeUnit.SECONDS,   // Unit of timeout
            //     this,               // Activity (for callback binding)
            //     mCallbacks);
    }

    _codeConfirm() {
        this.setState({ isLoading: true })
        const credential = firebase.auth.PhoneAuthProvider.credential(this.state.verificationId, this.state.code);
        console.log("credential" + JSON.stringify(credential));
        firebase.auth().signInWithCredential(credential)
            .then((data) => {
                this.setState({ isLoading: false })
                console.log("confirm" + data);
                db.collection("users").doc(this.state.uid).update({
                    phone: this.state.phone,
                    phVerify_status:'Y',
                    firstname:this.state.firstname,
                    lastname:this.state.lastname
                }).then((user) => {
                    this.setState({ isLoading: false });
                    //console.log("update data"+user);
                }).catch((err) => {
                    this.setState({ isLoading: false });
                    console.log("update err" + err);
                })
                CommonTasks._displayToast("Phone Number Successfully verified");
                this.props.navigation.replace('MyAccountScreen',{uid : this.state.uid}
                // 'MyAccountPreviewScreen', {
                //     uid: this.state.uid,
                //     email: this.state.email, 
                //     name: this.state.name,
                //     phone: this.state.phone,
                //     firstname: this.state.firstname,
                //     lastname: this.state.lastname
                // }
                )

            })
            .catch((err) => {
                this.setState({ isLoading: false })
                console.log("err" + err);
                //var errorMessage = err.message;
                CommonTasks._displayToast(err.message);
            })

    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.signinText}></Text>
                    <Text style={styles.headerText}>Verify Phone</Text>
                    <Text style={styles.nextText} onPress={() => this.gotoNextStep()}>Next</Text>
                </View>

                <View style={styles.verificationContainer}>
                    <TextInput
                        style={styles.textInput}
                        keyboardType='numeric'
                        underlineColorAndroid="transparent"
                        placeholder="Verification code"
                        placeholderTextColor="#ababab"
                        returnKeyType='done'
                        autoCorrect={false}
                        autoCapitalize="none"
                        // onSubmitEditing={() => this.inputLname.focus()}
                        onChangeText={(input) => this.setState({ code: input })}
                        ref={((input) => this.inputCode = input)}
                    />
                </View>

                {(this.state.verfication_statement == true) ?
                    (<Text style={styles.verificationText}>A verification code was sent to &nbsp;
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{this.state.phoneNumberFormat1}.</Text>
                    </Text>)
                    :
                    null}

                <View style={styles.verificationContainer}>
                <TextInputMask
            placeholder="Phone number"
            returnKeyType="done"
            value={this.state.phoneNumberFormat}
            onChangeText={phoneNumberFormat => {
              let phoneNumber = phoneNumberFormat
                .toString()
                .replace(/\D+/g, "");
              this.setState({
                phoneNumberFormat: phoneNumberFormat,
                phone: phoneNumber,
                verfication_statement: false
              });
            }}
            onFocus={() => this.setState({ selectedField: "phone" })}
            type={"cel-phone"}
            maxLength={
              this.state.phoneNumberFormat.toString().startsWith("1") ? 18 : 16
            }
            options={
              this.state.phone.startsWith("1")
                ? {
                    dddMask: "9 (999) 999 - "
                  }
                : {
                    dddMask: "(999) 999 - "
                  }
            }
            style={styles.textInput}
          />
                    {/* <TextInput
                        style={styles.textInput}
                        keyboardType='phone-pad'
                        underlineColorAndroid="transparent"
                        placeholder="Phone number"
                        placeholderTextColor="#ababab"
                        returnKeyType='done'
                        autoCorrect={false}
                        autoCapitalize="none"
                        // onSubmitEditing={() => this.inputLname.focus()}
                        onChangeText={(input) => this.setState({ phone: input })}
                        ref={((input) => this.inputPhone = input)}
                    /> */}

                    <TouchableOpacity
                        onPress={() => this._phoneVerify()}>
                        <Text style={styles.sendCodeText}>Resend Code</Text>
                    </TouchableOpacity>

                </View>

                {/* <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('CreateAccountStep3Screen', { uid: this.state.uid })}
                    style={{marginTop:20}}
                >
                    <Text style={styles.sendCodeText}>Skip</Text>
                </TouchableOpacity> */}


                {(this.state.isLoading == true) ?
                    (
                        <View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                         <View style={styles.activity_sub}>
                        <ActivityIndicator
                            size="large"
                            color='#D0D3D4'
                            style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                        />
                    </View>   
                        </View>)
                    : null}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    textInput: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17
    },

    verificationContainer: {
        backgroundColor: 'white',
        marginVertical: 20
    },

    verificationText: {
        fontFamily: regularText,
        fontSize: 15,
        marginLeft: 15
    },

    sendCodeText: {
        textAlign: 'center',
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff',
        marginVertical: 10
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
});