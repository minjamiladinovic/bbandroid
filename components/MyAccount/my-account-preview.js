import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Alert,
    BackHandler
} from 'react-native';

import { TextInputMask } from 'react-native-masked-text';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationActions, StackActions } from 'react-navigation';

const regularText = "SF-Pro-Display-Regular";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');

export default class MyAccountPreview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.navigation.state.params.name,
            uid: this.props.navigation.state.params.uid,
            email: this.props.navigation.state.params.email,
            phone: this.props.navigation.state.params.phone,
            phoneFormat: this.props.navigation.state.params.phone.charAt(0) == '+' ? this.props.navigation.state.params.phone.substring(1) : this.props.navigation.state.params.phone,
            firstname: this.props.navigation.state.params.firstname,
            lastname: this.props.navigation.state.params.lastname,
            //defaultImage: this.props.navigation.state.params.defaultImage,
            imageLink: this.props.navigation.state.params.imageLink,
            subscription_type: this.props.navigation.state.params.subscription_type,
            lifeTimeSubscription: this.props.navigation.state.params.lifeTimeSubscription,
            subscriptionDate: this.props.navigation.state.params.subscriptionDate
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        this.props.navigation.replace('MyAccountScreen', { uid: this.state.uid });
        return true;
    }

    gotoEditAccount() {
        this.props.navigation.replace('EditAccountScreen',
            {
                uid: this.state.uid,
                email: this.state.email,
                phone: this.state.phone,
                firstname: this.state.firstname,
                lastname: this.state.lastname,
                //defaultImage:this.state.defaultImage,
                imageLink: this.state.imageLink,
                lifeTimeSubscription:this.state.lifeTimeSubscription
            });
    }

    _signout() {
        Alert.alert(
            'Pro In Your Pocket For Pros',
            'Are you sure to logout?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Ok', onPress: () => this._confirmSignout() },
            ],
            { cancelable: false }
        )
    }



    _confirmSignout() {
        firebase.auth().signOut().then((success) => {
            console.log('Signed Out');
            AsyncStorage.setItem('user_id', "");
            AsyncStorage.setItem('user_status', "");
            var fcmToken = AsyncStorage.getItem('fcmToken');;

            db.collection("sp_tokens").doc(fcmToken).delete().then(function () {
                console.log("Document successfully deleted!");
            }).catch(function (error) {
                console.error("Error removing document: ", error);
            });
            //this.props.navigation.replace('SigninConfirmScreen');
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({
                    routeName: 'SigninConfirmScreen',
                    //routeName: 'DriverPayScreen',

                })],
            });
            this.props.navigation.dispatch(resetAction);
        })
            .catch((err) => {
                console.error('Sign Out Error', err);
            });

    }

    _goToCancelSubscription() {
        this.props.navigation.navigate("CancelSubscriptionScreen", {
            uid: this.state.uid
        });
    }


    _goToBuySubscription() {
        this.props.navigation.replace('BuySubscriptionScreen', {
            projectId: "",
            ownerId: '',
            description: '',
            serviceId: '',
            createdAt: '',
            bidderIdList: '',
            imageLink: '',
            ownerName: '',
            postalCode: '',
            timeString: '',
            Pimage: '',
            bidderId: '',
            navigateTo: "my-acc"
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    {/* <TouchableOpacity style={styles.backContainer}
                        onPress={() => this._signout()}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Sign Out</Text>
                    </TouchableOpacity> */}

                    <TouchableOpacity style={styles.backContainer} onPress={() => this.props.navigation.replace('MyAccountScreen', { uid: this.state.uid })}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Back</Text>
                    </TouchableOpacity>

                    <Text style={styles.headerText}>My Account</Text>
                    <TouchableOpacity onPress={() => this.gotoEditAccount()}>
                        <Text style={styles.nextText}>Edit</Text>
                    </TouchableOpacity>

                </View>
                <ScrollView style={{ backgroundColor: "#ffff" }}>
                    <View style={styles.profileContainer}>

                        <View style={[styles.avatorStyle]}>
                            {(this.state.imageLink == undefined || this.state.imageLink == "" || this.state.imageLink == null) ?

                                (<View>
                                    <Image
                                        source={require('../../assets/images/profileImage.png')}
                                        style={[styles.avatorStyle, { borderColor: "rgba(0,0,0,1)", borderWidth: .5, zIndex: 0 }]}
                                    />

                                    {(this.state.lifeTimeSubscription) ?
                                        <Image
                                            source={require('../../assets/images/medal.png')}
                                            style={{ position: "absolute", height: 40, width: 40, right:0, zIndex: 10, top: 0 }}
                                        />
                                        :
                                        null}
                                </View>

                                )
                                :
                                (
                                    <View>
                                        <Image
                                            source={this.state.imageLink}
                                            style={[styles.avatorStyle, { borderColor: "rgba(0,0,0,1)", borderWidth: .5, zIndex: 0 }]}
                                        />
                                        {(this.state.lifeTimeSubscription) ?
                                            <Image
                                                source={require('../../assets/images/medal.png')}
                                                style={{ position: "absolute", height: 40, width: 40, right:0, zIndex: 10, top: 0 }}
                                            />
                                            :
                                            null}
                                    </View>
                                )}
                        </View>


                        <Text style={styles.profileName}>{this.state.name}</Text>
                    </View>

                    <View style={[styles.emailContainer, { borderTopColor: "#f5f5f5", borderTopWidth: 1 }]}>
                        <Text style={styles.emailText}>{this.state.email}</Text>
                    </View>
                    <View style={[styles.emailContainer, { marginTop: 0.5, height: 40 }]}>
                        <TextInputMask
                            placeholder="My phone"
                            placeholderTextColor="#ababab"
                            value={this.state.phoneFormat}
                            editable={false}
                            selectTextOnFocus={false}
                            type={'cel-phone'}
                            maxLength={this.state.phoneFormat.startsWith("1") ? 18 : 16}
                            options={
                                this.state.phone.startsWith("1") ?
                                    this.state.phone.length == 11 ?
                                        {
                                            dddMask: '9 (999) 999 - '
                                        } : {
                                            dddMask: '(999) 999 - '
                                        }
                                    : {
                                        dddMask: '(999) 999 - '
                                    }
                            }
                            style={styles.emailText1}

                        />
                    </View>


                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('ChangePasswordScreen', { uid: this.state.uid, email: this.state.email })}>
                        <Text style={styles.passwordText}>Change Password</Text>
                    </TouchableOpacity>
                    {(this.state.subscription_type == "paid") ?
                        (<View style={styles.subscriptionDiv}>

                            <Text style={styles.subscriptionText}>Subscription Plan</Text>


                            <View style={styles.planDiv}>
                                <Text style={{ fontFamily: regularText, fontSize: 15 }}>Current Subscription : </Text>
                                <Text style={{ fontFamily: regularText, fontSize: 15, color: "#52CA66" }}>Active</Text>
                            </View>

                            {(!this.state.lifeTimeSubscription) ?
                                (<View style={styles.planDiv}>
                                    <Text style={{ fontFamily: regularText, fontSize: 15 }}>Next Payment Due :</Text>
                                    <Text style={{ fontFamily: regularText, fontSize: 15 }}>{this.state.subscriptionDate}</Text>
                                </View>)
                                :
                                (
                                    <View style={styles.planDiv}>
                                        <Text style={{ fontFamily: regularText, fontSize: 15 }}>Subscription Plan:</Text>
                                        <Text style={{ fontFamily: regularText, fontSize: 15,color: "#52CA66"  }}>LifeTime</Text>
                                    </View>
                                )}

                            {(!this.state.lifeTimeSubscription) ?
                                (<View style={styles.planDiv}>
                                    <Text style={{ fontFamily: regularText, fontSize: 15 }}></Text>
                                    <TouchableOpacity
                                        onPress={() => this._goToCancelSubscription()}
                                        style={{ justifyContent: "flex-end", borderColor: "#3A4958", borderRadius: 5, borderWidth: 1, padding: 10, justifyContent: "center", alignItems: "center" }}>
                                        <Text style={{ color: "#3A4958", fontFamily: regularText, fontSize: 13 }}>Cancel Subscription</Text>
                                    </TouchableOpacity>
                                </View>)
                                :
                                null}
                        </View>)
                        :
                        (
                            <View style={styles.subscriptionDiv}>
                                <Text style={styles.subscriptionText}>Subscription Plan</Text>
                                <View style={[styles.planDiv]}>
                                    <Text style={{ fontFamily: regularText, fontSize: 15 }}>No active subscription plan</Text>
                                    <TouchableOpacity
                                        onPress={() => this._goToBuySubscription()}
                                        style={{ justifyContent: "flex-end", borderColor: "#3A4958", borderRadius: 5, borderWidth: 1, padding: 10, justifyContent: "center", alignItems: "center" }}>
                                        <Text style={{ color: "#3A4958", fontFamily: regularText, fontSize: 13 }}>Buy Subscription</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        )}

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#3A4958',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    profileContainer: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20
    },

    avatorStyle: {
        width: 140,
        height: 140,
        borderRadius: 70
    },

    profileName: {
        fontFamily: regularText,
        color: '#1e1e1e',
        fontSize: 25,
        marginTop: 20
    },

    emailContainer: {
        paddingLeft: 15,
        marginVertical: 1,
        // backgroundColor: '#fff'
        borderBottomColor: "#f5f5f5",
        //borderTopColor:"grey",
        borderBottomWidth: 1,
        //borderTopWidth:0.5

    },

    emailText: {
        fontFamily: regularText,
        fontSize: 20,
        marginVertical: 5
    },

    emailText1: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#1e1e1e'
    },

    passwordText: {
        fontFamily: regularText,
        color: '#007aff',
        fontSize: 16,
        alignSelf: 'flex-end',
        marginRight: 15,
        marginTop: 15
    },
    subscriptionDiv: {
        width: screenSize.width - 20,
        margin: 10,
        borderRadius: 10,
        elevation: 5,
        backgroundColor: "#fff",
        alignSelf: "center",
        padding: 10,
        marginTop: 20
    },
    subscriptionText: {
        fontFamily: regularText,
        color: '#1e1e1e',
        fontSize: 18,
        width: screenSize.width - 40,
        borderBottomColor: "grey",
        borderBottomWidth: 0.5,
        paddingBottom: 5,
    },
    planDiv: {
        width: screenSize.width - 40,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 10
    }
})


//<Text style={styles.emailText}>{this.state.phone}</Text>