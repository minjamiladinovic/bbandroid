// Create account page 2
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/verify-phone-number.png

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';
import MyAccount from './my-account';
import CommonTasks from '../../common-tasks/common-tasks';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();


export default class BusinessSummaryEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            summary: '',
            summary: "",
            uid: this.props.navigation.state.params.uid,
            businessSummary: this.props.navigation.state.params.businessSummary,
            isLoading: false
        }
    }

    
    _updateSummary() {
        this.setState({ isLoading: true });
        db.collection("users").doc(this.state.uid).update({
            BusinessSummary:this.state.businessSummary
        }).then((user) => {
            MyAccount._changeBusinessSummary(this.state.businessSummary);  
            this.setState({ isLoading: false });
            console.log("user data" + user);
            this.props.navigation.pop();
            CommonTasks._displayToast("Successfully update Business Summary");
        }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error);
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode + "-" + errorMessage);
            CommonTasks._displayToast(errorMessage);
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>

                     <TouchableOpacity style={styles.backContainer}
                        onPress={() =>this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Back</Text>
                    </TouchableOpacity>


                    <Text style={styles.headerText}>Business Summary</Text>

                    <TouchableOpacity onPress={() => this._updateSummary()}>
                        <Text style={styles.nextText}>Save</Text>
                    </TouchableOpacity>

                    {/* {(this.state.summary == "") ?
                        (<Text style={[styles.nextText,{color:'#3A4958'}]} onPress={() => this.gotoNextStep()}>Next</Text>)
                        :
                        (<Text style={[styles.nextText,{color:'#007aff'}]} onPress={() => this.gotoNextStep()}>Next</Text>)} */}
                </View>

                <View style={styles.verificationContainer}>
                    <TextInput
                        style={styles.textInput}
                        keyboardType='default'
                        multiline={true}
                        numberOfLines={5}
                        underlineColorAndroid="transparent"
                        placeholder="Business summary"
                        placeholderTextColor="#ababab"
                        returnKeyType='done'
                        autoCorrect={false}
                        autoCapitalize="none"
                        // onSubmitEditing={() => this.inputLname.focus()}
                        value={this.state.businessSummary}
                        onChangeText={(input) => this.setState({businessSummary: input })}
                        ref={((input) => this.inputCode = input)}
                    />
                </View>

                <Text style={styles.verificationText}>Enter a summary about your business. This will display on your public profile to help customers get to know you.</Text>

                {(this.state.isLoading == true) ?
                    (<View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                           <View style={styles.activity_sub}>
                        <ActivityIndicator
                            size="large"
                            color='#D0D3D4'
                            style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                        />
                    </View> 
                            </View>)
                    : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    textInput: {
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17,
        textAlignVertical: 'top',
        height: 100
    },

    verificationContainer: {
        backgroundColor: 'white',
        marginVertical: 20,
        elevation: 1
    },

    verificationText: {
        fontFamily: regularText,
        fontSize: 15,
        marginHorizontal: 15
    },

    sendCodeText: {
        textAlign: 'center',
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff',
        marginVertical: 10
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
});