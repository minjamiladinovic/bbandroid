
/* Project image view page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/imagei-view.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
} from 'react-native';
const screenSize = Dimensions.get('window');

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
    CachedImage,
} from 'react-native-cached-image';


export default class ImageView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           image_uri: this.props.navigation.state.params.image_uri,

        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#1e1e1e"
                    barStyle="light-content"
                />
                <MaterialCommunityIcons name="close" style={styles.closeBtn} onPress={() => this.props.navigation.goBack()} />
                <View style={{paddingVertical:1}}>
                <CachedImage
                     source={{ uri: this.state.image_uri}}
                    style={styles.propertyImage}
                />    
                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1e1e1e',
        justifyContent: 'center'

    },

    closeBtn: {
        color: '#fff',
        fontSize: 32,
        position: 'absolute',
        top: 10,
        marginRight: 5
    },

    propertyImage: {
        width: screenSize.width,
        height: screenSize.height-100,
        resizeMode: 'contain'
    },
})