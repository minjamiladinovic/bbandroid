
/* Project search page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/project-search.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    RefreshControl,
    FlatList,
    BackHandler,
    Alert
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import firebase from 'react-native-firebase';
import CommonTasks from '../../common-tasks/common-tasks';
import Footer from '../footer/footer';
import {
    CachedImage,
} from 'react-native-cached-image';
const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();
import VerifiedLabel from '../IndependentComponent/verifiedLabel'


export default class ProjectSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //uid: this.props.navigation.state.params.uid,
            uid: '',
            data: [],
            data1: [],
            isLoading: false,
            id: "192",
            serviceData: CommonTasks.serviceList,
            dataLength: 0,
            refreshing: false,
            condensedMode: '0',
            SubcriptionType: "",
            SubscriptionEndDate: "",
            userService: [],
            userZipCodes: [],
            showNoProj: false,
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('condensed_mode').then((value) => value ? this.setState({ condensedMode: value == '1' ? '1' : '0' }) : null);
        AsyncStorage.getItem('user_id').then(value => {
            this.setState({ uid: value })
            this._getUserDetails(value);
        }
        );

        //    AsyncStorage.getItem('SubcriptionType').then(value => this.setState({SubcriptionType: value }));
        //    AsyncStorage.getItem('SubscriptionEndDate').then(value => this.setState({SubscriptionEndDate: value }));

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    }

    componentWillMount() {
        //AsyncStorage.getItem('condensed_mode').then((value) => value? this.setState({ condensedMode: value=='1'?'1':'0' }):null );
        //AsyncStorage.getItem('user_id').then(value => this.setState({ uid: value }));
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    _getUserDetails(uid) {
        debugger;
        this.setState({ isLoading: true });
        var docRef = db.collection("users").doc(uid);
        docRef.get().then((doc) => {
            if (doc.exists) {
                AsyncStorage.setItem("SubcriptionType", doc.data().SubcriptionType);
                this.setState({
                    userService: doc.data().ServiceType,
                    userZipCodes: doc.data().BusinessInfo.SelectedZip//this needs to be managed
                }, this._onget.bind(this))
            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
            }
            // this.setState({ isLoading: false });
        }).catch((eror) => {
            this.setState({ isLoading: false });
            console.log("Error getting document:", error);
        });
    }

    handleBackButton = () => {
        Alert.alert(
            'Pro in your pocket for Pros',
            'Do you want to exit?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            },], {
            cancelable: false
        }
        )

        return true;
    }

    componentWillMount() {
        // this._onget();
    }

    goto_projectCondensed() {
        this.props.navigation.navigate('ProjectSearchCondensedScreen');
    }


    goto_placebid(item) {
        //alert(id);
        console.log("place bid : " + JSON.stringify(item));

        this.props.navigation.navigate('ProjectPlaceBidScreen',
            {
                projectId: item.projectId,
                ownerId: item.ownerId,
                description: item.description,
                serviceId: item.serviceId,
                createdAt: item.createdAt,
                bidderIdList: item.bidderIdList,
                imageLink: item.imageLink,
                ownerName: item.ownerName,
                postalCode: item.postalCode,
                timeString: item.timeString,
                Pimage: item.Pimage,
                bidderId: this.state.uid,
                projectVerified: item.projectVerified,
                email_verify_status:item.email_verify_status
            });
    }


    gotoFilterPage() {
        //this.props.navigation.navigate('FilterSearchScreen');
    }

    /*Get and modify projects array */

    _onget = () => {
        debugger;
        //alert("onget");
        this.setState({ isLoading: true })
        db.collection("projects").get().then((data) => {
            //console.log("data"+data.docs);
            var array = [];
            var mod_array = [];

            data.docs.forEach(doc => {

                if (doc.data().projectStatus == 'Completed') {
                    console.log('\n')
                }
                else if (doc.data().projectStatus == 'Hired' || doc.data().projectStatus == 'Deleted') {
                    console.log(' ')
                }
                else {

                    var obj = {}
                    if (doc.data().serviceId != undefined) {
                        obj.serviceId = doc.data().serviceId;
                    }
                    else {
                        obj.serviceId = ''
                    }

                    var checkZip = false;


                    if (this.state.userZipCodes.length > 0 && doc.data().postalCode != undefined) {
                        this.state.userZipCodes.forEach(function (element, i, object) {
                            if (element.toString().trim() === doc.data().postalCode.toString().trim()) {
                                checkZip = true
                                console.log(checkZip);
                            }

                        })
                    }
                    obj.ZipExists = checkZip;
                    console.log("obj.ZipExists" + obj.ZipExists + "check" + checkZip);
                    obj.projectId = doc.id;
                    obj.createdAt = doc.data().createdAt;
                    obj.ownerId = doc.data().owner;

                    obj.description = doc.data().description;
                    obj.postalCode = doc.data().postalCode;

                    obj.Pimage = doc.data().Pimage;
                    obj.bidderIdList = doc.data().bidderIdList;
                    obj.projectVerified = doc.data().projectVerified == 'N' ? 'N' : 'Y';

                    if (obj.Pimage) {
                        if (obj.Pimage.length == 0) {
                            obj.Pimage = null;
                        }
                    }
                    else {
                        obj.Pimage = null;
                    }
                    if (obj.bidderIdList) {
                        if (obj.bidderIdList.length == 0) {
                            obj.bidderIdList = null;
                        }
                    }
                    else {
                        obj.bidderIdList = null;
                    }
                    var dateValue = new Date(obj.createdAt)
                    var timeNumber = dateValue.valueOf()

                    var hours = dateValue.getHours();
                    var minutes = dateValue.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0' + minutes : minutes;
                    hours = hours < 10 ? '0' + hours : hours;
                    var strTime = hours + ':' + minutes + ' ' + ampm;

                    obj.timeValue = timeNumber;
                    obj.timeString = strTime;

                    if (this.state.userZipCodes.length > 0) {
                        var checkService = false;
                        if (obj.ZipExists == true) {
                            this.state.userService.forEach(function (element, i, object) {
                                console.log("user service id" + element.id);
                                console.log("zip exists:" + obj.ZipExists);
                                if (element.id.trim() === obj.serviceId.trim()) {
                                    checkService = true;
                                }

                            })

                            if (checkService == true) {
                                array.push(obj);
                            }

                        }
                    }
                    else if (this.state.userZipCodes.length == 0) {
                        var checkService = false;
                        this.state.userService.forEach(function (element, i, object) {
                            console.log("project service id" + obj.serviceId);
                            console.log("user service id" + element.id);

                            if (element.id.trim() == obj.serviceId.trim()) {
                                checkService = true
                            }
                        })

                        if (checkService == true) {
                            array.push(obj);
                        }
                    }
                }
            })
            console.log('array length : ' + array.length);
            if (array.length == 0) {
                this.setState({
                    isLoading: false,
                    showNoProj: true,
                })
            }
            else {
                this.setState({
                    dataLength: array.length,
                    data1: array,
                }, this._getOwnerName.bind(this))
            }


        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log(err)
        })
    }

    _checkPostal(ZipCode) {
        this.state.userZipCodes.forEach(function (element, i, object) {
            if (element.toString() == ZipCode) {
                return true;
            }
            else {
                return false;
            }
        })
    }

    _ProjetServiceName(id) {
        console.log("id" + id);
        var serviceData = CommonTasks.serviceList;
        var service_name = '';
        for (var j = 0; j < serviceData.length; j++) {
            console.log("select" + serviceData[j].name);
            if (id == serviceData[j].id) {
                service_name = serviceData[j].name;
            }
        }
        console.log("service name" + service_name);
        return (service_name);
    }


    _getUserDetailsRefresh() {
        debugger;
        var docRef = db.collection("users").doc(this.state.uid);
        docRef.get().then((doc) => {
            if (doc.exists) {
                AsyncStorage.setItem("SubcriptionType", doc.data().SubcriptionType);
                this.setState({
                    userService: doc.data().ServiceType,
                    userZipCodes: doc.data().BusinessInfo.SelectedZip
                }, this._onget.bind(this))
            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
            }
        }).catch((eror) => {
            console.log("Error getting document:", error);
        });
    }

    _onget1 = () => {
        this.setState({ dataLength: 0 })
        db.collection("projects").get().then((data) => {
            //console.log("data"+data.docs);
            var array = [];
            var mod_array = [];

            data.docs.forEach(doc => {
                //console.log('Pimage : '+doc.data().Pimage)
                if (doc.data().projectStatus == 'Completed') {
                    console.log('\n')
                }
                else if (doc.data().projectStatus == 'Hired' || doc.data().projectStatus == 'Deleted') {
                    console.log(' ')
                }
                else {
                    console.log('Bidders : ' + doc.data().bidderIdList)

                    var obj = {}
                    obj.serviceId = doc.data().serviceId;
                    var checkZip = false;


                    if (this.state.userZipCodes.length > 0) {
                        this.state.userZipCodes.forEach(function (element, i, object) {
                            if (element.toString().trim() === doc.data().postalCode.trim()) {
                                checkZip = true
                                console.log(checkZip);
                            }

                        })
                    }
                    obj.ZipExists = checkZip;
                    obj.projectId = doc.id;
                    obj.createdAt = doc.data().createdAt;
                    obj.ownerId = doc.data().owner;
                    if (doc.data().serviceId != undefined) {
                        obj.serviceId = doc.data().serviceId;
                    }
                    else {
                        obj.serviceId = ''
                    }

                    obj.description = doc.data().description;
                    obj.postalCode = doc.data().postalCode;
                    obj.projectVerified = doc.data().projectVerified == 'N' ? 'N' : 'Y';
                    obj.Pimage = doc.data().Pimage;
                    obj.bidderIdList = doc.data().bidderIdList;

                    if (obj.Pimage) {
                        if (obj.Pimage.length == 0) {
                            obj.Pimage = null;
                        }
                    }
                    else {
                        obj.Pimage = null;
                    }


                    if (obj.bidderIdList) {
                        if (obj.bidderIdList.length == 0) {
                            obj.bidderIdList = null;
                        }
                    }
                    else {
                        obj.bidderIdList = null;
                    }

                   


                    var dateValue = new Date(obj.createdAt)
                    var timeNumber = dateValue.valueOf()

                    var hours = dateValue.getHours();
                    var minutes = dateValue.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0' + minutes : minutes;
                    hours = hours < 10 ? '0' + hours : hours;
                    var strTime = hours + ':' + minutes + ' ' + ampm;

                    obj.timeValue = timeNumber;
                    obj.timeString = strTime;


                    if (this.state.userZipCodes.length > 0) {
                        var checkService = false;
                        if (obj.ZipExists == true) {
                            this.state.userService.forEach(function (element, i, object) {
                                console.log("user service id" + element.id);
                                console.log(obj.ZipExists);

                                if (obj.serviceId != undefined) {
                                    if (element.id.trim() === obj.serviceId.trim()) {
                                        checkService = true;
                                    }
                                }


                            })
                            if (checkService == true) {
                                array.push(obj);
                            }

                        }
                    }
                    else if (this.state.userZipCodes.length == 0) {
                        var checkService = false;
                        this.state.userService.forEach(function (element, i, object) {
                            //console.log("project service id" + obj.serviceId);
                            console.log("user service id" + element.id);

                            if (obj.serviceId != undefined) {
                                if (element.id.trim() == obj.serviceId.trim()) {
                                    checkService = true
                                }
                            }

                        })

                        if (checkService == true) {
                            array.push(obj);
                        }
                    }
                }

            })
            console.log('array length : ' + array.length)

            if (array.length == 0) {
                this.setState({
                    isLoading: false,
                    refreshing: false,
                    showNoProj: true
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    refreshing: false,
                    dataLength: array.length,
                    data1: array,
                }, this._getOwnerName.bind(this))
            }

            alert('hi')
        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log(err)
        })
    }


    _getOwnerName() {
        console.log('_getOwnerName');
        var dbData = [];
        var pData = this.state.data1;

        pData.forEach(data => {
            var name
            db.collection("users").doc(data.ownerId).get()
                .then(doc => {
                    if (doc.exists) {
                        var projectData = data// project data


                        projectData.uid = doc.data().uid;
                        if (doc.data().firstname != null) {
                            projectData.firstname = doc.data().firstname;
                        }
                        else {
                            projectData.firstname = '';
                        }

                        if (doc.data().lastname != null) {
                            projectData.lastname = doc.data().lastname;
                        }
                        else {
                            projectData.lastname = ''
                        }


                        projectData.email = doc.data().email;
                        projectData.imageLink = doc.data().imageLink;
                        projectData.email_verify_status = doc.data().email_verify_status == 'N' ? 'N' : 'Y';

                        if (projectData.imageLink) {
                            if (projectData.imageLink == '') {
                                projectData.imageLink = null;
                            }
                        }
                        else {
                            projectData.imageLink = null;
                        }

                        //lastname
                        if (projectData.lastname) {
                            if (projectData.lastname == '') {
                                projectData.lastname = null;
                            }
                        }
                        else {
                            projectData.lastname = null;
                        }

                        //nameformating
                        var nameflag = projectData.firstname.indexOf(' ');
                        var nameflag1 = projectData.firstname.indexOf('  ');
                        if (nameflag != -1) {
                            var nameArray = projectData.firstname.split(' ')
                            var name = nameArray[0] + ' ' + nameArray[1].charAt(0).toUpperCase();
                            projectData.ownerName = name;
                        }
                        else if (nameflag1 != -1) {
                            var nameArray = projectData.firstname.split('  ')
                            var name = nameArray[0] + ' ' + nameArray[1].charAt(0).toUpperCase();
                            projectData.ownerName = name;
                        }
                        else {

                            if (projectData.lastname != null) {
                                var name = projectData.firstname + ' ' + projectData.lastname.charAt(0).toUpperCase();
                                projectData.ownerName = name;
                            }
                            else {
                                projectData.ownerName = projectData.firstname;
                            }
                        }


                        // console.log(projectData);
                        // console.log('\n\n\n');
                        dbData.push(projectData);
                        //console.log('hi count '+dbData.length);
                        if (dbData.length == this.state.dataLength) {
                            dbData = dbData.sort((a, b) => (b.timeValue > a.timeValue) ? 1 : ((a.timeValue > b.timeValue) ? -1 : 0));

                            this.setState({
                                data: dbData,

                            }, this._stopLoading.bind(this));
                        }
                    }
                    else {
                        var projectData = data
                        name = 'Customer';
                        //console.log(name);
                        projectData.ownerName = name;
                        projectData.imageLink = null;
                        dbData.push(projectData);

                        if (dbData.length == this.state.dataLength) {
                            dbData = dbData.sort((a, b) => (b.timeValue > a.timeValue) ? 1 : ((a.timeValue > b.timeValue) ? -1 : 0));
                            this.setState({
                                data: dbData,

                            }, this._stopLoading.bind(this));
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                    var projectData = data
                    name = 'Customer';
                    projectData.ownerName = name;
                    projectData.imageLink = null;

                    dbData.push(projectData);
                    if (dbData.length == this.state.dataLength) {

                        dbData = dbData.sort((a, b) => (b.timeValue > a.timeValue) ? 1 : ((a.timeValue > b.timeValue) ? -1 : 0));
                        this.setState({
                            data: dbData,

                        }, this._stopLoading.bind(this));
                    }
                });



        })




    }



    _stopLoading() {
        //console.log("get ids" + JSON.stringify(this.state.data_id));
        //alert();
        this.setState({ isLoading: false, refreshing: false });
    }

    _getImage(id) {

        //var serviceData = CommonTasks.serviceList;
        if (id == "") {
            return (
                <Image
                    source={require('../../assets/images/house.png')}
                    style={styles.propertyImage}
                />
            )
        }

        else {
            for (var i = 0; i < this.state.serviceData.length; i++) {
                if (this.state.serviceData[i].id == id) {
                    return (
                        <CachedImage
                            source={(this.state.serviceData[i].service_url == '') ? require('../../assets/images/house.png') : { uri: this.state.serviceData[i].service_url }}
                            style={styles.propertyImage}
                        />
                    )
                }
            }
        }

    }

    _getServiceName(id) {
        for (var i = 0; i < this.state.serviceData.length; i++) {
            if (this.state.serviceData[i].id == id) {
                return (
                    <Text style={styles.titleText}>{this.state.serviceData[i].name}</Text>
                )
            }
        }

    }

    _showDate(date, timestr) {
        var dateData = date.split(" ")[0];

        var dateObject = new Date(Date.parse(dateData));

        var dateReadable = dateObject.toDateString().trim();

        var time = date.split(" ")[1];

        return (
            <View>
                <Text style={styles.timeText}>{dateReadable}   {timestr}</Text>
            </View>
        )
    }

    /*Check whether go to place bid or edit bid page*/



    _getBidDetails(item) {
        console.log(JSON.stringify(item))
        this.setState({ isLoading: true });
        //alert();
        var docRef = db.collection("place_bid").doc(item.projectId + "_" + this.state.uid);
        docRef.get().then((doc) => {

            //alert();
            if (doc.exists) {
                console.log("Document data:", doc.data());
                //alert(doc.data());
                console.log(JSON.stringify(doc.data()))
                this.props.navigation.navigate('ProjectBidPlacedScreen', {

                    bidId: item.projectId + "_" + doc.data().bidderId,
                    bidPrice: doc.data().bidPrice,
                    bidderId: this.state.uid,
                    bidderIdList: item.bidderIdList,
                    projectId: item.projectId,
                    ownerId: doc.data().ownerId,
                    ownerName: item.ownerName,
                    imageLink: item.imageLink ? item.imageLink : null,
                    description: item.description,
                    serviceId: item.serviceId,
                    createdAt: item.createdAt,
                    postalCode: item.postalCode,
                    timeString: item.timeString,
                    Pimage: item.Pimage ? item.Pimage : null,
                    projectStatus: doc.data().projectStatus,
                    projectVerified: item.projectVerified,
                    email_verify_status: item.email_verify_status,
                    navigateFrom: 'projectSearch',
                });
                this.setState({ isLoading: false });
            }
        }).catch((eror) => {
            this.setState({ isLoading: false });
            alert(eror);
            //console.log("Error getting document:", error);
        });
    }


    _onRefresh = () => {
        this.setState({ refreshing: true });
        this._getUserDetailsRefresh();
    }


    _gotoCustomerProfile(item) {
        //console.log(item.imageLink)
        this.props.navigation.navigate('CustomerProfileScreen', { profileId: item.uid, profileName: item.ownerName, profileImage: item.imageLink })
    }


    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#3A4958"
                    barStyle="light-content"
                />
                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("AccountSettingsScreen")}>
                        <MaterialCommunityIcons name="settings" style={styles.headerIcon} />
                    </TouchableOpacity>

                    <Image
                        source={require('../../assets/images/logo.png')}
                        style={{ width: 150, height: 50, alignSelf: 'center', resizeMode: 'contain' }}
                    />
                    <TouchableOpacity style={{ width: 50 }}>
                    </TouchableOpacity>

                </View>


                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }>

                    {(this.state.data.length == 0 && this.state.isLoading == false) ?
                        (<View style={{ width: screenSize.width, height: screenSize.height, backgroundColor: "#EFEFF1" }}>
                            <Image style={{ width: 200, height: 200, resizeMode: 'stretch', alignSelf: "center" }} source={require('../../assets/images/error.png')} />
                            <Text style={{ color: "#3A4958", fontSize: 50, fontFamily: regularText, alignSelf: "center", fontWeight: "300" }}>Oops!</Text>
                            <Text style={{ color: "black", fontSize: 18, fontFamily: regularText, alignSelf: "center", marginTop: 20 }}>No Projects Found</Text>
                            <Text style={{ color: "black", fontSize: 18, fontFamily: regularText, alignSelf: "center" }}>In Your Chosen Catagories</Text>
                            <Text style={{ fontSize: 15, fontFamily: regularText, alignSelf: "center", marginTop: 20 }}>Check back soon!!</Text>
                        </View>)
                        :
                        (<FlatList
                            data={this.state.data}
                            renderItem={({ item }) =>
                                (
                                    <View style={styles.itemContainer}>

                                        {(item.bidderIdList == null) ?
                                            (<View style={styles.imageHeadingContainer}>

                                                <TouchableOpacity onPress={() => this.goto_placebid(item)}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <View>
                                                            <TouchableOpacity onPress={() => this._gotoCustomerProfile(item)}>
                                                                <CachedImage
                                                                    source={item.imageLink ?
                                                                        { uri: item.imageLink }
                                                                        :
                                                                        require('../../assets/images/userOwner.png')}
                                                                    style={styles.avatorImage}
                                                                />
                                                            </TouchableOpacity>

                                                        </View>
                                                        <View style={{ marginLeft: 10 }}>
                                                            <Text style={styles.titleText}><Text onPress={() => this._gotoCustomerProfile(item)}>{item.ownerName ? item.ownerName : 'Customer'}</Text>&nbsp;
                                    <Text style={{ fontWeight: '100' }}>requested</Text>&nbsp;
                                            {this._getServiceName(item.serviceId)} <Text style={{ color: 'grey' }}>{item.postalCode ? 'in ' + item.postalCode : null}</Text>
                                                            </Text>
                                                            {this._showDate(item.createdAt, item.timeString)}
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>



                                                <Text style={styles.descriptionText}>{item.description}</Text>
                                                {(this.state.condensedMode == '0') ?
                                                    <TouchableOpacity onPress={() => this.goto_placebid(item)}>
                                                        {(item.Pimage != null) ? <CachedImage source={{ uri: item.Pimage[0] }} style={styles.propertyImage} /> : this._getImage(item.serviceId)}
                                                    </TouchableOpacity>
                                                    :
                                                    null

                                                }
                                                <VerifiedLabel projectVerified={item.projectVerified} type={'Project'} />
                                                <VerifiedLabel projectVerified={item.email_verify_status} type={'User'} />



                                            </View>)
                                            :
                                            (item.bidderIdList.includes(this.state.uid)) ?
                                                (<View style={styles.imageHeadingContainer}>
                                                    <TouchableOpacity onPress={() => this._getBidDetails(item)}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <View>
                                                                <TouchableOpacity onPress={() => this._gotoCustomerProfile(item)}>
                                                                    <CachedImage
                                                                        source={item.imageLink ?
                                                                            { uri: item.imageLink }
                                                                            :
                                                                            require('../../assets/images/userOwner.png')}
                                                                        style={styles.avatorImage}
                                                                    />
                                                                </TouchableOpacity>

                                                            </View>
                                                            <View style={{ marginLeft: 10 }}>
                                                                <Text style={styles.titleText}><Text onPress={() => this._gotoCustomerProfile(item)}>{item.ownerName ? item.ownerName : 'Customer'}</Text>&nbsp;
                                    <Text style={{ fontWeight: '100' }}>requested</Text>&nbsp;
                                            {this._getServiceName(item.serviceId)} <Text style={{ color: 'grey' }}>{item.postalCode ? 'in ' + item.postalCode : null}</Text>
                                                                </Text>
                                                                {this._showDate(item.createdAt, item.timeString)}
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>


                                                    <Text style={styles.descriptionText}>{item.description}</Text>

                                                    <TouchableOpacity onPress={() => this._getBidDetails(item)}>
                                                        {(this.state.condensedMode == '0') ?
                                                            (item.Pimage != null) ? <CachedImage source={{ uri: item.Pimage[0] }} style={styles.propertyImage} /> : this._getImage(item.serviceId)
                                                            :
                                                            null
                                                        }
                                                        <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>

                                                            {(item.bidderIdList.length == 0 || item.bidderIdList.length == 1) ?
                                                                (
                                                                    <Text style={styles.bidText} >{item.bidderIdList.length} bid</Text>
                                                                )
                                                                :
                                                                (
                                                                    <Text style={styles.bidText} >{item.bidderIdList.length} bids</Text>
                                                                )}


                                                            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                                                                <Feather name="check" style={styles.bidIcon} />
                                                                <Text style={{ marginLeft: 10, color: "#4cd964", fontSize: 15, fontFamily: regularText }}>Bid Placed</Text>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <VerifiedLabel projectVerified={item.projectVerified} type={'Project'} />
                                                    <VerifiedLabel projectVerified={item.email_verify_status} type={'User'} />



                                                </View>)
                                                :
                                                (<View style={styles.imageHeadingContainer}>

                                                    <TouchableOpacity onPress={() => this.goto_placebid(item)}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <View>
                                                                <TouchableOpacity onPress={() => this._gotoCustomerProfile(item)}>
                                                                    <CachedImage
                                                                        source={item.imageLink ?
                                                                            { uri: item.imageLink }
                                                                            :
                                                                            require('../../assets/images/userOwner.png')}
                                                                        style={styles.avatorImage}
                                                                    />
                                                                </TouchableOpacity>

                                                            </View>
                                                            <View style={{ marginLeft: 10 }}>
                                                                <Text style={styles.titleText}><Text onPress={() => this._gotoCustomerProfile(item)}>{item.ownerName ? item.ownerName : 'Customer'}</Text>&nbsp;
                                    <Text style={{ fontWeight: '100' }}>requested</Text>&nbsp;
                                            {this._getServiceName(item.serviceId)} <Text style={{ color: 'grey' }}>{item.postalCode ? 'in ' + item.postalCode : null}</Text>
                                                                </Text>
                                                                {this._showDate(item.createdAt, item.timeString)}
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>



                                                    <Text style={styles.descriptionText}>{item.description}</Text>

                                                    <TouchableOpacity onPress={() => this.goto_placebid(item)}>
                                                        {(this.state.condensedMode == '0') ?
                                                            (item.Pimage != null) ? <CachedImage source={{ uri: item.Pimage[0] }} style={styles.propertyImage} /> : this._getImage(item.serviceId)
                                                            :
                                                            null
                                                        }

                                                        {(item.bidderIdList.length == 0 || item.bidderIdList.length == 1) ?
                                                            (
                                                                <Text style={styles.bidText} >{item.bidderIdList.length} bid</Text>
                                                            )
                                                            :
                                                            (
                                                                <Text style={styles.bidText} >{item.bidderIdList.length} bids</Text>
                                                            )}
                                                        {/* <Text style={styles.bidText} >{item.bidderIdList.length} bids</Text>  */}
                                                    </TouchableOpacity>

                                                    <VerifiedLabel projectVerified={item.projectVerified} type={'Project'} />
                                                    <VerifiedLabel projectVerified={item.email_verify_status} type={'User'} />
                                                </View>)
                                        }
                                    </View>
                                )}
                            keyExtractor={item => item.createdAt}
                        />)}
                </ScrollView>


                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>)
                    : null}

                <Footer itemColor='globe'></Footer>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    headerContainer: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#3A4958',
        paddingTop: Platform.OS === 'ios' ? 20 : 5,
        paddingHorizontal: 5
    },

    headerIcon: {
        color: '#fff',
        fontSize: 32
    },

    bidIcon: {
        marginTop: 10,
        fontSize: 25,
        color: "#4cd964"
    },

    itemContainer: {
        backgroundColor: 'white',
        marginTop: 10,
        padding: 10
    },

    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    titleText: {
        fontFamily: regularText,
        fontWeight: 'bold',
        color: '#1e1e1e',
        fontSize: 16,
        maxWidth: screenSize.width - 95
    },

    timeText: {
        fontFamily: regularText,
        fontSize: 16,
        marginTop: 10
    },

    descriptionText: {
        fontFamily: regularText,
        fontSize: 16,
        marginVertical: 10,
        color: '#1e1e1e'
    },

    propertyImage: {
        width: screenSize.width - 20,
        height: 200,
        resizeMode: 'cover'
    },

    bidText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        marginTop: 12,
        fontSize: 16
    },


    reviewContainer: {
        backgroundColor: '#3A4958',
        alignSelf: 'flex-end',
        borderRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
        height: 35,
        width: 120
    },

    reviewText: {
        color: '#fff',
        fontFamily: regularText,
        fontSize: 17,
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,

        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
})