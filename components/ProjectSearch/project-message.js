
/* Project bid placed page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/project-messages-empty.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    FlatList,
    Keyboard,
    BackHandler
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Footer from '../footer/footer';
import Entypo from 'react-native-vector-icons/Entypo';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';
// import { DotsLoader, RotationHoleLoader, CirclesLoader, BubblesLoader } from 'react-native-indicator';
import { CachedImage } from 'react-native-cached-image';


const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const italicText = "SF-Pro-Display-LightItalic";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class ProjectMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_archive: false,
            ProjectOwnerName: this.props.navigation.state.params.ProjectOwnerName,
            uid: this.props.navigation.state.params.Uid,
            toChatId: this.props.navigation.state.params.toChatId,
            fromChatId: this.props.navigation.state.params.fromChatId,
            navigateFrom: this.props.navigation.state.params.navigateFrom,
            message_status: this.props.navigation.state.params.message_status,
            imageLink: this.props.navigation.state.params.imageLink,
            project_id: this.props.navigation.state.params.project_id,//new change

            serviceData: CommonTasks.serviceList,
            show_archive: false,
            message: "",
            isLoading: false,
            isLoading1: true,
            chatArray: [],
            isLoadingMore: false,
            isarchive: false,
            reciver_message_status: "",
            chatArrayMod: [],
            lastPage: 5,
            totalNoFetch: 1,
            fetchCounter: 5,
            pageCounter: 1,
            isLoadingMore: false,

            navigateImageLink:"",
            navigateUserName:"",
            navigateUserBusinessName:""
            //pagination:true
        }
    }

    componentDidMount() {
        console.log("Project owner name" + this.state.ProjectCreateDate);
        console.log("uid" + this.state.uid);
        console.log("to chat id" + this.state.toChatId);
        console.log("from chat id" + this.state.fromChatId);
        AsyncStorage.multiGet([
          'userName',
          'imageLink',
          'userBusinessName',
          ]).then(values => {
            this.setState({
              navigateUserName : values[0][1],
              navigateImageLink : values[1][1],
              navigateUserBusinessName : values[2][1],
            },this._getMessages.bind(this));
          });

        //this._getMessages();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    

    componentWillUnmount() {
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        }
    
        handleBackButton = () => {
            this.props.navigation.pop();
             return true;
           }

    _showArchive() {
        if (this.state.show_archive == false) {
            this.setState({
                show_archive: true
            })
        }
        else {
            this.setState({
                show_archive: false
            })
        }

    }
    _getMessages() {
    var docRef = db.collection("messages").doc(this.state.uid).collection(this.state.message_status).doc(this.state.toChatId + "_" + this.state.project_id);
    if (this.state.navigateFrom.trim() == "project_details") {
      var docRef2 = db.collection("messages").doc(this.state.uid).collection("archive").doc(this.state.toChatId + "_" + this.state.project_id);
      docRef2.get().then((doc) => {
        if (doc.exists) {
          var array = [];
          array = doc.data().chat;
          console.log("array chat" + array);
          // if (array.length > 5) {
          //   this.setState({
          //     totalNoFetch: (array.length / 5),
          //     chatArrayMod: array.slice(0, 5),
          //     fetchCounter: 10
          //   })
          // }
          // else {
          //   this.setState({
          //     chatArrayMod: array
          //   })
          // }

          this.setState({
            chatArray: array,
            chatArrayMod: array,
            message_status: "archive"
          });

          console.log("chat array mod" + this.state.chatArrayMod);
        }
        else {
          docRef.get().then((doc) => {
            if (doc.exists) {
              var array = doc.data().chat;

              // if (array.length > 5) {
              //   this.setState({
              //     totalNoFetch: (array.length / 5),
              //     chatArrayMod: array.slice(0, 5),
              //     fetchCounter: 10
              //   })
              // }
              // else {
              //   this.setState({
              //     chatArrayMod: array
              //   })
              // }
              this.setState({
                chatArray: array,
                chatArrayMod: array
              });
              console.log("chat array mod" + this.state.chatArrayMod);
              console.log("chat array" + array);
            } else {
              // doc.data() will be undefined in this case
              console.log("No such document!");
            }
            this.setState({ isLoading1: false });
          }).catch((eror) => {
            this.setState({ isLoading1: false });
            console.log("Error getting document:", error);
          });
        }
        this.setState({ isLoading1: false });
      }).catch((eror) => {
        this.setState({ isLoading1: false });
        console.log("Error getting document:", error);
      });
    }

    else {
      docRef.get().then((doc) => {
        if (doc.exists) {
          var array = [];
           array = doc.data().chat;
          // if (array.length > 5) {
          //   this.setState({
          //     totalNoFetch: (array.length / 5),
          //     chatArrayMod: array.slice(0, 5),
          //     fetchCounter: 10
          //   })
          // }
          // else {
          //   this.setState({
          //     chatArrayMod: array
          //   })
          // }
          this.setState({
            chatArray: array,
            chatArrayMod: array
          });
          console.log("chat array mod" + this.state.chatArrayMod);
          console.log("chat array" + array);
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
        this.setState({ isLoading1: false });
      }).catch((eror) => {
        this.setState({ isLoading1: false });
        console.log("Error getting document:", error);
      });
    }

    this._checkReciverStatus();

  }


  _checkReciverStatus() {
    this.setState({ isLoading1: true });
    var docRef = db.collection("messages").doc(this.state.toChatId).collection("archive").doc(this.state.uid + "_" + this.state.project_id);
    //new change
    docRef.get().then((doc) => {
      if (doc.exists) {
        this.setState({
          reciver_message_status: "archive"
        });
      }
      else {
        this.setState({
          reciver_message_status: "inbox"
        });
      }
      this.setState({ isLoading1: false });
    }).catch((eror) => {
      this.setState({ isLoading1: false });
      console.log("Error getting document:", error);
    });
  }


  _onEndReached = () => {
    if (this.state.chatArray.length >= 5) {
      this.setState({ isLoadingMore: true });
      this.fetchMore();
    }

  }

  fetchMore() {
    var page;
    if (this.state.pageCounter > this.state.totalNoFetch) {
      this.setState({
        isLoadingMore: false,
      })
    }
    else {
      page = this.state.fetchCounter;
      this.setState({
        chatArrayMod: this.state.chatArray.slice(0, page),
        fetchCounter: page + 5,
        pageCounter: this.state.pageCounter + 1
      })
    }
  }


  _getMessage_afterSend() {
    var docRef = db.collection("messages").doc(this.state.uid).collection(this.state.message_status).doc(this.state.toChatId + "_" + this.state.project_id);
    docRef.get().then((doc) => {
      if (doc.exists) {
        var array_mod = [];
        array_mod = doc.data().chat;
        console.log("length check" + array_mod.length);
        this.setState({
          chatArrayMod:array_mod,
          chatArray: array_mod,
          chatArraySend:array_mod,
          showChat:true
        }, this.showAlert.bind(this));
      }
      else {
        console.log("No such document!");
      }
    }).catch((eror) => {
      this.setState({isLoading: false });
      console.log("Error getting document:", error);
    });
  }

  showAlert() {
    this.setState({ isLoading: false })
  }



  _sendMessage() {

    this.setState({ isLoading: true });
    var message_id = this.state.fromChatId + "-" + this.state.toChatId;//customer-serviceprovider
    var obj = {};
    obj = {
      "message_id": message_id,
      "sender_id": this.state.fromChatId,
      "reciver_id": this.state.toChatId,
      "project_id": this.state.project_id,//new change
      "description": this.state.message,
      "status": "inbox",
      "createdAt": new Date()
    };

    var navigationData = {
      "toChatId": this.state.uid,
      "fromChatId": this.state.toChatId,
      "bidderName":this.state.navigateUserName,
      "project_id": this.state.project_id,
      "bidderBusinessName":this.state.navigateUserBusinessName,
      "navigateFrom": "notification_message",
      "bidderImage":this.state.navigateImageLink,
      "message_status": this.state.reciver_message_status,
    };



    var array = [];
    array = this.state.chatArray;
    array.unshift(obj);

    db.collection("messages").doc(this.state.uid)
      .collection(this.state.message_status)
      .doc(this.state.toChatId + "_" + this.state.project_id)//new change
      .set({
        chat: array
      }).then((user) => {
        db.collection("messages").doc(this.state.toChatId)
          .collection(this.state.reciver_message_status)
          .doc(this.state.fromChatId + "_" + this.state.project_id)//new change
          .set({
            chat: array
          }).then((user) => {

            var msjFunction = db.collection("messages_function").doc(this.state.fromChatId + "_" + this.state.toChatId + "_" + this.state.project_id);

             msjFunction.delete().then((data) => {
              msjFunction
                .set({
                  reciver_id: this.state.toChatId,
                  sender_id: this.state.fromChatId,
                  reciver_status: "Customer",
                  message: this.state.message,
                  reciver_msj_status: this.state.reciver_message_status,
                  navigationData: navigationData,
                  create_message_time: new Date()
                }).then((user) => {
                  console.log("success");
                  this.inputMessage.setNativeProps({ text: "" });
                  this.setState({
                    message: "",
                  })

                  Keyboard.dismiss();
                  this._getMessage_afterSend();
                  //this.setState({ isLoading: false });
                }).catch((error) => {
                  this.setState({ isLoading: false });
                  console.log("user error" + error)
                })


            }).catch((error) => {
              this.setState({ isLoading: false });
              console.log("user error" + error)
            });

            // console.log("success");
            // this.setState({ isLoading: false });
          }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error)
          })


      }).catch((error) => {
        this.setState({ isLoading: false });
        console.log("user error" + error)
      })
  }



  _archiveChat() {
    this.setState({
      show_archive: false,
      isLoading1: true
    });


    db.collection("messages").doc(this.state.uid)//new change
      .collection("inbox").doc(this.state.toChatId + "_" + this.state.project_id).delete().then((data) =>
        db.collection("messages").doc(this.state.uid)
          .collection("archive")
          .doc(this.state.toChatId + "_" + this.state.project_id)//new change
          .set({
            chat: this.state.chatArray
          }).then((user) => {
            this.setState({
              isLoading1: false,
              //isarchive: true,
              message_status: "archive"
              // show_archive: false
            });
            this.props.navigation.replace("Messages");
          }).catch((error) => {
            this.setState({ isLoading1: false });
            console.log("user error" + error)
          }))
  }




_goTo()
{
    if(this.state.navigateFrom.trim() === "bids_pending")
    {
        this.props.navigation.goBack();
    }
    else
    {
        this.props.navigation.replace('MessageListingScreen',{uid: this.state.uid});
    }
}

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.topContainer, { zIndex: 0, borderBottomColor: '#8e8e93', borderBottomWidth: 0.4, }]}>
                    <TouchableOpacity 
                    onPress={() => this._goTo()}>
                        <Ionicons name="ios-arrow-back" style={styles.backButton} />
                    </TouchableOpacity>
                    <View style={styles.avatorContainer}>
                        {(this.state.imageLink == "" || this.state.imageLink == undefined || this.state.imageLink == null) ?
                            (
                                <Image
                                    source={require('../../assets/images/userOwner.png')}
                                    style={styles.avatorImage}
                                />
                            )
                            :
                            (
                                <CachedImage
                                    source={{ uri: this.state.imageLink }}
                                    style={styles.avatorImage}
                                />
                            )}

                        <View style={{ marginLeft: 10 }}>
                           
                            <View style={{ flexDirection: "row", justifyContent: "space-between", width: screenSize.width - 130, marginTop: 15 }}>
                                <Text style={styles.titleText}>{this.state.ProjectOwnerName}</Text>

                                {(this.state.message_status == "inbox") ?
                                    (<TouchableOpacity
                                        onPress={() => this._showArchive()}>
                                        <Entypo name='dots-three-vertical' color='#969da8' size={30} />
                                    </TouchableOpacity>)
                                    :
                                    null}
                            </View>

                            {(this.state.show_archive == true) ?
                                (<TouchableOpacity
                                    style={styles.archiveMenu}
                                    onPress={() => this._archiveChat()}
                                >
                                    <Text style={{ color: "black", fontWeight: "600", fontSize: 16 }}>Archive</Text>
                                </TouchableOpacity>)
                                :
                                null}

                        </View>
                    </View>
                </View>

                <FlatList
                    key={this.state.chatArrayMod.length}
                    data={this.state.chatArrayMod}
                    extraData={this.state}
                    inverted={true}
                    refreshing={true}
                    ListHeaderComponent={() =>
                        <View style={{ height: 100 }}></View>
                    }
                    renderItem={({ item, index }) => (
                        <View>
                            {/* Message section start */}

                            {/* sender section */}
                            {(item.sender_id == this.state.uid) ?
                                (<View style={[styles.senderMessageContainer]}>
                                    <Text style={styles.sendMessageText}>{item.description}</Text>
                                </View>)
                                :
                                (<View style={styles.receiverMessageContainer}>
                                    <Text style={styles.receiveMessageText}>{item.description}</Text>
                                </View>)}
                            {/* receiver section */}
                            {/* Message section end */}
                        </View>
                    )}
                   // onEndReached={this._onEndReached.bind(this)}
                    //onEndReachedThreshold={10}
                  //   ListFooterComponent={() =>
                  //     <View style={{ height: 100 }}></View>
                  // }
                    keyExtractor={item => item.sender_id}
                />

                {
                    (this.state.isLoading1 == true) ?
                        (<View style={styles.activity_main}>
                        </View>)
                        :
                        (<View>
                            {(this.state.isarchive == false) ?
                                (<View style={styles.messageContainer}>
                                    <TextInput
                                        style={styles.textInput}
                                        multiline={true}
                                       // keyboardType='default'
                                        underlineColorAndroid="transparent"
                                        placeholder="Enter a message"
                                        placeholderTextColor="#ababab"
                                        returnKeyType='send'
                                        autoCorrect={false}
                                        autoCapitalize="sentences"
                                        // onSubmitEditing={() => this.inputPassword.focus()}
                                        onChangeText={(input) => this.setState({ message: input })}
                                        ref={((input) => this.inputMessage = input)}
                                    />

                                    {(this.state.isLoading == true) ?
                                        (<View style={{ padding: 5 }}>
                                            <ActivityIndicator size={15} rotationSpeed={800} strokeWidth={10} color='#3A4958' />
                                        </View>)
                                        :
                                        null}

                                    {(this.state.message.trim() != "") ?
                                        (
                                            <TouchableOpacity   activeOpacity={0.7} onPress={() => this._sendMessage()}>
                                                <Text style={styles.sendText}>Send</Text>
                                            </TouchableOpacity>)
                                        :
                                        (<View>
                                            <Text style={styles.sendText}>Send</Text>
                                        </View>
                                        )}

                                </View>)
                                :
                                null}
                        </View>)}

                {/* {(this.state.isLoading1 == true) ?
                    (<View style={styles.activity_sub}>
                        <BubblesLoader size={50} dotRadius={5} color='black' />
                    </View>)
                    :
                    null} */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        //zIndex:0
    },

    topContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        // position:"absolute",
        // top:0
    },

    backButton: {
        fontSize: 30,
        color: '#1e1e1e',
        paddingLeft: 10
    },

    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    titleText: {
        fontFamily: regularText,
        fontWeight: 'bold',
        color: '#1e1e1e',
        fontSize: 16,
        maxWidth: screenSize.width - 100
    },

    avatorContainer: {
        flexDirection: 'row',
        marginTop: 10,
        padding: 10
    },

    timeText: {
        fontFamily: regularText,
        fontSize: 16,
        marginTop: 10
    },

    bidPlaceContainer: {
        paddingHorizontal: 10,
        borderTopColor: '#8e8e93',
        borderTopWidth: 0.7,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        paddingVertical: 15
    },

    bidText: {
        fontFamily: italicText,
        fontSize: 15
    },

    footerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        // alignItems : 'center',
        height: 60,
        backgroundColor: '#fff'
    },

    footerItem: {
        width: screenSize.width / 4,
        alignItems: 'center',
        justifyContent: 'center'
    },

    footerImage: {
        fontSize: 35
    },

    messageContainer: {
        borderTopColor: '#8e8e93',
        borderTopWidth: 0.5,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
        // position:"absolute",
        // bottom:0
    },

    textInput: {
        marginLeft: 10,
        fontFamily: italicText,
        fontSize: 16,
        flex: 1
    },

    sendText: {
        fontFamily: regularText,
        color: '#3A4958',
        marginRight: 10,
        fontSize: 18
        // alignSelf: 'flex-end'
    },

    senderMessageContainer: {
        backgroundColor: '#3A4958',
        marginTop: 8,
        marginBottom: 4,
        marginRight: 10,
        maxWidth: screenSize.width - 60,
        alignSelf: 'flex-end',
        borderRadius: 10
    },
    activity_sub1: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'rgba(200, 200, 200, .6)',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },

    sendMessageText: {
        color: '#fff',
        fontFamily: regularText,
        padding: 10
    },

    receiverMessageContainer: {
        backgroundColor: '#e5e5ea',
        marginTop: 8,
        marginBottom: 4,
        marginLeft: 10,
        maxWidth: screenSize.width - 60,
        alignSelf: 'flex-start',
        borderRadius: 10
    },

    receiveMessageText: {
        color: '#1e1e1e',
        fontFamily: regularText,
        padding: 10
    },
    archiveMenu: {
        backgroundColor: "#ffff",
        width: 100,
        height: 40,
        position: "absolute",
        right: 30,
        top: 30,
        zIndex: 10,
        padding: 10,
        elevation: 5,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: 'center'
    },
    activity_main: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
    },

    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
    },
})
