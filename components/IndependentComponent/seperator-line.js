import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

class SeperatorLine extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        const { text } = this.props;
        return (
            <View style={styles.container}>
                <Text style={styles.or_text}>{'Or'}</Text>
                <View style={styles.line}>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        width: Dimension.width - 60,
        justifyContent: 'space-between',
        marginVertical: 10
    },
    or_text: {
        fontFamily: Fonts.boldText,
        color: colors.colorPrimary,
        fontSize: 16
    },
    line: {
        borderWidth: 0.5,
        borderColor: colors.colorLightGray,
        width: Dimension.width - 90
    }

});


export default SeperatorLine;
