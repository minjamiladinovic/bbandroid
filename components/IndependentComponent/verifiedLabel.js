import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

const VerifiedLabel = props => {
    const { projectVerified,type } = props;
    return (
        <>
            {projectVerified == 'Y' &&
                <Text style={[styles.text]}>{type} Verified</Text>}
            {projectVerified == 'N' &&
                <Text style={[styles.text,{backgroundColor:colors.colorOpacityOrange,color:colors.colorRedBrown}]}>{type} Not Verified</Text>}
        </>
    );
}

const styles = StyleSheet.create({
    text: {
        color: colors.colorGreen,
        backgroundColor: colors.colorOpacityGreen,
        borderRadius: 10,
        height: 25, 
        paddingHorizontal: 10, overflow: 'hidden',
        textAlign: 'center', margin: 5,
        textAlignVertical:'center'
    }
});

VerifiedLabel.propTypes = {
    projectVerified: PropTypes.string
};

export default VerifiedLabel;
