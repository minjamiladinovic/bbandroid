import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

class InputHeading extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        const { text } = this.props;
        return (
            <Text style={styles.head_text}>{text}</Text>
        );
    }
}

const styles = StyleSheet.create({
    head_text: {
        fontFamily: Fonts.boldText,
        color: colors.colorXDarkGray,
        fontSize: 15
    }
});

InputHeading.propTypes = {
    text: PropTypes.string
};

export default InputHeading;
