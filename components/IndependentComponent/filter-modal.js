import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text, TouchableOpacity, FlatList, TextInput, Modal } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

import TagViewList from '../IndependentComponent/tag-view-list';

export default class FilterModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            arrData: this.props.arrData
        }
    }
    // componentWillReceiveProps() {
    //     setTimeout(() => this.searchInput.focus(), 250);
    // }

    render() {
        var { removeItem, chooseItem, arrData, searchData, textVal, onClose, ngIf, marginTop, searchFilterFunction, placeholderText, selectAll, allSelected, isStateFilterModal } = this.props;
        return (
            <Modal
                animationType="slide"
                transparent={true}
                onRequestClose={() => onClose()}
                visible={ngIf}
                style={{}}>
                <TouchableOpacity style={Styles.commonModalContainer} activeOpacity={1} onPress={() => onClose()}>
                    <TouchableOpacity activeOpacity={1} style={[Styles.commonModalContent]}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={{
                                flexDirection: 'row', alignItems: 'center', backgroundColor: colors.colorWhite,
                                borderWidth: 0.5,
                                borderColor: colors.colorPrimary,
                                borderRadius: 10,
                                flex: 1
                            }}>
                                <Ionicons name='ios-search' color={colors.colorPrimary} size={20} style={{ marginHorizontal: 20 }} />
                                <TextInput
                                    placeholderTextColor={colors.colorLightGray}
                                    placeholder={placeholderText}
                                    style={styles.input}
                                    value={textVal}
                                    keyboardType="default"
                                    onChangeText={(textVal) => searchFilterFunction(textVal)}
                                    ref={((input) => this.searchInput = input)}
                                />
                            </View>
                            <Text onPress={() => onClose()} style={{ paddingHorizontal: 20, color: colors.colorPrimary }}>close</Text>
                        </View>
                        <View style={{
                            padding: 3,
                            marginVertical: 10,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <TagViewList removeItem={(item) => chooseItem(item)} data={arrData} />
                        </View>
                        {/* {isStateFilterModal ? <TouchableOpacity
                            onPress={() => selectAll()}
                            style={[styles.item]}>
                            <Text style={[styles.text]}>{'Select All'}</Text>
                            {allSelected && <Ionicons color={colors.colorPrimary} size={40} name='ios-checkmark' />}
                        </TouchableOpacity> : null} */}
                        <FlatList
                            contentContainerStyle={{ width: Dimension.width - 30, marginBottom: 20 }}
                            data={searchData}
                            extraData={this.props}
                            keyExtractor={(item, index) => String(index)}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity
                                    onPress={() => chooseItem(item.toString())}
                                    style={index % 2 === 0 ? styles.item : styles.item_odd}>
                                    <Text style={styles.text}>{item}</Text>
                                    {(arrData.indexOf(item) > -1) && <Ionicons color={colors.colorPrimary} size={40} name='ios-checkmark' />}
                                </TouchableOpacity>
                            )} />
                    </TouchableOpacity>
                </TouchableOpacity>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    tagView: {
        backgroundColor: colors.colorPrimary,
        paddingHorizontal: 5,
        height: 25,
        borderRadius: 3,
        justifyContent: "space-between",
        alignItems: "center",
        margin: 3,
        flexDirection: "row"
    },
    input: {
        fontSize: 12,
        fontFamily: Fonts.ralewayMedium,
        height: 30,
        width: Dimension.width * 0.5,
        padding: 0
    },
    item: {
        backgroundColor: colors.colorWhite,
        paddingVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
        paddingRight: 20
    },
    item_odd: {
        backgroundColor: colors.colorXLightGray,
        paddingVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingRight: 20,
        height: 30
    },
});

FilterModal.propTypes = {
    removeItem: PropTypes.func,
    arrData: PropTypes.array,
    searchData: PropTypes.array,
    onClose: PropTypes.func,
    textVal: PropTypes.string,
    placeholderText: PropTypes.string,
    searchFilterFunction: PropTypes.func,
    chooseItem: PropTypes.func,
    selectAll: PropTypes.func,
    allSelected: PropTypes.bool,
    isStateFilterModal: PropTypes.bool
}
