import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text, TouchableOpacity, Modal, TextInput, FlatList, ScrollView } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PopupHeading from '../IndependentComponent/popup-heading';
import PopupFooter from '../IndependentComponent/popup-footer';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

export default class DropDownPopup extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { data, onClose, ngIf, inputTerm, searchFilterFunction, headingText } = this.props;
        return (
            <Modal
                animationType="fade"
                transparent={true}
                onRequestClose={() => onClose()}
                visible={ngIf}
                style={{}}>
                <TouchableOpacity style={Styles.commonModalContainer} activeOpacity={1} onPress={() => onClose()}>
                    <TouchableOpacity activeOpacity={1} style={[[Styles.commonPopupContent]]}>
                        <PopupHeading text={headingText} />
                        <View style={{ marginTop: 30, flexDirection: 'row', alignItems: 'center', borderRadius: 5, borderColor: colors.colorPrimary, borderWidth: 0.5 }}>
                            <Ionicons name='ios-search' color={colors.colorPrimary} size={20} style={{ marginHorizontal: 20 }} />
                            <TextInput
                                placeholderTextColor={colors.colorLightGray}
                                placeholder="Type here to search"
                                style={styles.input}
                                value={inputTerm}
                                keyboardType="default"
                                onChangeText={(inputTerm) => searchFilterFunction(inputTerm)}
                            />
                        </View>
                        <FlatList
                            contentContainerStyle={{ width: Dimension.width - 30, marginBottom: 20 }}
                            data={data}
                            extraData={this.props}
                            keyExtractor={(item, index) => String(index)}
                            renderItem={({ item, index }) => (
                                <View style={index % 2 == 0 ? styles.item : styles.item_odd}>
                                    <Text style={styles.text}>{item}</Text>
                                </View>
                            )} />
                        {/* <ScrollView contentContainerStyle={{ height: Dimension.height * 0.5, width: Dimension.width - 80, paddingBottom: 20 }}> */}
                        {/* <FlatList
                            data={data}
                            contentContainerStyle={{ marginBottom: 20 }}
                            extraData={this.props}
                            renderItem={({ item, index }) => (
                                <View style={index % 2 == 0 ? styles.item : styles.item_odd}>
                                    <Text style={styles.text}>{item}</Text>
                                </View>
                            )}
                            keyExtractor={(item, index) => String(index)}
                        /> */}
                        {/* {data.map((item, index) => {
                                return (
                                    <View style={index % 2 == 0 ? styles.item : styles.item_odd}>
                                        <Text style={styles.text}>{item}</Text>
                                    </View>
                                )
                            })} */}
                        {/* </ScrollView> */}

                        <PopupFooter text={'Ok, Close'} onClick={() => onClose()} />
                    </TouchableOpacity>
                </TouchableOpacity>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: Fonts.regularText,
        fontSize: 18,
        color: colors.colorDarkGray,
        marginLeft: 10,

    },
    item: {
        backgroundColor: colors.colorWhite,
        paddingVertical: 5
    },
    item_odd: {
        backgroundColor: colors.colorXLightGray,
        paddingVertical: 5
    },
    input: {
        fontSize: 12,
        fontFamily: Fonts.ralewayMedium,
        width: Dimension.width * 0.5,
        height: 30,
        padding: 0,
    }
});

DropDownPopup.propTypes = {
    removeItem: PropTypes.func,
    data: PropTypes.array,
    onClose: PropTypes.func,
    ngIf: PropTypes.bool,
    inputTerm: PropTypes.string,
    searchFilterFunction: PropTypes.func,
    headingText: PropTypes.string
};

