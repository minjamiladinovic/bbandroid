import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text, TouchableOpacity, FlatList, TextInput } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

export default class LightBackground extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { ngIf } = this.props;
        return (
            <View
                style={ngIf ? Styles.opacityLayer : { display: 'none' }}>
                <View style={Styles.containerCenter}>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
   
});

LightBackground.propTypes = {
    ngIf:PropTypes.bool
};
