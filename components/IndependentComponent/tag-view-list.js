import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text, TouchableOpacity, Modal, TextInput, FlatList } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import InputHeading from '../IndependentComponent/text-input-heading';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

export default class TagViewList extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        var { data, removeItem } = this.props;
        return (
            <FlatList
                contentContainerStyle={{ borderRadius: 5 }}
                horizontal={true}
                scrollEnabled={true}
                extraData={this.props}
                showsHorizontalScrollIndicator={true}
                data={data}
                renderItem={({ item, index }) =>
                    <View style={styles.tagView}>
                        <Text onPress={() => console.log('hi')} style={{ fontFamily: Fonts.ralewayMedium, fontSize: 12, color: colors.colorWhite, marginLeft: 5 }}>{item}</Text>
                        <TouchableOpacity onPress={() => removeItem(item)} style={{paddingLeft:15}}>
                            <MaterialCommunityIcons name="close" style={{ fontSize: 15, color: colors.colorWhite}} />
                        </TouchableOpacity>
                    </View>
                }
                keyExtractor={(item, index) => String(index)}
            />
        );
    }
}

const styles = StyleSheet.create({
    tagView: {
        backgroundColor: colors.colorPrimary,
        paddingHorizontal: 5,
        height: 25,
        borderRadius: 3,
        justifyContent: "space-between",
        alignItems: "center",
        margin: 3,
        flexDirection: "row"
    },
});

TagViewList.propTypes = {
    data: PropTypes.array,
    removeItem: PropTypes.func
};

