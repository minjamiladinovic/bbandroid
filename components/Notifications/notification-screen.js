import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    FlatList,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Alert,
    BackHandler
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';
import Footer from '../footer/footer';
import {
    CachedImage,
} from 'react-native-cached-image';


const regularText = "SF-Pro-Display-Regular";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();


export default class NotificationsScreen extends React.Component { 
    
    
    constructor(props) {
        super(props);
        this.state = {
            uid: this.props.navigation.state.params.uid,
            isLoading : false,
            Data : [],
            notificationData : [],
            notificationDataLength : 0
        }
    }
    
    componentDidMount() {
        this._getNotifications();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
    // componentDidMount() {
    //     firebase.analytics().setCurrentScreen("Notifications", "Notifications");
    // }

    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    
    handleBackButton = () => {
        this.props.navigation.pop();
         return true;
       }


    _stopLoading() {
        //console.log('details'+JSON.stringify(this.state.pendingDataMod))
      this.setState({ isLoading: false });
    }




    _showDate(time) {

        var date = new Date(time);
        var dateReadable = date.toDateString().trim();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        hours = hours < 10 ? '0'+hours : hours;
        var strTime = hours + ':' + minutes + ' ' + ampm;
       
        return (
        <View style={{marginBottom : 5}} >
            <Text style={styles.timeText}>{dateReadable}  {strTime}</Text>
        </View>
        );
      }




    async _getNotifications() {
        this.setState({ isLoading: true });
        
          await db
            .collection("notifications").doc(this.state.uid).collection('notifications')
            .get()
            .then(data => {
              var dbData = [];
    
              data.forEach(doc => {
                var nData = doc.data();
                //console.log("loop :  " + JSON.stringify(doc.data()));
    
                dbData.push(nData);
              });
              if(dbData.length!=0)
              {
                this.setState({
                notificationData: dbData,
                notificationDataLength: dbData.length
                },this._getOwnerName.bind(this));
              }
              else { 
                this.setState({ isLoading: false });
              }
                 
            })
            .catch(err => {
              this.setState({ isLoading: false });
              console.log('onget : '+err);
            });
        
      }





      _getOwnerName() {
        var dbData = [];
        var Data = this.state.notificationData;
    
         Data.forEach(data => {
              var name
            db.collection("users").doc(data.sender).get()
              .then(doc => {
             //   console.log("_Owner Id :  " + data.sender);
                if (doc.exists) {
                    var userData=data
                    userData.firstname=doc.data().firstname;
                    userData.lastname=doc.data().lastname;
                    userData.imageLink=doc.data().imageLink;

                    if(userData.imageLink)
                    {
                        if(userData.imageLink=='')
                        {
                            userData.imageLink=null;
                        }
                    }
                    else{
                        userData.imageLink=null;
                    }
                    // if(doc.data().imageLink || doc.data().imageLink!='')
                    // userData.imageLink=doc.data().imageLink;
                    

                    //firstname
                    if(userData.firstname)
                    {
                        if(userData.firstname=='')
                        {
                            userData.ownerName='Customer';
                        }
                        else{
                            if(userData.lastname)
                            {
                                if(userData.lastname=='')
                                {
                                    userData.lastname=null;
                                }
                            }
                            else{
                                userData.lastname=null;
                            }

                            //nameformating
                            var nameflag=userData.firstname.indexOf(' ');
                            var nameflag1=userData.firstname.indexOf('  ');
                            if(nameflag!=-1)
                            {
                                var nameArray = userData.firstname.split(' ')
                                var name=nameArray[0]+' '+nameArray[1];
                                userData.ownerName=name;
                            }
                            else if(nameflag1!=-1) {
                                var nameArray = userData.firstname.split('  ')
                                var name=nameArray[0]+' '+nameArray[1];
                                userData.ownerName=name;
                            }
                            else{
                                
                                if(userData.lastname!=null)
                                {
                                    var name=userData.firstname+' '+userData.lastname;
                                    userData.ownerName=name;
                                }
                                else {
                                    userData.ownerName=userData.firstname;
                                }
                            }
                        }
                    }
                    else{
                        userData.ownerName='Customer';
                    }
                    



                  dbData.push(userData);
                  //console.log('hi count '+dbData.length);
                  if(dbData.length==this.state.notificationDataLength)
                    {
                        dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));

                        this.setState({
                            Data: dbData,
                            
                        },this._stopLoading.bind(this));
                        //console.log('hi count ');
                    }
                }
                 else {
                    var userData=data
                    name = 'Customer';
            //        console.log(name);
                    userData.ownerName=name;
                    userData.imageLink=null;
                    dbData.push(userData);
                   // console.log('hi count '+dbData.length);
                    if(dbData.length==this.state.notificationDataLength)
                    {
                        dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));
                        this.setState({
                            Data: dbData,
                            
                        },this._stopLoading.bind(this));
                       // console.log('hi count ');
                    }
                }
              })
              .catch(err => {
                console.log(err);
                var userData=data
                name = 'Customer';
          //      console.log(name);
                userData.ownerName=name;
                userData.imageLink=null;
                dbData.push(userData);
               // console.log('hi count '+dbData.length);
                if(dbData.length==this.state.notificationDataLength)
                {
                    dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));
                    this.setState({
                        Data: dbData,
                        
                    },this._stopLoading.bind(this));
                    //console.log('hi count ');
                }
              });
              
              
              
          })
          
          
          
        
      }

    


      _gotoCustomerProfile(item)
      {
       // console.log(JSON.stringify(item))
        this.props.navigation.navigate('CustomerProfileScreen',
                {profileId: item.sender,
                 profileName : item.ownerName,
                 profileImage: item.imageLink})
      }




    render() {

        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <MaterialCommunityIcons style={{ paddingLeft: 6 }} name="chevron-left" color="white" size={28} />
                    </TouchableOpacity>

                    <Text style={styles.headerText}>Notifications</Text>
                    <View></View>
                </View>


                {(this.state.notificationDataLength != 0)?
                    <ScrollView style={{width: screenSize.width ,position: "absolute",  top: 60, bottom  : 60}}>
                <FlatList
                    data={this.state.Data}
                    
                    renderItem={({ item }) => (
                        <View style={{ backgroundColor: "#ffff" ,marginTop : 5}}>
                                   


                                        <View style={{ flexDirection: "row", padding: 16, width: screenSize.width, height: 100, borderBottomColor: "#d2d1d1", borderBottomWidth: 0.5 }}>
                                            {/* <Image
                                                source={require('../../assets/images/userOwner.png')}
                                                style={styles.avatorImage}
                                            /> */}
                                            <TouchableOpacity  onPress={() => this._gotoCustomerProfile(item)}>
                                            <CachedImage
                                                source={item.imageLink?
                                                    {uri: item.imageLink }
                                                    :
                                                    require('../../assets/images/userOwner.png')}
                                                style={styles.avatorImage}
                                            />
                                            </TouchableOpacity>
                                            <View style={styles.TextView}>
                                                <Text style={{ fontSize: 14 }}>{item.subject}</Text>
                                                <View style={{height : 40,padding :0}}>
                                                 <Text style={{ fontWeight: "500", color: "black", fontSize: 15 }} numberOfLines={2}  ellipsizeMode='tail'><Text onPress={() => this._gotoCustomerProfile(item)}>{item.ownerName}</Text>
                                                {(item.status=='hired')?<Text style={{ fontWeight: "100",fontSize: 13 }}> has Hired you for project </Text> : <Text style={{ fontWeight: "100",fontSize: 13 }}> has completed the project </Text>}
                                                <Text style={{ fontWeight: "100" }}> {item.projectDescription}</Text>    
                                                </Text>   
                                                </View>
                                                
                                                {this._showDate(item.timeStamp)}
                                            </View>
                                    </View>
                                </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
                </ScrollView>
                :
                null

                }
                

                


                
                
        {this.state.isLoading == true ? (
          <View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
          <View style={styles.activity_sub}>
            <ActivityIndicator
              size="large"
              color="#D0D3D4"
              style={{
                justifyContent: "center",
                alignItems: "center",
                height: 50
              }}
            />
          </View>  
          </View>
          
        ) : null}
                    
                    
                    <Footer itemColor='notification'></Footer>


            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1
    },
    headerContainer: {
        backgroundColor: '#3A4958',
        paddingTop: Platform.OS === 'ios' ? 50 : 30,
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop : -5
    },
    headerText: {
        fontFamily: boldText,
        color: 'white',
        fontSize: 22,
        marginLeft : -40
    },
    timeText: {
        fontFamily: regularText,
        fontSize: 14,
        marginTop: 10
      },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
      },
    TextView : { marginLeft: 10,
        justifyContent: 'space-between',
        width: screenSize.width-100,
        // backgroundColor : 'red' ,
        height : 100,
        position : 'absolute',
        top : 0,
        right : 5,
        paddingVertical : 3
    },
};


