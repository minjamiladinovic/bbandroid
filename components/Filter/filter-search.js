
/* Project filter page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/filters.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Slider
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');

export default class FilterSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedSort: 'date',
            value: 40
        }
    }

    change(value) {
        this.setState({value : value});
    }

    gotoVisibleService() {
        this.props.navigation.navigate('VisibleServiceScreen');
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.signinText} onPress={() => this.props.navigation.goBack()}>Close</Text>
                    <Text style={styles.headerText}>Filters</Text>
                    <Text style={styles.nextText}></Text>
                </View>

                <View style={styles.headingContainer}>
                    <Text style={styles.headingText}>SORT</Text>
                </View>

                <View style={{ backgroundColor: '#fff' }}>
                    <TouchableOpacity style={styles.sortOptionContainer} onPress={() => this.setState({ selectedSort: 'date' })}>
                        <Text style={styles.optionText}>Date</Text>
                        {
                            this.state.selectedSort == 'date' ? (
                                <Ionicons style={styles.optionIcon} name="ios-checkmark" />
                            ) : (
                                    <View style={{ height: 36 }}></View>
                                )
                        }

                    </TouchableOpacity>
                    <TouchableOpacity style={styles.sortOptionContainer} onPress={() => this.setState({ selectedSort: 'distance' })}>
                        <Text style={styles.optionText}>Distance</Text>
                        {
                            this.state.selectedSort == 'distance' ? (
                                <Ionicons style={styles.optionIcon} name="ios-checkmark" />
                            ) : (
                                    <View style={{ height: 36 }}></View>
                                )
                        }
                    </TouchableOpacity>
                </View>

                <View style={styles.headingContainer}>
                    <Text style={styles.headingText}>SEARCH DISTANCE</Text>
                </View>

                <View style={{backgroundColor : '#fff', paddingVertical : 10}}>
                    <Slider
                        step={1}
                        maximumValue={100}
                        onValueChange={this.change.bind(this)}
                        value={this.state.value}
                        // thumbTintColor={'#fff'}
                        // minimumTrackTintColor={'#007aff'}                        
                    />
                </View>

                <View style={styles.headingContainer}>
                    <Text style={styles.headingText}>{this.state.value} miles</Text>
                </View>

                <View style={styles.headingContainer}>
                    <Text style={styles.headingText}>SERVICES</Text>
                </View>

                <TouchableOpacity style={styles.serviceOptionContainer} onPress={()=> this.gotoVisibleService()}>
                    <View>
                        <Text style={styles.optionText}>Visible servies</Text>
                    </View>

                    <View style={{flexDirection : 'row', alignItems : 'center'}}>
                        <Text style={{fontFamily : regularText, fontSize : 17, marginRight : 8}}>4 Selected</Text>
                        <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#3A4958',
        paddingVertical: 10,
        alignItems: 'center'
    },

    // backContainer: {
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     justifyContent: 'center'
    // },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    // headerImage: {
    //     fontSize: 30,
    //     color: '#007aff',
    //     fontWeight: 'bold',
    //     marginHorizontal: 3
    // },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: regularText,
        fontSize: 18,
        marginRight: 10
    },

    headingContainer: {
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 10,
    },

    headingText: {
        fontFamily: regularText,
        fontSize: 16
    },

    sortOptionContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
    },

    optionText: {
        fontFamily: regularText,
        color: '#1e1e1e',
        fontSize: 18
    },

    optionIcon: {
        fontSize: 36,
        color: '#007aff'
    },

    serviceOptionContainer : {
        justifyContent : 'space-between',
        alignItems : 'center',
        flexDirection : 'row',
        backgroundColor : '#fff',
        padding : 10
    },

    optionImage: {
        fontSize: 20,
        color: '#8e8e93'
    },
})