
/* Filter visible service page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/visible-services.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    FlatList,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    StatusBar,
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CommonTasks from '../../common-tasks/common-tasks';
const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');

export default class VisibleService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        //    serviceItems: ["Landscaping", "Lawn care", "Tree maintenance", "Pavers", "Gravel", "Lighting", "Sod installation", "Synthetic grass", "Irrigation", "Walls", "Concrete"]
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.backContainer} onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Filters</Text>
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Visible Services</Text>
                    <Text style={styles.nextText}></Text>
                </View>

                <FlatList
                    data={CommonTasks.serviceList}
                    renderItem={({ item, index }) =>
                        <View>
                            <TouchableOpacity style={styles.optionContainer}>
                                <Text style={styles.optionText}>{item.name}</Text>
                                <Ionicons name="ios-checkmark" style={styles.optionImage} />
                            </TouchableOpacity>
                        </View>
                    }
                    keyExtractor={item => item.name}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#3A4958',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff'
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    optionImage: {
        fontSize: 30,
        color: '#007aff'
    },

    optionText: {
        fontFamily: regularText,
        color: '#1e1e1e',
        fontSize: 18
    },

    optionContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: 10,
        borderTopColor: '#8e8e93',
        borderTopWidth: 0.7,
    }
})